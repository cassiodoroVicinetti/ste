\chapter{Sistemi e segnali elettronici}
\section{Definizioni}
Un \textbf{sistema elettronico} è costituito da un'interconnessione di \textbf{moduli} più semplici. Si dice che i moduli sono \textbf{interconnessi} se scambiano informazioni tra di loro.

Un sistema elettronico è univocamente definito in termini di tensioni e correnti, e l'informazione è contenuta nelle loro variazioni nel tempo.

Il progettista spesso collega dei moduli già costruiti da altri: egli non ne conosce la struttura interna, ma solo il comportamento esterno, e in particolare:
\begin{itemize}
\item \ul{funzione:} che cosa fa?
\item \ul{alimentazione:} qual è il tipo di alimentazione? quanta alimentazione assorbe?
\item \ul{segnali:} come si interconnette il modulo con il mondo esterno?
\end{itemize}

Verrà preso come modello un telefono cellulare.

\section{Funzione}
Il cellulare scambia informazioni con l'utente da una parte e il campo elettromagnetico esterno dall'altra. L'antenna è un \textbf{trasduttore} che trasforma l'informazione da un segnale elettrico in un campo elettromagnetico, e viceversa. Altri trasduttori (tastiera, microfono, auricolare, display) permettono lo scambio di informazioni tra l'utente e il telefono.

La catena di ricezione riceve i segnali analogici dall'antenna, mentre la catena di trasmissione li invia ad essa. La parte più interna del telefono lavora invece su segnali digitali. Gli \textbf{oscillatori} sono dispositivi elettronici che ricevendo non segnali in ingresso ma un'alimentazione e forniscono in uscita un segnale tempo-variante.

Il \textbf{deviatore} è un interruttore che regola la direzione del flusso di informazioni tra le catene e l'antenna.

I \textbf{transistori} sono dei dispositivi a semiconduttore che, opportunamente combinati con elementi elettrici più semplici, realizzano funzioni più complesse. L'opportuna combinazione delle tensioni applicate ai \textbf{terminali di controllo} fa commutare il deviatore.

\section{Alimentazione}
Per le leggi della termodinamica, ogni elaborazione di informazioni richiede un flusso di \textbf{energia}/potenza in ingresso $\Rightarrow$ l'alimentatore trasferisce una corrente applicando una tensione continua/costante (DC).

Il sistema di alimentazione di un cellulare si compone di circuiti complessi atti a:
\begin{itemize}
\item compensare le variazioni di tensione della batteria, in particolare quando si sta per scaricare, producendo una tensione il più costante possibile;
\item minimizzare il tempo di ricarica delle batterie e massimizzarne la durata;
\item minimizzare la produzione di calore.
\end{itemize}

\section{Segnali}
I flussi di informazioni sono contenuti in vari tipi di segnali:
\begin{itemize}
\item segnali analogici a radiofrequenza: sono ad alta frequenza;
\item segnali analogici audio: sono a bassa frequenza;
\item segnali digitali: sono codificati in bit.
\end{itemize}

\subsection{Segnali analogici}
I segnali analogici sono di tipo sinusoidale:
\[
v\left(t\right)=V \sin{\left(\omega t + \phi\right)} = V_{C} \cos{\left(\omega t\right)} + V_{S} \sin{\left(\omega t\right)}
\]

\paragraph{Caso particolare} $\phi = 0 \Rightarrow V_{C} = 0 \wedge V_{S} = V$

Dato un segnale analogico descritto dalla trasformata di Fourier:
\[
v\left(t\right)=\sideset{}{_n}\sum{V_n \sin{\left(\omega_n t + \phi_n\right)}}
\]
il \textbf{contenuto spettrale} è il grafico delle ampiezze $V_n$ in funzione della frequenza $f= \frac{1}{T} = \frac{\omega}{2\pi}$. $V_0$ si dice \textbf{componente continua} (DC), perché il suo contributo è costante ($\omega=0$).

\paragraph{Esempi di contenuti spettrali}
\begin{itemize}
\item singolo tono audio a singola frequenza non periodico (\ul{sinusoide}) $\Leftrightarrow$ linea verticale in corrispondenza della singola frequenza;
\item segnale periodico in più frequenze (\ul{onda triangolare}) $\Leftrightarrow$ tante linee verticali quante sono le frequenze;
\item sovrapposizione di sinusoidi (\ul{voce}) $\Leftrightarrow$ l'energia è distribuita su intervalli di frequenza continui (= \textbf{banda}), ovvero costituiti dall'accostamento di linee verticali infinitesime.
\end{itemize}

La rapidità (pendenza) di variazione del segnale è proporzionale alla sua frequenza.

I segnali analogici sono continui sia nel tempo sia in ampiezza.

Il \textbf{modulatore} è un dispositivo che trasferisce le informazioni da segnale audio a segnale a radiofrequenza, e il demodulatore viceversa.

\subsection{Segnali digitali}
I \textbf{segnali digitali} sono discreti sia nel tempo sia in ampiezza $\Rightarrow$ essi sono delle rappresentazioni approssimate dei segnali analogici che introducono degli errori:
\begin{itemize}
\item \textbf{frequenza di campionamento} $f_s$ (nel tempo): sono rappresentabili segnali analogici con banda $f_a \le {1 \over 2} f_s$ (\textbf{criterio di Nyquist});
\item \textbf{quantizzazione:} nel rappresentare le ampiezze, i valori digitali sono in numero finito $\Rightarrow$ tra gli istanti di tempo discretizzati il valore di tensione non è ben definito.
\end{itemize}

A ciascun istante di tempo discretizzato è associato un valore di bit: $1 = H$ (\textbf{stato logico} alto) e $0 = L$ (stato basso).

Ridurre i margini di rumore, ovvero avvicinare gli intervalli di valori corrispondenti agli stati, aiuta a ridurre i consumi di energia $\Rightarrow$ ridurre la tensione di alimentazione, infatti, è utile per compensare gli aumenti di campo elettrico determinati dalla miniaturizzazione dei componenti. Bisogna tuttavia assicurarsi di non ridurre troppo i margini di rumore, per non confondere il bit 1 con il bit 0.

\subsection{Rumore e disturbi}
Più l'ampiezza del segnale utile è bassa, più il \textbf{rumore} disturba in modo significativo il segnale.

Ogni passaggio di elaborazione/amplificazione aggiunge rumore al segnale. Inoltre, gli amplificatori amplificano oltre al segnale utile il rumore prodotto da tutti quelli precedenti $\Rightarrow$ è importante porre dei dispositivi a basso rumore, soprattutto all'inizio della catena.

Se il segnale è digitale e gli effetti del rumore sono sufficientemente contenuti, il segnale originario può essere recuperato alla fine della catena da un dispositivo di ripristino chiamato \textbf{comparatore di soglia}, che discerne i valori digitali per confronto con il \textbf{valore soglia}.

\subsection{Interfacce verso il mondo esterno}
La trasmissione del segnale (d)al mondo esterno è tipicamente in analogico, mentre l'elaborazione è più efficiente in digitale $\Rightarrow$ occorrono dei convertitori analogico/digitale (A/D) e digitale/analogico (D/A). Il demodulatore abbassa la banda del segnale analogico affinché la conversione in digitale risulti più efficiente.

Si dice \textbf{front-end} l'interfaccia in ingresso verso il mondo esterno, e il \textbf{back-end} viceversa.

\subsection{Vantaggi/limiti dei segnali digitali}
\paragraph{Vantaggi}
\begin{itemize}
\item comparatore di soglia $\Rightarrow$ non cumula rumore;
\item si possono realizzare più facilmente componenti che eseguono funzioni complesse;
\item esistono programmi CAD per progettare moduli digitali in maniera quasi automatica $\Rightarrow$ costi più bassi;
\item comportamento modificabile a livello software, grazie alla programmazione.
\end{itemize}

\paragraph{Limiti}
\begin{itemize}
\item errore di quantizzazione e discretizzazione nel tempo;
\item non tutti i segnali possono essere digitali.
\end{itemize}