\chapter{Il transistore bipolare}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/09/Schema_generico_transistore.png}
\end{figure}

\noindent
Il \textbf{transistore} è un dispositivo elettronico caratterizzato da:
\begin{itemize}
\item almeno tre terminali, uno dei quali individua il \textbf{terminale comune} tra la porta\footnote{La \textbf{porta} è una coppia di terminali tali che la corrente che esce da uno entra dall'altro.} di ingresso e la porta di uscita;
\item una corrente di uscita $I_u$ che dipende, oltre che dalla tensione di uscita $V_u$, anche dal segnale di ingresso:
\begin{itemize}
\item \textbf{transistore bipolare:} (analogico) la \textbf{caratteristica di uscita} $I_u \left( V_u,I_i \right)$ è controllata in corrente $I_i$;
\item \textbf{transistore a effetto di campo} (FET): (digitale) la caratteristica di uscita $I_u \left( V_u,V_i \right)$ è controllata in tensione $V_i$.
\end{itemize}
\end{itemize}

I transistori trovano applicazione nei generatori pilotati, negli amplificatori, nei circuiti digitali\textellipsis\ Per le leggi di Kirchhoff:
\[
\begin{cases} \displaystyle I_1 + I_2 + I_3 = 0 \\ \displaystyle V_{21} = V_2 - V_1 = V_{23} - V_{13} \end{cases}
\]

Il \textbf{transistore bipolare a giunzione} è un dispositivo caratterizzato da tre terminali detti emettitore, base e collettore, che corrispondono alle tre regioni ottenute unendo due giunzioni \textit{pn} in antiserie.
\FloatBarrier

\section{Regioni di funzionamento}
A seconda della polarizzazione delle due giunzioni, si hanno quattro regioni di funzionamento:
\begin{itemize}
\item \textbf{regione attiva diretta:} giunzione BE in diretta ($V_{BE} >0$), giunzione BC in inversa ($V_{BC} <0$);
\item \textbf{regione attiva inversa:} giunzione BE in inversa ($V_{BE} <0$), giunzione BC in diretta ($V_{BC} >0$): le prestazioni da amplificatore sono pessime perché il transistore bipolare non è ottimizzato in termini di livelli di drogaggio per lavorare in regione attiva inversa;
\item \textbf{regione di interdizione:} giunzione BE in inversa ($V_{BE} <0$), giunzione BC in inversa ($V_{BC} <0$): approssima il circuito aperto ($I_C \approx I_E \approx 0$) perché i portatori maggioritari non sono spinti dall'emettitore al collettore;
\item \textbf{regione di saturazione:} giunzione BE in diretta ($V_{BE} >0$), giunzione BC in diretta ($V_{BC} >0$): approssima il cortocircuito perché la caduta di potenziale è trascurabile ($V_{CE} = V_{BE} - V_{BC} \approx 0$) con un forte flusso di corrente.
\end{itemize}

Le regioni attive sono usate per le applicazioni analogiche, le altre due per le applicazioni digitali.

\section{Regione attiva diretta}
\subsection{Uso come amplificatore (analogico)}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/09/Transistore_BJT_in_uso_come_amplificatore.png}
\end{figure}

\noindent
Si collegano un generatore di corrente $I_i$ e una resistenza di carico $R_c$ a un transistore che lavora in condizioni di piccolo segnale. Un \textbf{amplificatore} richiede una relazione lineare tra ingresso e uscita:
\[
\begin{cases} \displaystyle I_u \left( V_u , I_i \right) = I_u \left( {V_u}_0 + {v_u}_{ss} \left( t \right) , {I_i}_0 + {i_i}_{ss} \left( t \right) \right) \simeq \\ \displaystyle I_u \left( V_u , I_i \right) = {I_u}_0 + {i_u}_{ss} \left( t \right) = I_u \left( {V_u}_0 , {I_u}_0 \right) + {i_u}_{ss} \left( t \right) \end{cases}
\]
\[
\begin{cases} \displaystyle \simeq I_u \left( {V_u}_0 , {I_u}_0 \right) + {\left. \frac{\partial I_u}{\partial V_u} \right|}_{\left( {V_u}_0 , {I_u}_0 \right)} \cdot {v_u}_{ss} \left( t \right) + {\left. \frac{\partial I_u}{\partial I_i} \right|}_{\left( {V_u}_0 , {I_u}_0 \right)} \cdot {i_i}_{ss} \left( t \right) \\
{} \end{cases} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle {i_u}_{ss} \left( t \right) = {\left. \frac{\partial I_u}{\partial V_u} \right|}_{\left( {V_u}_0, {I_u}_0 \right)} \cdot {v_u}_{ss} \left( t \right) + {\left. \frac{\partial I_u}{\partial I_i} \right|}_{\left( {V_u}_0 , {I_u}_0 \right)} \cdot {i_i}_{ss} \left( t \right) \\ \displaystyle {i_u}_{ss} \left( t \right) = C \cdot {i_i}_{ss} \left( t \right) \end{cases} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle C= {\left. \frac{\partial I_u}{\partial I_i} \right|}_{\left( {V_u}_0 , {I_u}_0 \right)} \\ \displaystyle {\left. \frac{\partial I_u}{\partial V_u} \right|}_{\left( {V_u}_0 , {I_u}_0 \right)} \cdot {v_u}_{ss} \left( t \right) = 0 \Leftrightarrow I_u \left( V_u \right) = \text{cost.} \end{cases}
\]

Affinché sia un amplificatore, la caratteristica di uscita $I_u$ deve dipendere da $V_i$ ma non da $V_u$.
\FloatBarrier

\subsection{Descrizione qualitativa}
Per esempio, nel transistore $npn$:
\begin{enumerate}
\item la tensione di polarizzazione diretta applicata sulla giunzione $np$ a sinistra spinge gli elettroni liberi maggioritari dal suo lato $n$ (\textbf{emettitore}) alla regione $p$ (\textbf{base}) in comune con la giunzione \textit{pn} a destra;
\item se la base è abbastanza sottile (in particolare più corta della lunghezza di diffusione), gli elettroni non fanno in tempo a ricombinarsi ed entrano nella regione svuotata della giunzione \textit{pn} a destra;
\item la tensione di polarizzazione inversa della giunzione \textit{pn} a destra li spinge verso il suo lato $n$ (\textbf{collettore}), creando una corrente di trascinamento.
\end{enumerate}

In questo modo, la corrente di trascinamento generata è molto più alta di quella che si otterrebbe da una giunzione \textit{pn} isolata, perché ai portatori minoritari della giunzione \textit{pn} a destra si aggiungono i portatori maggioritari provenienti dalla giunzione $np$ a sinistra; per questo motivo, la corrente di trascinamento generata è fortemente dipendente dalla tensione di polarizzazione diretta applicata alla giunzione \textit{pn} il cui lato $n$ è l'emettitore, e viceversa per il collettore $\Rightarrow$ il transistore bipolare è un amplificatore perché la corrente di uscita $I_C$ è indipendente dalla tensione applicata sull'uscita $V_{CE}$.

Nonostante sia geometricamente simmetrica, la struttura del transistore bipolare non è simmetrica in quanto i livelli di drogaggio dell'emettitore e del collettore differiscono in modo che il transistore sia ottimizzato per lavorare in regione attiva diretta.

\subsection{Descrizione quantitativa}
Le concentrazioni di portatori minoritari ai confini delle regioni di svuotamento seguono le leggi delle giunzioni:
\begin{itemize}
\item regione di svuotamento tra emettitore e base:
\[
\begin{cases} \displaystyle p_n' \left( {x_E}^- \right) = \frac{ {n_i}^2 }{ {N_D}_E } \left( e^{\frac{V_{BE}}{V_T}} -1 \right) \\ \displaystyle n_p' \left( {x_E}^+ \right) = \frac{{n_i}^2}{ {N_A}_B } \left( e^{\frac{V_{BE}}{V_T}} -1 \right) \end{cases}
\]
\item regione di svuotamento tra base e collettore:
\[
\begin{cases} \displaystyle n_p' \left( {x_C}^- \right) = \frac{ {n_i}^2 }{ {N_A}_B } \left( e^{\frac{V_{BC}}{V_T}} -1 \right) \\ \displaystyle p_n' \left( {x_C}^+ \right) = \frac{{n_i}^2}{ {N_D}_C } \left( e^{\frac{V_{BC}}{V_T}} -1 \right) \end{cases}
\]
\end{itemize}

Poiché i bordi della giunzione sono dei contatti ohmici, non ci sono portatori minoritari in eccesso. Spesso l'emettitore e la base sono lati corti e il collettore è un lato lungo $\Rightarrow$ gli eccessi di portatori minoritari hanno un andamento esponenziale nel collettore e lineare nella base e nell'emettitore $\Rightarrow$ le correnti di diffusione, legate alle derivate degli eccessi di portatori minoritari,\footnote{Per i portatori maggioritari si assumono le ipotesi semplificative (condizioni stazionarie e fenomeni di generazione/ricombinazione trascurabili) $\Rightarrow$ il loro contributo in corrente è esprimibile in funzione di quello dei portatori minoritari.} sono costanti nella base e nell'emettitore, ed esponenziali ma molto piccole nel collettore.

Il \textbf{flusso} $F$ di portatori è il numero di portatori per unità di tempo e di area che si spostano:
\[
\begin{cases} \displaystyle \left| {J_{\text{diff}}}_n \right| = q \left| F_n \right| \\ \displaystyle \left| {J_{\text{diff}}}_p \right| = q \left| F_p \right| \end{cases}
\]

In totale sono presenti 5 flussi di carica:
\[
\begin{cases} \displaystyle I_E = -q A {F_n}_{EB} -qA {F_p}_{BE} \\ \displaystyle I_C = qA {F_n}_{BC} + \left( qA { {F_n}_{BC} }_0 + q A { {F_p}_{CB} }_0 \right) = qA {F_n}_{BC} + {I_{CB}}_0 \approx qA {F_n}_{BC} \end{cases}
\]
dove:
\begin{itemize}
\item la corrente $I_E$ deriva dal moto di portatori in corrispondenza della regione svuotata tra emettitore e base;
\item la corrente $I_C$ deriva dal moto di portatori in corrispondenza della regione svuotata tra base e collettore.
\end{itemize}

Il flusso ${F_n}_{BC}$ dei portatori maggioritari provenienti dall'emettitore si aggiunge ai flussi ${ {F_n}_{BC} }_0$ e ${ {F_p}_{BC} }_0$ di portatori minoritari che avrebbe il collettore se fosse isolato dall'emettitore, e che danno origine alla piccolissima \textbf{corrente inversa di saturazione} ${I_{CB}}_0$.

\subsection{Parametri di efficienza del dispositivo a transistore}
Il comportamento da amplificatore è accentuabile minimizzando la corrente di ingresso $I_B$:
\[
\begin{cases} \displaystyle I_B + I_C + I_E = 0 \\ \displaystyle I_B \approx 0 \end{cases} \Rightarrow I_C \approx - I_E
\]
cioè minimizzando il flusso di lacune maggioritarie ${F_p}_{BE}$ tra base ed emettitore con un differente livello di drogaggio tra base ed emettitore:
\[
\begin{cases} \displaystyle I_B = - I_C - I_E = qA {F_n}_{EB} + qA {F_n}_{BE} - qA {F_n}_{BC} \\ \displaystyle I_C \approx - I_E \\ \displaystyle {N_A}_B \ll {N_D}_E \Rightarrow {F_p}_{BE} \approx 0 \end{cases} \Rightarrow \begin{cases} \displaystyle I_B \approx 0 \\ \displaystyle {F_n}_{EB} \approx {F_n}_{BC} \end{cases}
\]

I \textbf{fattori di merito} del transistore sono due parametri che misurano la qualità del dispositivo, perché sono tanto più vicini a 1 quanto più è minimizzata la corrente di ingresso $I_B$:
\begin{itemize}
\item la condizione ${F_p}_{BE} \approx 0$ è misurata dall'\textbf{efficienza di iniezione di emettitore} $\gamma$:
\[
\gamma = \frac{ {F_n}_{EB} }{ {F_n}_{EB} + {F_p}_{BE} } \leq 1
\]
che per avvicinarsi all'idealità impone la seguente condizione sui livelli di drogaggio di base ${N_A}_B$ ed emettitore ${N_D}_E$:\footnote{La base e l'emettitore sono supposti essere lati corti.}
\[
\gamma = \frac{1}{1+ \frac{ {D_p}_E }{ {D_n}_B } \cdot \frac{ {N_A}_B }{ {N_D}_E } \cdot \frac{w_B}{w_E} } \to 1 \Leftrightarrow {N_A}_B \ll {N_D}_E
\]
\item la condizione ${F_n}_{EB} \approx {F_n}_{BC}$ è misurata dal \textbf{fattore di trasporto di base} $b$:
\[
b= \frac{ {F_n}_{BC} }{ {F_n}_{EB} } \leq 1
\]
che per avvicinarsi all'idealità impone che la base sia un lato corto:
\[
b= \frac{1}{ \cosh{ \frac{w_B}{ {L_n}_B } } } \to 0 \Leftrightarrow w_B \ll {L_n}_B
\]
\end{itemize}

Altri parametri descrivono l'efficienza del dispositivo:
\begin{itemize}
\item l'\textbf{amplificazione di corrente a base comune} $\alpha_F$, idealmente pari a 1, lega la corrente $I_C$ del collettore alla corrente $I_E$ dell'emettitore:
\[
\alpha_F = \gamma_b < 1 \Rightarrow I_C = - \alpha_F I_E + {I_{CB}}_0 \approx - \alpha_F I_E
\]
dove la corrente inversa di saturazione ${I_{CB}}_0$ è detta \textbf{corrente di collettore a emettitore aperto} (cioè se $I_E=0 \Rightarrow I_C= {I_{CB}}_0$);
\item l'\textbf{amplificazione di corrente a emettitore comune} $\beta_F$, idealmente molto grande,\footnote{Proprio perché $\beta_F$ è un valore molto grande rispetto ad $\alpha_F$ da cui deriva, l'errore di $\alpha_F$ si propaga molto in $\beta_F$:
\[
\frac{\Delta \beta_F}{\beta_F} \approx \beta_F \frac{\Delta \alpha_F}{\alpha_F}
\]} lega la corrente $I_C$ del collettore alla corrente $I_B$ della base:
\[
\beta_F = \frac{\alpha_F}{1- \alpha_F} \Rightarrow I_C = \beta_F I_B + {I_{CE}}_0
\]
dove ${I_{CE}}_0$ è la \textbf{corrente di collettore a base aperta} ($I_B=0$):
\[
\begin{cases} \displaystyle {I_{CE}}_0 = \frac{\beta_F}{\alpha_F} {I_{CB}}_0 \\ \displaystyle \alpha_F \to 1 \end{cases} \Rightarrow \begin{cases} \displaystyle {I_{CE}}_0 \approx \beta_F {I_{CB}}_0 \\ \displaystyle {I_{CB}}_0 \approx 0 \end{cases} \Rightarrow I_C \approx \beta_F I_B
\]
\end{itemize}

\section{Modello statico di Ebers Moll}
\subsection{Configurazione a base comune}
\label{sez:regione_attiva_diretta}
Il \textbf{modello statico di Ebers Moll} generalizza il funzionamento in regione attiva diretta $\Rightarrow$ vale in ogni regione di funzionamento indipendentemente dalla polarizzazione delle tensioni applicate:
\[
\begin{cases} \displaystyle I_E = - I_F + \alpha_R I_R \\ \displaystyle I_C = - I_R + \alpha_F I_F \end{cases}
\]
dove:
\begin{itemize}
\item $I_F$ e $I_R$ sono le caratteristiche statiche delle singole giunzioni:
\[
\begin{cases} \displaystyle I_F = {I_E}_0 \left( e^{\frac{ V_{BE} }{ V_T }} -1 \right) \\ \displaystyle I_R = {I_C}_0 \left( e^{\frac{ V_{BC} }{V_T}} -1 \right) \end{cases}
\]
\item $\alpha_R$ è l'amplificazione di corrente a base comune in regione attiva inversa, legata ad $\alpha_F$ dalla \textbf{condizione di reciprocità}:
\[
\alpha_R {I_C}_0 = \alpha_F {I_E}_0
\]
\end{itemize}

Il circuito equivalente sostituisce a ogni giunzione il parallelo tra un diodo, rappresentante il flusso di corrente che la giunzione avrebbe se fosse isolata, e un generatore pilotato di corrente, rappresentante il contributo in corrente proveniente dall'altra giunzione:
\begin{itemize}
\item \ul{regione attiva diretta:} la corrente dei portatori minoritari della giunzione base-collettore $I_R$ è trascurabile:
\[
\begin{cases} \displaystyle I_E = - I_F \\ \displaystyle I_C = \alpha_F I_F \\ \displaystyle I_B = - I_C - I_E \end{cases} \Rightarrow \begin{cases} \displaystyle I_C \approx - \alpha_F I_E = \alpha_F {I_E}_0 \left( e^{\frac{V_{BE}}{V_T}} -1 \right) \\ \displaystyle I_B \approx \left( 1- \alpha_F \right) I_E \end{cases}
\]
\item \ul{regione attiva inversa:} la corrente dei portatori minoritari della giunzione base-emettitore $I_E$ è trascurabile:
\[
\begin{cases} \displaystyle I_C = - I_R \\ \displaystyle I_E = \alpha_R I_R \end{cases} \Rightarrow I_E \approx - \alpha_R I_C
\]
\end{itemize}

\subsection{Configurazione a emettitore comune}
Il modello statico di Ebers Moll descrive un transistore in configurazione a base comune:
\[
\begin{cases} \displaystyle I_E = f \left( V_{BE} , V_{BC} \right) \\ \displaystyle I_C = f \left( V_{BE} , V_{BC} \right) \end{cases}
\]

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/09/Regioni_transistore_BJT_ad_emettitore_comune.png}
\end{figure}

Nella \textbf{configurazione a emettitore comune}, l'ingresso è la corrente $I_B$ entrante nella base, e l'uscita è la corrente $I_C$ entrante nel collettore. Il terminale in comune tra ingresso e uscita è l'emettitore, a cui fanno riferimento tutte le tensioni:
\[
\begin{cases} \displaystyle I_B = f \left( V_{BE} , V_{CE} \right) \\ \displaystyle I_C = f \left( V_{BE} , V_{CE} \right) \end{cases}
\]

In regione attiva diretta ($V_{CE} \geq 0,2 \; V$):
\begin{itemize}
\item la caratteristica di ingresso $I_B \left( V_{BE} , V_{CE} \right)$, per ogni tensione $V_{BE}$ fissata, è indipendente dalla tensione $V_{CE}$, e quindi $V_{BC}$, applicata, ed è approssimabile a una ``spezzata'' nel punto di accensione ($V_{BE} =0,5 \div 0,6 \; V$ per il silicio);
\item la corrente di uscita $I_C \left( V_{BE} , V_{CE} \right)$, per ogni corrente di ingresso $I_B \left( V_{BE} , V_{CE} \right)$ fissata, è indipendente dalla tensione $V_{CE}$ applicata,\footnote{Anche in regione attiva inversa la corrente di uscita è indipendente, ma essa risulta amplificata molto meno.} ed è approssimabile con un ``pettine'' e con una resistenza finita in condizioni di saturazione.
\end{itemize}
\FloatBarrier

\subsection{Uso in commutazione (digitale)}
\begin{figure}
	\centering
	\includegraphics[width=0.19\linewidth]{pic/09/BJT_transistor_in_use_on_switching.png}
\end{figure}

\noindent
Il circuito in figura impone una retta di carico alla porta di uscita (collettore):
\[
V_{CC} = R_C I_C+ V_{CE}
\]

Il transistore bipolare può essere usato nelle applicazioni digitali commutando, attraverso la scelta della corrente di ingresso $I_B$, tra la regione di saturazione ($V_{CE} = {V_{CE}}_{\text{sat}} \approx 0$) e quella di interdizione ($I_C=0$).

Il punto di funzionamento $\left( {I_C}_0 , {V_{CE}}_0 \right)$ del transistore è l'intersezione tra la retta di carico e la caratteristica di uscita $I_C= \beta_F I_B$:
\begin{itemize}
\item interdizione (circuito aperto):
\[
I_B \approx 0 \Rightarrow \begin{cases} \displaystyle {I_C}_0 \approx 0 \\ \displaystyle {V_{CE}}_0 \approx V_{CC} \end{cases}
\]
\item saturazione/conduzione (cortocircuito):
\[
I_B \geq \frac{ {I_C}_0 }{\beta_F} \Rightarrow \begin{cases} \displaystyle {I_C}_0 = \frac{V_{CC} - {V_{CE}}_{\text{sat}}}{R_C} \approx \frac{V_{CC}}{R_C} \\ \displaystyle {V_{CE}}_0 \approx {V_{CE}}_{\text{sat}} \end{cases}
\]
\end{itemize}

Più la corrente $I_B$ è elevata, più si garantisce che la tensione ${V_{CE}}_0$ sia compresa nella regione di saturazione (${V_{CE}}_0 < {V_{CE}}_{\text{sat}}$), e più è bassa la potenza dissipata $P= {V_{CE}}_0 \cdot {I_C}_0 \leq {V_{CE}}_0 \cdot \left( {\beta_F}_{\text{min}}  I_B \right)$ $\Rightarrow$ per massimizzare la corrente $I_B$ così da minimizzare la tensione ${V_{CE}}_0$, conviene scegliere l'estremo inferiore ${\beta_F}_{\text{min}}$ della fascia di incertezza di $\beta_F$.
\FloatBarrier

\subsection{Effetto Early}
In regione attiva diretta, a un aumento della tensione $V_{CE}$ corrisponde un aumento\footnote{in valore assoluto} della tensione di polarizzazione inversa $V_{BC}$ $\Rightarrow$ la regione svuotata della giunzione base-collettore allarga i propri confini $x_p$ e $x_n$, in particolare avvicinandosi all'emettitore dalla parte della base $\Rightarrow$ aumenta il flusso di elettroni maggioritari provenienti dall'emettitore $\Rightarrow$ le caratteristiche di uscita $I_C \left( V_{CE} \right)$ non sono più idealmente costanti al variare della tensione di uscita $V_{CE}$, ma convergono alla \textbf{tensione di Early} $-V_A$ con pendenza $\frac{ {I_C}_0}{V_A+ {V_C}_0}$.

Per minimizzare l'effetto Early, poiché vale la condizione di neutralità:
\[
{N_A}_B x_p = {N_D}_C x_n \Rightarrow \frac{ {N_A}_B}{ {N_D}_C} = \frac{x_p}{x_n} = \text{cost.}
\]
si può minimizzare l'estensione del confine $x_p$ nella base imponendo un differente livello di drogaggio tra base e collettore:
\[
{N_A}_B \gg {N_D}_C
\]
in modo che la regione di svuotamento si estenda di più nel collettore dalla parte del confine $x_n$. In definitiva:
\[
{N_D}_E \gg {N_A}_B \gg {N_D}_C
\]

\subsection{Fenomeni di breakdown}
L'allargamento della regione svuotata verso l'emettitore può portare al breakdown per perforazione diretta (la base ``sfora'' nella regione svuotata della giunzione base-emettitore) o per effetto valanga: si verifica quindi un forte aumento della tensione $V_{CE}$ e della corrente di uscita $I_C$, e quindi della potenza dissipata.

\section{Modello dinamico di Ebers Moll}
\subsection{Modello dinamico di ampio segnale}
Il \textbf{modello dinamico di ampio segnale} aggiunge al modello statico di Ebers Moll:
\begin{itemize}
\item due coppie di condensatori (non lineari) in parallelo, che tengono conto degli effetti capacitivi di ritardo associati alle singole giunzioni (ovvero la capacità di svuotamento $C_s \left( v \right)$ e la capacità di diffusione $C_d \left( v \right)$);
\item tre resistenze parassite collegate ai tre terminali, che tengono conto delle perdite in prestazioni.
\end{itemize}

\subsection[Modello statico di piccolo segnale]{Modello statico\footnote{Il modello statico è approssimativamente valido, oltre che per il punto di funzionamento a riposo, anche per segnali tempo-varianti a basse frequenze, poiché si possono trascurare gli effetti capacitivi.} di piccolo segnale}
Scomponendo, linearizzando e approssimando in piccolo segnale le caratteristiche $I_C$ e $I_B$ della configurazione a emettitore comune, si trovano le espressioni del doppio bipolo transistore:
\[
\begin{cases} \displaystyle {i_B}_{ss} \left( t \right) = {\left. \frac{\partial i_B}{\partial v_{BE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} {v_{BE}}_{ss} \left( t \right) + {\left. \frac{\partial i_B}{\partial v_{CE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} {v_{CE}}_{ss} \left( t \right) = g_{11} {v_{BE}}_{ss} \left( t \right) + g_{12} {v_{CE}}_{ss} \left( t \right) \\
\displaystyle {i_C}_{ss} \left( t \right) = {\left. \frac{\partial i_C}{\partial v_{BE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} {v_{BE}}_{ss} \left( t \right) + {\left. \frac{\partial i_C}{\partial v_{CE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} {v_{CE}}_{ss} \left( t \right) = g_{21} {v_{BE}}_{ss} \left( t \right) + g_{22} {v_{CE}}_{ss} \left( t \right) \end{cases}
\]
dove, in regione attiva diretta:\footnote{Si veda la sezione~\ref{sez:regione_attiva_diretta}.}
\begin{itemize}
\item $g_{11}$ e $g_{22}$ sono delle conduttanze:
\[
\begin{cases} \displaystyle g_{11} = {\left. \frac{\partial i_B}{\partial v_{BE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} = \frac{g_{21}}{\beta_F} = \frac{ {I_C}_0 }{\beta_F V_T} \\
\displaystyle g_{22} = {\left. \frac{\partial i_C}{\partial v_{CE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} = \frac{ {I_C}_0 }{V_A + {V_C}_0} \approx \frac{ {I_C}_0 }{V_A} \end{cases}
\]
\item $g_{12}$ e $g_{21}$ sono delle trans-conduttanze:
\[
\begin{cases} \displaystyle g_{12} = {\left. \frac{\partial i_B}{\partial v_{CE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} = \frac{g_{22}}{\beta_F} \approx \frac{ {I_C}_0 }{\beta_F V_A} \\
\displaystyle g_{21} = {\left. \frac{\partial i_C}{\partial v_{BE}} \right|}_{\left( {V_{BE}}_0 , {V_{CE}}_0 \right)} = \frac{ {I_C}_0 }{V_T} \end{cases}
\]
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.66\linewidth]{pic/09/Small-signal_static_model_for_BJT_transistor.png}
\end{figure}

Le equazioni sono interpretabili circuitalmente come il \textbf{circuito a $\pi$} in figura, dove:
\[
\begin{matrix} \displaystyle r_\pi = \frac{1}{g_{11}} = \beta_F \frac{V_T}{ {I_C}_0 } && \displaystyle r_o = \frac{1}{g_{22}} = \frac{V_A}{ {I_C}_0 } \\ \displaystyle r_\mu = \frac{1}{g_{12}} = \beta_F \frac{V_A}{ {I_C}_0 } && \displaystyle g_m = g_{21} = \frac{ {I_C}_0 }{V_T} \end{matrix}
\]

$g_m$ è la transconduttanza del transistore nel suo punto di funzionamento a riposo, e lega la corrente di uscita $i_C$ e la tensione di ingresso $V_{BE}$ $\Rightarrow$ per avere un'amplificazione elevata, il transistore va polarizzato con una corrente di collettore ${I_C}_0$ elevata. In assenza di effetto Early, le resistenze $r_\mu$ e $r_o$ che modellizzano le perdite non hanno più influenza:
\[
V_A \to + \infty \Rightarrow \begin{cases} \displaystyle r_\mu \to + \infty \\ \displaystyle r_o \to + \infty \end{cases}
\]

\subsection{Modello dinamico di piccolo segnale}
Gli effetti capacitivi che tengono conto del comportamento dinamico sono dati dai contributi della capacità di svuotamento $C_s \left( v \right)$ della giunzione base-collettore e della capacità di diffusione $C_d \left( v \right)$ della giunzione base-emettitore. Ad alta frequenza, i condensatori si approssimano al cortocircuito $\Rightarrow$ impediscono al dispositivo di funzionare.