\chapter{Gli oscilloscopi analogici}
L'\textbf{oscilloscopio} nasce come strumento che visualizza, su uno schermo con tubo a raggi catodici, l'andamento dei segnali di un generatore. Successivamente è diventato uno strumento di misura.

\paragraph{Misura due tensioni $V_x$ e $V_y$ in ingresso variabili nel tempo}
La prima fa muovere un punto luminoso lungo l'asse $x$, la seconda lungo l'asse $y$. Il punto ha una certa persistenza su uno schermo, permettendo di saperne la traiettoria.

\section{Modi operativi}
\subsection{Modalità XY}
Sullo schermo appare la curva parametrica data dalle equazioni: $x=V_x (t)$ e $y=V_y (t)$.

\subsection{In base ai tempi}
Sullo schermo appare solo l'andamento nel tempo di $V_y (t)$, mentre $V_x (t)$ viene generato all'interno dell'oscilloscopio come una funzione proporzionale rispetto al tempo: $x=V_x (t)=k \cdot t$.

Il grafico di $V_x$ è composto da una successione di: salita (detta \textbf{rampa}/{spazzolata}) $\rightarrow$ discesa (qualunque) $\rightarrow$ retta orizzontale. Durante la spazzolata, viene disegnato sullo schermo l'andamento di $V_y$.

\section{Parametri}
Sul pannello anteriore dell'oscilloscopio, vi sono due potenziometri per regolare la traccia:
\begin{itemize}
\item INT (intensità): regola la luminosità della traccia $\rightarrow$ una intensità troppo alta produce una traccia troppo spessa e rovina lo schermo;
\item FOCUS: regola la messa a fuoco del punto $\rightarrow$ è necessario rendere il punto il più piccolo possibile per ridurre l'incertezza di lettura.
\end{itemize}

\subsection{Asse verticale}
\begin{itemize}
\item \ul{sensibilità:} ampiezza del segnale in ingresso necessaria per muovere di un tot il punto lungo l'asse $y$ (con incertezza di qualche \%);
\item \ul{posizione:} la posizione del punto rispetto all'asse $V_y$ quando non c'è tensione $V_y$;
\item \ul{scala verticale} (V/div, ``Volt a divisione''): si può variare tramite un \textbf{attenuatore calibrato}, che ha due manopole: una gira a scatti (con incertezza di qualche \%), l'altra gira in modo continuo ma non è tarata;
\item \ul{impedenza e capacità di ingresso:} le grandezze elettriche del modello equivalente (vd. sezione~\ref{sez:stadio_ingresso});
\item \ul{banda:} nel diagramma di Bode dell'oscilloscopio guadagno (modulo)/frequenza, i segnali ad alta frequenza vengono distorti, in particolare attenuati in guadagno $\rightarrow$ la banda passante è la frequenza per cui si ha un'attenuazione di $-3$ dB ($-30\%$) rispetto al guadagno che si trova alle basse frequenze;
\item \ul{tempo di risposta/salita:} tipicamente nelle funzioni a gradino {\small (un interruttore accende/spegne un segnale periodico)}, è nel grafico segnale in uscita/tempo l'intervallo di tempo per passare dal 10\% al 90\% del valore finale di oscillazione:
\[
T_{\text{salita}} = \frac{0,35}{\text{banda}}
\]
\end{itemize}

\subsection{Asse orizzontale}
\begin{itemize}
\item \ul{velocità di scansione:} tramite il \textbf{regolatore tarato} (due manopole), si può spostare l'asse dei tempi lungo l'asse $x$;
\item \ul{posizione:} la posizione del punto rispetto all'asse $V_x$ all'inizio della spazzolata (consigliato a sinistra dello schermo).
\end{itemize}

\section{Sincronizzazione}
La traccia ha una persistenza di pochi secondi $\rightarrow$ per vederla stabile bisogna dare continue spazzolate sull'asse orizzontale (il segnale in ingresso è periodico).

Le spazzolate vanno date in determinati istanti in modo che la traccia sia stabile e la spazzolata sia sincrona con la precedente.

Il pennello elettronico deve partire quando il segnale da visualizzare:
\begin{itemize}
 \item assume un valore prefissato;
 \item ha la derivata di un determinato segno (pendenza).
\end{itemize}

La rampa parte all'\textbf{impulso di trigger} (``grilletto''). L'estremo sinistro della spazzolata si dice \textbf{punto di trigger}.

Durante la spazzolata il trigger è accecato: la spazzolata continua fino alla fine, senza dare altri impulsi di trigger se $V_y$ passa nuovamente per il punto di trigger. L'\textbf{hold off} fa sì che il trigger rimane accecato anche per un certo tempo dopo la fine della rampa.

\subsection{Parametri del trigger}
\begin{itemize}
\item \ul{livello di trigger:} tensione a cui far partire l'impulso (potenziometro)
\item \ul{pendenza} (slope) \ul{della tensione} (commutatore $+$/$-$)
\end{itemize}

\subsection{Tipi di trigger}
Sul pannello anteriore dell'oscilloscopio è presente un commutatore per impostare il segnale da triggerare per far partire la rampa: (il segnale visualizzato è sempre $V_y$)
\begin{itemize}
\item \textbf{INT} (internal): si triggera lo stesso segnale $V_y$;
\item \textbf{EXT} (external): si triggera il segnale proveniente dal connettore posto sul pannello posteriore, usato per sincronizzare più strumenti;
\item \textbf{LINE:} si triggera il segnale sinusoidale di rete, cioè la corrente a 220 V con cui è alimentato l'oscilloscopio.
\end{itemize}

\subsection{Correzioni livello di trigger}
Per ovviare agli errori di livello di trigger, esiste un auto trigger chiamato \textbf{gate}, corrispondente a un commutatore con tre posizioni:
\begin{itemize}
\item \textbf{normal:} il trigger parte solo quando riceve il segnale;
\item \textbf{auto:} se il trigger non comanda la spazzolata entro un certo tempo, fornisce comunque una spazzolata ogni tanto, che però non è sincronizzata;
\item \textbf{single:} il trigger parte una sola volta $\Rightarrow$ serve per vedere i segnali non periodici (es. gradino).
\end{itemize}

\section{Oscilloscopi a tracce multiple}
L'oscilloscopio a tracce multiple permette di visualizzare sullo schermo più segnali d'ingresso per rilevarne la differenza di fase, cioè la differenza temporale per esempio tra i punti di trigger. Normalmente sono 2 o al massimo 4, per non generare confusione. Si usa un unico punto luminoso per tutte le tracce.

\subsection{Rappresentazione alternate}
La rappresentazione alternate conviene per alte frequenze.

In ogni spazzolata si disegnano alternativamente una sola delle due tracce, tramite un commutatore.

\paragraph{Modalità di trigger}
\begin{enumerate}
\item si triggera uno solo dei due segnali $\Rightarrow$ utile se i due segnali sono sincronizzati tra di loro, cioè sono isofrequenziali $\Rightarrow$ la \textbf{relazione di fase} (cioè differenza di fase nulla) tra i due segnali viene conservata sullo schermo;
\item si triggerano alternativamente entrambi i segnali $\Rightarrow$ utile se i due segnali non sono isofrequenziali, ma bisogna fare attenzione a non perdere la relazione di fase, impostando un opportuno punto di trigger per il secondo segnale.
\end{enumerate}

\subsection{Rappresentazione chopped}
La rappresentazione chopped conviene per basse frequenze.

In una stessa spazzolata si disegnano entrambe le tracce, ciascuna disegnata alternativamente in brevissimi trattini $\Rightarrow$ se la frequenza di commutazione tra le due tracce è abbastanza piccola, le tracce sembrano continue. Come nella seconda modalità di rappresentazione alternate, non è garantita la relazione di fase.

\section{Stadio di ingresso}
\label{sez:stadio_ingresso}
L'oscilloscopio è un carico passivo nei confronti del circuito, e può essere visto dall'esterno come il modello equivalente parallelo resistenza-condensatore con impedenza di $\tfrac{1 \; M \Omega}{10 \; PF}$ (con incertezza di qualche \%). Ai connettori BNC\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Connettore_BNC}{Connettore BNC} su Wikipedia in italiano.} relativi ai segnali in ingresso (${V_y}_A$, ${V_y}_B$, EXT\textellipsis ), che si trovano sul pannello dell'oscilloscopio, si collegano dei cavi coassiali\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Cavo_coassiale}{Cavo coassiale} su Wikipedia in italiano.}. Il cavo coassiale può essere visto in un modello elettrico semplificato, composto da una parte resistiva trascurabile e una parte con capacità dell'ordine di grandezza di quella dell'oscilloscopio. Per ragioni di sicurezza:
\begin{itemize}
\item cavo coassiale: collegata alla massa vi è una ``calza'' di separazione tra il filo di rame e un certo materiale sintetico;
\item connettore BNC: la parte centrale è circondata da una ``ghiera'' collegata alla carcassa dell'oscilloscopio, a sua volta collegata alla terra attraverso un terzo filo giallo-verde.
\end{itemize}

\section{Esercitazione}
Lo scopo dell'esercitazione è imparare a usare l'oscilloscopio. La prima cosa da fare è chiudere l'interruttore differenziale e accendere l'oscilloscopio per evitare il problema di warm-up. L'oscilloscopio riceve i segnali da un sintetizzatore realizzato su scheda, a sua volta collegato a un alimentatore doppio (due tensioni) o triplo (tre tensioni).

\subsection{Alimentatore}
L'alimentatore presenta in alto degli indicatori della tensione e della corrente erogate, e nella parte centrale dei connettori per le due tensioni $+12$ V e $-12$ V; ai lati di questi connettori, delle manopole impongono una corrente e una tensione massime per motivi di sicurezza.

\subsection{Scheda}
La scheda può generare segnali con diversi tipi di forme d'onda, che possono essere commutati con i pulsanti Up e Down. Un cavo coassiale BNC collega l'uscita A della scheda con il primo canale dell'oscilloscopio.

\subsection{Oscilloscopio}
Inizialmente, è consigliabile porre il gate in auto, e agire sulla intensità e sul fuoco per regolare la visualizzazione della traccia.

Figura 18: accanto al connettore BNC maschio, vi è un commutatore per determinare il segnale di ingresso:
\begin{itemize}
 \item DC: il segnale di ingresso non viene filtrato e arriva direttamente all'oscilloscopio (uso normale);
 \item AC: viene introdotto un filtro passa-alto (da non usare);
 \item GD (ground): l'ingresso è in cortocircuito $\rightarrow$ segnale 0 (serve per regolare la posizione $y$).
\end{itemize}

Il tasto INV cambia segno al segnale: un segnale $+$/$-$ viene visualizzato come $-$/$+$; si può usare per fare la somma o la differenza {\small (a uno dei due si applica INV)} dei due segnali.

\subsection{Misurazioni}
\begin{itemize}
\item \ul{frequenza:} Dato un segnale sinusoidale, si imposta il trigger secondo istruzioni, quindi effettua la misurazione del periodo del segnale, cioè l'intervallo di tempo tra due punti che hanno lo stesso valore e la stessa pendenza (tipicamente il punto 0, che dovrebbe essere impostato sulla pendenza massima). Viene calcolato il periodo tramite la formula $T=K_o \cdot n_{\text{div}}$, quindi si fa l'inverso per ricavare la frequenza. Truccone: c'è il pulsante autoset che visualizza automaticamente la frequenza!
\item \ul{duty cycle} di un segnale a impulso: è la percentuale dell'intervallo di tempo $T_H$ in cui il segnale è a picco rispetto all'intero periodo $T$.
\end{itemize}

\subsection{Cursori}
L'oscilloscopio può calcolare automaticamente l'intervallo di tempo (impostando le barre verticalmente) o la differenza di tensione (orizzontalmente) tra due cursori (= barre tratteggiate sullo schermo).