\chapter{L'amplificatore operazionale reale}
Un \textbf{amplificatore operazionale reale} si differenzia dall'ideale per le seguenti condizioni di non idealità:
\begin{itemize}
\item il guadagno differenziale $A_d$ è grande ma non infinito, e il guadagno di modo comune $A_C$ è piccolo ma non identicamente nullo;
\item le grandezze elettriche $V_d$, $i_+$, $i_-$ e $R_u$ sono piccole ma non identicamente nulle;
\item il grafico della transcaratteristica $V_u \left( V_d \right)$ non è lineare $\Rightarrow$ non si può applicare il principio della sovrapposizione degli effetti;
\item la dinamica di uscita è limitata principalmente dalle tensioni di alimentazione;
\item la banda passante è limitata $\Rightarrow$ l'amplificazione $A_d$ si riduce a frequenze $\omega$ grandi;
\item il comportamento dell'amplificatore è influenzato anche da parametri esterni (temperatura, tensioni di alimentazione\textellipsis).
\end{itemize}

\section{Guadagno differenziale}
\subsection{\texorpdfstring{$A_d$}{Ad} finito: effetto su \texorpdfstring{$A_V$}{AV}}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/06/Operational_amplifier_with_non-ideal_differential_gain.png}
\end{figure}

\noindent
Un guadagno differenziale $A_d$ non infinito comporta una riduzione $\varepsilon_G$ delle prestazioni dell'amplificatore dell'ordine del reciproco del guadagno di anello $T= \beta A_d$:
\[
\begin{cases} \displaystyle V_E = \frac{R_2}{R_1+R_2} V_u = \beta V_u \\
\displaystyle V_d = V_i - V_E {\color{red} \neq 0} \end{cases} \Rightarrow \begin{cases} \displaystyle V_d = V_i - \beta V_u \\
\displaystyle V_u = A_d V_d \end{cases} \Rightarrow
\]
\[
\Rightarrow V_i = V_u \left( {\color{red} \frac{1}{A_d}} + \beta \right) \Rightarrow A_V = \frac{V_u}{V_i} = \frac{1}{\beta} \cdot {\color{red} \frac{1}{1+ \frac{1}{T}}} \approx \frac{1}{\beta} \left( 1 {\color{red} - \frac{1}{T}} \right)
\]
\FloatBarrier

\section{Resistenze interne}
\subsection{\texorpdfstring{$A_d$}{Ad} finito + \texorpdfstring{${R_i}_d$}{Rid} finita: effetto su \texorpdfstring{$R_i$}{Ri}}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/06/Operational_amplifier_with_non-ideal_input_resistance.png}
\end{figure}

\noindent
Ponendo internamente in ingresso una \textbf{resistenza differenziale} ${R_i}_d$ finita e applicando un segnale differenziale $V_d$, si determina una corrente $I_i \neq 0$ che passa dal morsetto non invertente a quello invertente. Il suo effetto sulla resistenza $R_i$ vista complessivamente ai terminali di ingresso è però trascurabile anche per bassi valori di ${R_i}_d$ e $T$:
\[
{R_i}_d= \frac{V_d}{I_i} {\color{red} < + \infty} \Rightarrow R_i= \beta R_1+ {R_i}_d \left( 1+T \right) \simeq {R_i}_d \left( 1+T \right)
\]

A guadagno differenziale $A_d$ idealmente infinito, la resistenza di ingresso $R_i$ è ricondotta al caso ideale ($R_i \rightarrow + \infty$) indipendentemente dalla resistenza differenziale ${R_i}_d$.\footnote{Non si considerano mai resistenze differenziali ${R_i}_d$ troppo vicine al cortocircuito.}
\FloatBarrier

\subsection{\texorpdfstring{$A_d$}{Ad} finito + \texorpdfstring{$R_o$}{Ro} non nulla: effetto su \texorpdfstring{$R_u$}{Ru}}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/06/Operational_amplifier_with_non-ideal_output_resistance.png}
\end{figure}

\noindent
Una resistenza $R_o$ non nulla, posta in uscita in serie al generatore pilotato interno, ripartisce la tensione di quest'ultimo influenzando in modo limitato la resistenza di uscita $R_u$:\footnote{Si noti che $R_u$ è la resistenza vista complessivamente ai terminali di uscita quando i generatori indipendenti sono spenti $\Rightarrow$ $V_i = 0$.}
\[
V_u=A_d V_d {\color{red} + \left( I_u-I_R \right) R_o }  \Rightarrow R_u= \frac{R_o}{T+1+ \frac{R_o}{R_1+R_2 }} \simeq \frac{R_o}{T}
\]

A guadagno differenziale $A_d$ idealmente infinito, la resistenza di uscita $R_u$ è ricondotta al caso ideale ($R_u=0$) indipendentemente dalla resistenza $R_o$.
\FloatBarrier

\section{Tensione in ingresso}
\subsection{\texorpdfstring{$V_{\text{off}}$}{Voff} non nulla: effetto su \texorpdfstring{$V_u$}{Vu}}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.4\linewidth]{pic/06/Operational_amplifier_with_non-ideal_input_voltage.png}
\end{figure}

Assumiamo che il modulo abbia una transcaratteristica lineare, ma non ideale perché si discosta dall'origine per i due offset $\Delta U$ e $\Delta I$.

La \textbf{tensione di offset} $V_{\text{off}} =\Delta I$ si può interpretare/modellizzare circuitalmente come un generatore di tensione aggiuntivo in serie all'ingresso, che riduce dello stesso offset la tensione in ingresso:
\[
V_d \rightarrow 0 \Rightarrow V_i {\color{red} - V_{\text{off}}} =V_E
\]

A ingresso $V_i$ nullo:
\[
V_i=0 \Rightarrow V_u=- V_{\text{off}}  \left( 1+ \frac{R_1}{R_2}  \right) {\color{red} \neq 0}
\]

Il costruttore del modulo si limita a specificare la tensione di offset massima senza segno (l'estremo superiore del suo valore assoluto).

\section{Correnti in ingresso}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/06/Operational_amplifier_with_non-ideal_input_current.png}
\end{figure}

\noindent
Le correnti d'ingresso $I_+$ e $I_-$ sono definite in funzione della \textbf{corrente di bias} $I_b$ (modo comune) e della \textbf{corrente di offset} $I_{\text{off}}$ (modo differenziale):
\[
\begin{cases} \displaystyle I_b = \frac{I_+ + I_-}{2} \\
\displaystyle I_{\text{off}}  = I_+ - I_- \end{cases}
\]

Il costruttore fornisce il segno solo della corrente di bias; il segno della corrente di offset non è noto.
\FloatBarrier

\subsection{\texorpdfstring{$I_+$}{I+} e \texorpdfstring{$I_-$}{I-} non nulle: effetto su \texorpdfstring{$V_u$}{Vu}}
Le non idealità delle correnti $I_+$ e $I_-$ si possono modellizzare con generatori di corrente uscente dalle relative linee in ingresso. Applicando il principio di sovrapposizione degli effetti su ciascun generatore, si porta a zero la corrente in ingresso:
\begin{itemize}
\item $I_- \neq 0$: la resistenza $R_3$ non è attraversata da corrente $I_+$ $\Rightarrow$ $V_-=V_+=V_{R_1}=0$ $\Rightarrow$ la corrente scorre solamente su $R_2$:
\[
{V_u}_- = R_2 I_-
\]
\item $I_+ \neq 0$: il parallelo resistenza $R_3$-generatore di corrente $I_+$ equivale alla serie resistenza-generatore di tensione $R_3 I_+$ $\Rightarrow$ diventa un amplificatore di tensione non invertente reazionato, con tensione di uscita:
\[
{V_u}_+ = -R_3 I_+ \left( \frac{R_2}{R_1} +1 \right)
\]

Le correnti $I_+$ e $I_-$ non nulle danno alla tensione di uscita un contributo complessivo:
\[
V_u= {V_u}_+ + {V_u}_- =R_2 I_- - R_3 I_+ \left( \frac{R_2}{R_1} +1 \right)
\]
\item l'effetto della corrente di bias $I_b$ può essere annullato se la resistenza $R_3$ è uguale al parallelo tra $R_1$ e $R_2$:
\[
I_{\text{off}} =0 \Rightarrow V_u=I_b \left[ R_2-R_3 \left( \frac{R_2}{R_1} +1 \right) \right] =0 \Leftrightarrow R_3=R_1 || R_2
\]
\item l'effetto della corrente di offset $I_{\text{off}}$ non può essere annullato per alcun valore di resistenza $R_3$ (tranne se $R_2 \gg 0$):
\[
I_b=0 \Rightarrow \left| V_u \right| = \frac{1}{2} \left| I_{\text{off}}  \right| \left[ R_2+R_3 \left( \frac{R_2}{R_1} +1 \right) \right]
\]
\end{itemize}

\section{Limiti di dinamica}
\subsection{Dinamica di uscita}
L'intervallo dei possibili valori di tensione di uscita $V_u$ è limitato da:
\begin{itemize}
\item le tensioni di alimentazione $V_{AL+}$ e $V_{AL-}$;
\item la resistenza interna di uscita $R_o$ non nulla, perché su di essa vi è una caduta di potenziale (non indipendente dal carico).
\end{itemize}

\subsection{Dinamica di ingresso}
Anche il segnale di ingresso non è illimitato:
\begin{itemize}
\item \ul{dinamica di modo comune $\Delta V_C$:} è limitata dalle tensioni di alimentazione $V_{AL+}$ e $V_{AL-}$;
\item \ul{dinamica differenziale $\Delta V_d$:} è legata alla dinamica di uscita: $\Delta V_d= \tfrac{\Delta V_u}{A_d}$.
\end{itemize}

Valori di tensione in ingresso al di fuori della dinamica possono danneggiare il circuito. La restrizione $\Delta \text{MC}$ della dinamica di modo comune comprende valori di tensione lontani dalle tensioni di alimentazione che, oltre a non danneggiare il circuito, garantiscono il suo corretto funzionamento.

\subsection{Transcaratteristica}
La transcaratteristica $V_u \left( V_d \right)$ dell'amplificazione operazionale ideale è una retta verticale poiché $A_d \to + \infty$.

Si discosta dalla idealità per l'amplificazione $A_d$ finita, la dinamica di uscita $\Delta V_u$ finita (che provoca una saturazione quando $V_u$ si avvicina alle tensioni di alimentazione), le tensioni di offset ${V_i}_{\text{off}}$ e ${V_u}_{\text{off}}$, e la corrente che determina la caduta di potenziale sulla resistenza di uscita interna $R_o$ (specialmente se la resistenza di carico $R_C$ richiede un'elevata corrente).

\section{Moduli funzionali commerciali}
I parametri del modulo sono descritti dal costruttore nel data sheet. Esempi di utilizzo sono contenuti nelle application notes.

Il data sheet non si occupa delle caratteristiche interne, ma descrive il modulo solo ai morsetti.

I valori dei parametri forniti hanno un'imprecisione. Più si vuole precisione, più il costo del componente aumenta.