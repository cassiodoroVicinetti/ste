\chapter{Amplificatore operazionale ideale}
\section{Amplificatore differenziale}
A differenza di un amplificatore standard il cui ingresso $V_i$ è sempre riferito a massa, l'\textbf{amplificatore differenziale} è caratterizzato in ingresso dalla \textbf{tensione differenziale} $V_d$:
\[
V_d = V_+ - V_-
\]
dove $V_+$ è la tensione al terminale non invertente ($+$) e $V_-$ è la tensione al terminale invertente ($-$). La tensione di uscita $V_u$ si può esprimere come la combinazione lineare delle tensioni applicate ai singoli terminali di ingresso, ed è pertanto proporzionale alla tensione differenziale $V_d$ con un \textbf{guadagno differenziale} $A$:
\[
\begin{cases} \displaystyle V_u = A V_+ + B V_- \\
\displaystyle A > 0 , \, A = -B \end{cases} \Rightarrow V_u = A \left( V_+ - V_- \right) = A V_d
\]

\subsection{Amplificatore operazionale ideale}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Ideal_operational_amplifier.png}
\end{figure}

\noindent
Un amplificatore differenziale si dice \textbf{operazionale ideale} (AO) se:
\begin{itemize}
\item la tensione differenziale è proporzionale alla tensione in uscita $V_u$ con un guadagno differenziale $A_d$ infinito:
\[
V_u=A_d V_d  , \quad A_d \rightarrow + \infty
\]

Siccome $V_u$ è una tensione finita, la condizione imposta sul guadagno differenziale implica: $V_d \rightarrow 0$
\item le correnti che scorrono ai terminali di ingresso sono nulle: $i_+=i_-=0$
\item non è presente una resistenza in uscita: $R_u=0$
\end{itemize}
\FloatBarrier

\section{Modello dei sistemi reazionati}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Feedback_system_model.png}
\end{figure}

\noindent
I \textbf{sistemi reazionati} sono basati sul principio della \textbf{reazione negativa} (o feedback):
\begin{enumerate}
\item il segnale di ingresso $i$ viene amplificato nell'uscita $u$ di un fattore $A$: $u=A \cdot d$;
\item l'uscita $u$ viene ridotta nella parte $E$ del fattore di partizione $\beta$: $E= \beta \cdot u$;
\item la parte $E$ viene riportata al morsetto invertente;
\item la parte $E$ viene confrontata con l'ingresso $i$ tramite l'errore $d$, che è la differenza tra l'ingresso $i$ e la parte $E$:
\[
\begin{cases} \displaystyle d = i - E \\
\displaystyle E = \beta \cdot u \end{cases} \Rightarrow \begin{cases} \displaystyle d = i - \beta u \\
\displaystyle u = A \cdot d \end{cases} \Rightarrow u = \frac{A}{1 + A \beta} i
\]
\end{enumerate}

Si parla di reazione negativa perché la parte $E$ dell'uscita $u$, riportata sul morsetto invertente, viene sottratta all'ingresso $i$.

Il \textbf{guadagno di anello} $T=A \beta$ è il contributo del ramo di reazione all'amplificazione del segnale. Se non c'è reazione ($\beta =0$), l'amplificazione si dice \textbf{ad anello aperto}, e l'ingresso $i$ è amplificato esattamente del fattore $A$: $u=A \cdot i$.

\paragraph{Criticità}
Se $A \beta = -1$ l'uscita $u$ diverge $\Rightarrow$ il sistema reazionato diventa \textbf{instabile}.

\subsection{Amplificatori reazionati}
Poiché il guadagno di anello di un amplificatore operazionale ideale è sempre infinito:
\[
A = A_d \rightarrow + \infty \Rightarrow A \beta \rightarrow + \infty
\]
l'amplificatore operazionale ideale rende l'amplificazione $\tfrac{u}{i}$ complessiva del sistema reazionato indipendente dal fattore $A$:
\[
A \beta \rightarrow + \infty \Rightarrow \frac{u}{i} = \frac{A}{1 + A \beta} \simeq \frac{1}{\beta} , \quad \beta < 1 \Rightarrow u > i
\]

Generalmente, le resistenze di ingresso e di uscita si comportano idealmente o da cortocircuiti o da circuiti aperti a seconda se i segnali sono correnti o tensioni.

L'amplificatore è invertente se il segnale viene applicato al morsetto invertente e viceversa, con la convenzione: tensione verso l'uscita dell'operazionale, corrente entrante nell'uscita dell'operazionale.

Se il circuito comprende un solo amplificatore operazionale, affinché sia un circuito amplificatore il ramo di reazione deve essere sempre collegato al morsetto invertente.

Per studiare un circuito con amplificatori operazionali, si possono usare le regole dell'elettrotecnica per eliminare gli elementi che non perturbano il comportamento del circuito (ad es. i resistori in parallelo con generatori di tensione diventano circuiti aperti).

\section{Amplificatore di tensione non invertente (\texorpdfstring{$V \longrightarrow V$}{V V})}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Non-inverting_voltage_amplifier.png}
\end{figure}

L'amplificatore operazionale può essere impiegato per realizzare circuiti amplificatori di tensione, con guadagno $A_V$ assegnato, di tipo reazionato: la parte $E=V_E$ della tensione $u=V_u$ viene riportata al morsetto invertente e confrontata con la tensione $d=V_d$. Le resistenze $R_1$ e $R_2$ sono in serie perché $i_-=0$ $\Rightarrow$ la tensione $V_u$ si ripartisce su di esse:
\[
\begin{cases} \displaystyle V_E = \frac{R_2}{R_1 + R_2} V_u = \beta V_u \\
\displaystyle V_d = V_i + V_E \end{cases} \Rightarrow \begin{cases} \displaystyle V_d = V_i - \beta V_u \\
\displaystyle V_d \rightarrow 0 \end{cases} \Rightarrow \begin{cases} \displaystyle V_u = \frac{1}{\beta} V_i \\
\displaystyle V_u = A_V V_i \end{cases} \Rightarrow A_V = \frac{V_u}{V_i} = \frac{1}{\beta} = 1 + \frac{R_1}{R_2}
\]

\paragraph{Parametri}
Il circuito può quindi essere modellato come il suo doppio bipolo equivalente definito da 3 parametri:
\[
\begin{cases} \displaystyle A_V = 1 + \frac{R_1}{R_2} \\
\displaystyle R_i \rightarrow + \infty \\
\displaystyle R_u \rightarrow 0 \end{cases}
\]

\begin{itemize}
\item la resistenza di ingresso $R_i$ è infinita perché le correnti ai terminali di ingresso sono nulle;
\item la resistenza $R_u$ interna all'amplificatore operazionale ideale è nulla $\Rightarrow$ la tensione $V_u$ non si ripartisce tra i resistori $R_u$ e $R_c$, ma si applica interamente al resistore $R_c$ indipendentemente dal suo valore di resistenza;
\item l'amplificazione $A_V$ del circuito è minore di quella che avrebbe l'amplificatore operazionale ideale se preso singolarmente, ma ci sono dei vantaggi:
\begin{itemize}
\item l'amplificazione è indipendente dalle caratteristiche dell'amplificatore operazionale;
\item l'amplificazione dipende solo dal rapporto delle resistenze $R_1$ e $R_2$ $\Rightarrow$ è indipendente dai singoli valori di resistenza purché il loro rapporto sia mantenuto;
\item il sistema reazionato ha migliori prestazioni e una maggiore stabilità.
\end{itemize}
\end{itemize}

\subsection{Inseguitore di tensione}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Voltage_follower.png}
\end{figure}

\noindent
L'\textbf{inseguitore di tensione} (o voltage follower) è un buffer\footnote{Un \textbf{buffer} permette di trasferire tutto il segnale di ingresso sul carico indipendentemente dal suo valore di resistenza.} che trasferisce tutta la tensione $V_i$ sul carico $R_c$ indipendentemente dal suo valore di resistenza.

\paragraph{Resistenze}
La resistenza di carico $R_c$ e la resistenza $R_S$ del generatore reale sono disaccoppiate:
\begin{itemize}
\item la tensione $V_S$ del generatore non viene ripartita in $V_i$ e non dipende dalla resistenza interna $R_S$:
\[
\begin{cases} \displaystyle V_i = V_S \frac{R_i}{R_i + R_S} \\
\displaystyle R_i \rightarrow + \infty \end{cases} \Rightarrow V_i = V_S
\]
\item la resistenza $R_u$ è nulla $\Rightarrow$ la tensione $V_u$ non si ripartisce tra i resistori $R_u$ e $R_c$, ma si applica interamente al resistore $R_c$ indipendentemente dal suo valore di resistenza.
\end{itemize}

\paragraph{Tensioni}
L'inseguitore può essere visto come un amplificatore con guadagno $A_V$ unitario, in cui tutta la tensione di uscita $V_u$ viene riportata attraverso il terminale invertente a quella d'ingresso $V_i$:
\[
\begin{cases} \displaystyle A_V = \frac{1}{\beta} = 1 + \frac{R_1}{R_2} \\
\displaystyle R_1 = 0 \wedge R_2 \rightarrow + \infty \end{cases} \Rightarrow \begin{cases} \beta = 1 \\
\displaystyle V_d = V_i - \beta V_u \rightarrow 0 \end{cases} \Rightarrow \begin{cases} V_i = V_u \\
\displaystyle A_V = 1 \end{cases}
\]
\FloatBarrier

\section{Amplificatore di transresistenza invertente (\texorpdfstring{$I \longrightarrow V$}{I V})}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Inverting_transresistance_amplifier.png}
\end{figure}

\noindent
Il generatore di corrente è disaccoppiato dal carico:
\begin{itemize}
\item la resistenza d'ingresso $R_i$ è idealmente nulla:
\[
\begin{cases} \displaystyle R_i = \frac{V_-}{I_i} = \frac{- V_d}{I_i} \\
\displaystyle V_d \rightarrow 0 \end{cases} \Rightarrow R_i  = 0
\]
\item la resistenza $R_u$ è nulla $\Rightarrow$ la tensione $V_u$ non si ripartisce tra i resistori $R_u$ e $R_c$, ma si applica interamente al resistore $R_c$ indipendentemente dal suo valore di resistenza.
\end{itemize}

Se il generatore non fosse disaccoppiato dal carico, il generatore invierebbe corrente verso un circuito aperto. È un amplificatore invertente perché ha una transresistenza $R_m$ negativa:
\[
\begin{cases} \displaystyle V_u = - V_d - R_M I_M \\
\displaystyle V_d \rightarrow 0 \end{cases} \Rightarrow \begin{cases} \displaystyle V_u = - R_M I_M \\
\displaystyle I_- = 0 \Rightarrow I_M = I_i \end{cases} \Rightarrow R_m = \frac{V_u}{I_i} = - R_M < 0
\]
\FloatBarrier

\subsection{Fotorivelatore}
In un \textbf{fotorivelatore} le correnti e la tensione $V_u$ sono proporzionali all'intensità $L$ della radiazione luminosa incidente:
\[
I_i=I_M=KL \Rightarrow V_u=-R_M I_i=-KR_M L
\]
dove $K$ è una proprietà intrinseca del fotorivelatore.

\section{Amplificatore di tensione invertente (\texorpdfstring{$V \longrightarrow V$}{V V})}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Inverting_voltage_amplifier.png}
\end{figure}

\noindent
Il generatore di tensione d'ingresso è applicato al morsetto invertente anziché a quello non invertente.

\paragraph{Parametri}
\begin{itemize}
\item \ul{amplificazione $A_V$:} anche in questo caso dipende solo dal rapporto tra le due resistenze $R_1$ e $R_2$:
\[
\begin{cases} \displaystyle I_1 = \frac{V_i - V_d}{R_1} \\
\displaystyle V_d \rightarrow 0 \end{cases} \Rightarrow \begin{cases} \displaystyle I_1 = \frac{V_i}{R_1} \\
\displaystyle I_-  = 0 \Rightarrow  I_1 = I_2 \end{cases} \Rightarrow \begin{cases} \displaystyle I_2 = \frac{V_i}{R_1} \\
\displaystyle V_d \rightarrow 0 \Rightarrow V_u = - R_2 I_2 \end{cases} \Rightarrow A_V = \frac{V_u}{V_i} = - \frac{R_2}{R_1}
\]
\item \ul{resistenza $R_i$:} coincide con la resistenza $R_1$ $\Rightarrow$ non rientra nei casi ideali;
\item \ul{resistenza $R_u$:} è quella nulla dell'amplificatore operazionale ideale, perché l'uscita è indipendente dal carico.
\end{itemize}
\FloatBarrier

\subsection{Integratore attivo}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Active_integrator.png}
\end{figure}

\noindent
L'\textbf{integratore attivo} è un amplificatore di tensione invertente il cui amplificatore operazionale è un circuito attivo: la resistenza di reazione è sostituita da un condensatore avente ai capi una tensione $V_u$:
\[
\begin{cases} \displaystyle - V_u \left( s \right) = \frac{1}{sC} \cdot I_2 \left( s \right) \\
\displaystyle I_2 \left( s \right) = I_1 = \frac{V_i \left( s \right)}{R} \end{cases} \Rightarrow \begin{cases} \displaystyle V_u \left( s \right) = - \frac{1}{RC} \cdot \frac{V_i \left( s \right)}{s} \\
\displaystyle {\mathcal{L}}^{-1} \left[ \frac{X \left( s \right)}{s} \right] \left( t \right) = \int_0^t x \left( \alpha \right) d \alpha \end{cases} \Rightarrow
\]
\[
\Rightarrow V_u \left( t \right) = - \frac{1}{RC} \cdot {\mathcal{L}}^{-1} \left[ \frac{V_i \left( s \right)}{s} \right] \left( t \right) = - \frac{1}{RC} \cdot \int_0^t V_i \left( \alpha \right) d \alpha
\]
\FloatBarrier

\subsection{Derivatore attivo}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Active_differentiator.png}
\end{figure}

\[
V_u = - R I_1 = - s C R V_i \Rightarrow V_u \left( t \right) = - RC \frac{d V_i}{dt}
\]
\FloatBarrier

\section{Amplificatore di corrente non invertente (\texorpdfstring{$I \longrightarrow I$}{I I})}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Non-inverting_current_amplifier.png}
\end{figure}

\noindent
L'uscita in corrente richiede che l'impedenza di carico $Z_c$ sia posta in serie all'uscita dell'amplificatore operazionale.

\paragraph{Parametri}
\begin{itemize}
\item \ul{amplificazione $A_i$:} è indipendente dall'impedenza $Z_c$:
\[
\begin{cases} \displaystyle I_- = 0 \Rightarrow I_i = I_1 \\
\displaystyle V_d \rightarrow 0 \Rightarrow R_1 I_1 = R_2 I_2  \\
\displaystyle I_u = I_1 + I_2 \end{cases} \Rightarrow A_i = \frac{I_u}{I_i} = 1 + \frac{R_1}{R_2}
\]
\item \ul{resistenza $R_i$:} è nulla $\Rightarrow$ non rientra nei casi ideali;
\item \marginpar{da chiarire}\ul{resistenza $R_u$:} $I_1$ e $I_2$ non dipendono da $Z_c$ $\Rightarrow$ la corrente $I_u$ non dipende dal valore di carico $Z_c$ $\Rightarrow$ la resistenza $R_u$ è infinita.
\end{itemize}
\FloatBarrier

\subsection{Inseguitore di corrente}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.45\linewidth]{pic/04/Current_follower.png}
\end{figure}

L'\textbf{inseguitore di corrente} è un buffer che, analogamente all'inseguitore di tensione, trasferisce tutta la corrente $I_i$ sul carico $Z_c$ indipendentemente dal suo valore di resistenza.

\paragraph{Resistenze}
La resistenza di carico $Z_c$ e la resistenza $R_S$ del generatore reale sono disaccoppiate:
\begin{itemize}
\item la corrente $I_i$ del generatore non viene ripartita in $I_1$ e non dipende dalla resistenza interna $R_S$:
\[
\begin{cases} \displaystyle I_1 = I_i \frac{G_i}{G_i + G_S} \\
\displaystyle R_i = 0 \Rightarrow G_i \rightarrow + \infty \end{cases} \Rightarrow I_1 = I_i
\]
\item la resistenza $R_u$ interna all'amplificatore operazionale ideale è infinita $\Rightarrow$ la corrente $I_u$ non si ripartisce tra i resistori $R_u$ e $Z_c$, ma attraversa interamente il resistore $Z_c$ indipendentemente dal suo valore di resistenza.
\end{itemize}

\paragraph{Correnti}
L'inseguitore può essere visto come un amplificatore con guadagno $A_i$ unitario:
\[
\begin{cases} \displaystyle A_V = \frac{I_u}{I_i} = 1 + \frac{R_1}{R_2} \\
\displaystyle R_1 = 0 \wedge R_2 \rightarrow + \infty \end{cases} \Rightarrow \begin{cases} \displaystyle I_i = I_u \\
\displaystyle A_V = 1 \end{cases}
\]

\section{Amplificatore di transconduttanza non invertente (\texorpdfstring{$V \longrightarrow I$}{V I})}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.33\linewidth]{pic/04/Non-inverting_transconductance_amplifier.png}
\end{figure}

\paragraph{Parametri}
\begin{itemize}
\item \ul{transconduttanza $G_m$:}
\[
\begin{cases} \displaystyle I_- = 0 \Rightarrow I_u = I_S = \frac{V_S}{R_S} \\
\displaystyle V_d \rightarrow 0 \Rightarrow V_S = V_i \end{cases} \Rightarrow G_m = \frac{I_u}{V_i} = \frac{1}{R_S}
\]
\item \ul{resistenza $R_i$:} la tensione del generatore non viene ripartita in $V_i$:
\[
\begin{cases} \displaystyle R_i = \frac{V_i}{I_i} \\
\displaystyle I_i = I_+ = 0 \end{cases} \Rightarrow R_i \rightarrow + \infty
\]
\item \ul{resistenza $R_u$:} la corrente $I_u$ è uguale alla corrente $I_S$ indipendentemente dal valore di carico $R_c$ $\Rightarrow$  la resistenza $R_u$ è infinita.
\end{itemize}