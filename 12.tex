\chapter{Uso in commutazione dei transistori}
I transistori sono usati in commutazione per pilotare \textbf{carichi di potenza} (attuatori), cioè dei carichi che richiedono che scorra al loro interno una corrente di valore significativo, che i \textbf{circuiti digitali} commutano tra on e off attraverso gli interruttori.

I circuiti logici possono basarsi su:
\begin{itemize}
\item logiche a giunzioni \textit{pn}: diodi e/o transistori bipolari (in disuso);
\item logiche a transistori MOSFET: \textit{n}MOS o CMOS.
\end{itemize}

Un carico di potenza è caratterizzato da tensione di lavoro, corrente, tipo (resistivo o reattivo), velocità di commutazione e tipo di interruttore.

L'interruttore può assumere due stati: ON quando è chiuso e approssima il cortocircuito, OFF quando è aperto e approssima il circuito aperto. Le non idealità si rappresentano circuitalmente con la \textbf{resistenza} $R_{\text{on}}$ in stato ON (idealmente nulla) e con un generatore di \textbf{corrente di perdita} $I_{\text{off}}$ (idealmente nulla).

Gli interruttori si distinguono in:
\begin{itemize}
\item \textbf{high-side:} l'interruttore si trova tra la tensione di alimentazione $V_{AL}$ e carico $Z_L$;
\item \textbf{low-side:} l'interruttore si trova tra il carico $Z_L$ e massa ($V=0$).
\end{itemize}

\section{Interruttori low-side}
\subsection{Interruttore low-side a transistore bipolare}
Il circuito impone una retta di carico alla porta di uscita (collettore):
\[
V_{AL} = Z_L I_C + V_{CE}
\]

Il transistore bipolare può essere usato nelle applicazioni digitali commutando, attraverso la scelta della corrente di ingresso $I_B$, tra la regione di saturazione ($V_{CE} = {V_{CE}}_{\text{sat}} \approx 0$) e quella di interdizione ($I_C=0$). Il punto di funzionamento $\left( {I_C}_0 , {V_{CE}}_0 \right)$ del transistore è l'intersezione tra la retta di carico e la caratteristica di uscita $I_C= \beta_F I_B$:
\begin{itemize}
\item interdizione (circuito aperto):
\[
I_B \approx 0 \Rightarrow \begin{cases} \displaystyle {I_C}_0 \approx 0 \\ \displaystyle {V_{CE}}_0 \approx V_{AL} \end{cases}
\]
\item saturazione/conduzione (cortocircuito):
\[
I_B \geq \frac{ {I_C}_0}{\beta_F} \Rightarrow \begin{cases} \displaystyle {I_C}_0 = \frac{V_{AL} - {V_{CE}}_{\text{sat}}}{Z_L} \approx \frac{V_{AL}}{Z_L} \\ \displaystyle {V_{CE}}_0 \approx {V_{CE}}_{\text{sat}} \end{cases}
\]
\end{itemize}

Più la corrente $I_B$ è elevata, più si garantisce che la tensione ${V_{CE}}_0$ sia compresa nella regione di saturazione (${V_{CE}}_0 < {V_{CE}}_{\text{sat}}$), e più è bassa la potenza dissipata $P= {V_{CE}}_0 \cdot {I_C}_0 \leq {V_{CE}}_0 \cdot \left( {\beta_F}_{\text{min}}  I_B \right)$ $\Rightarrow$ per massimizzare la corrente $I_B$ così da minimizzare la tensione ${V_{CE}}_0$, conviene scegliere l'estremo inferiore ${\beta_F}_{\text{min}}$ della fascia di incertezza di $\beta_F$.

La corrente di ingresso $I_B$, tuttavia, è piccola poiché proviene da un circuito digitale $\Rightarrow$ con un solo transistore non è possibile realizzare un interruttore che abbia un guadagno sufficientemente significativo senza dissipare troppa potenza.

\subsection{Interruttore low-side a transistori bipolari in configurazione Darlington}
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{pic/12/BJT-transistor_low-side_switch.png}
\end{figure}

\noindent
Due transistori bipolari sono collegati in \textbf{coppia Darlington} se la corrente di emettitore di uno è la corrente di base dell'altro. Il guadagno complessivo è il prodotto dei due guadagni parziali:
\[
I_C = \left[ \beta_2 \left( \beta_1 + 1 \right) + \beta_1 \right] I_B \approx \beta_1 \beta_2 I_B
\]

Si possono anche collegare in cascata più di due transistori bipolari, anche in configurazioni miste.

Il \textbf{tempo di commutazione} è il tempo richiesto da un transistore bipolare per passare tra una buona approssimazione del circuito aperto e una buona approssimazione del cortocircuito. In particolare, spesso è significativo il tempo di commutazione di spegnimento, cioè il tempo di apertura dell'interruttore: poiché il transistore passa dalla regione di saturazione a quella di interdizione ($I_C=0$), deve infatti passare un certo tempo affinché si riduca l'eccesso di portatori minoritari $n_p'$ nella regione di svuotamento (\textbf{transitorio di spegnimento}).

La corrente di base $I_B$, che allontana gli elettroni in eccesso, è però molto bassa poiché arriva dal circuito digitale. Nella coppia Darlington, si collega un resistore $R_E$ tra base e massa che aumenta la corrente per accelerare il tempo di commutazione di spegnimento del transistore $T_2$.
\FloatBarrier

\subsection{Interruttore low-side a transistore \textit{n}MOS}
Il transistore \textit{n}MOS si comporta da interruttore aperto in regione di interdizione ($V_{GS} < V_{th}$) $\Rightarrow$ la tensione di soglia $V_{th}$ deve frapporsi a metà tra gli stati logici 0 e 1. Per il gate del transistore è sufficiente la corrente bassa proveniente dal circuito digitale.

Se il circuito digitale è collegato in maniera diretta al gate, però, il circuito risonante formato dalla capacità equivalente del gate $C_G$ e da induttanze parassite può far oscillare la tensione rendendola instabile o superandone il valore massimo (breakdown) $\Rightarrow$ la resistenza $R_G$ aumenta il fattore di smorzamento $\gamma$:
\[
\gamma = \frac{R}{2} \sqrt{\frac{C}{L}}
\]
e riduce il fattore di qualità $Q= \frac{1}{2} \gamma$, ma aumenta il tempo di commutazione perché rallenta il transitorio di carica e scarica del condensatore $C_G$.

\section{Interfacce a componenti discreti}
Poiché i circuiti digitali operano con correnti molto basse e non sono adatti a pilotare direttamente carichi di potenza, le \textbf{interfacce a componenti discreti} sono dei circuiti \textbf{MOS driver} che si occupano di amplificare il comando del circuito digitale in modo da fornire un valore massimo di corrente sufficiente per pilotare il carico di potenza.

Nel caso di interruttore a MOS, il tempo di commutazione $t$ dipende da quanto viene caricata rapidamente la capacità di gate $C_G$:\footnote{Nell'esempio si suppone una corrente $I$ costante; in caso contrario, la tensione $v(t)$ varia con un andamento esponenziale.}
\[
I = C \frac{dv}{dt} \Rightarrow \Delta v = v(t) - v_0 = \frac{1}{C} \int_0^t I dt = \frac{1}{C} I \cdot t \Rightarrow t = C \frac{\Delta v}{I}
\]

L'aggiunta di un MOS driver amplifica la corrente $I$ e riduce il tempo di commutazione $t$.

In un condensatore posto molto vicino all'alimentazione del driver si accumula la carica di commutazione da fornire alla base.

\section{Interruttori high-side}
\subsection{Interruttore high-side a transistore bipolare \textit{npn}}
Il transistore bipolare \textit{npn} non riesce ad approssimare bene il cortocircuito e dissipa troppa potenza: la tensione $V_{CE}$ di conduzione, soprattutto considerando che la corrente sul carico $I_C$ è molto elevata, è significativa poiché sulla giunzione base-emettitore è applicata una tensione di polarizzazione diretta non trascurabile tra il carico $Z_L$ e l'alimentazione.

\subsection{Interruttore high-side a transistore \textit{n}MOS}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/12/NMOS-transistor_high-side_switch.png}
\end{figure}

\noindent
Il transistore \textit{n}MOS soffre in conduzione dello stesso tipo di problema: la tensione $V_{DS}$ non riesce a essere trascurabile, ma siccome è imposta una tensione $V_{GS} > V_{th}$ è dell'ordine di grandezza della tensione di soglia $V_{th}$. Il problema è risolvibile applicando al terminale di gate una tensione più elevata della tensione di alimentazione.
\FloatBarrier

\subsection{Interruttore high-side a transistore \textit{p}MOS}
Quando l'interruttore di controllo\footnote{L'\textbf{interruttore di controllo}, in questo caso low-side, serve per caricare e scaricare la capacità equivalente del transistore, ma è facile da realizzare perché è a bassa potenza e deve gestire correnti basse. Il transitorio immediatamente successivo alla commutazione dell'interruttore si considererà sempre esaurito.} è chiuso, vale $V_{GS}=-V_{AL}$ (affinché il transistore \textit{p}MOS sia in conduzione, deve valere $V_{GS} < V_{th}$), ma c'è poca dissipazione di potenza perché la tensione di source però non è più vincolata ad essere uguale a quella del carico $Z_L$. Tuttavia, le prestazioni di un \textit{p}MOS sono peggiori di quelle di un \textit{n}MOS sia in termini di velocità di commutazione, sia perché ha un'elevata resistenza $R_{\text{on}}$.

\section{Isolamento galvanico ottico}
La parte di controllo (il circuito digitale) e la parte di potenza (l'interruttore) devono essere elettricamente isolate l'una dall'altra a causa di eventuali sbalzi di tensione o interferenze.

\paragraph{Tipi di isolamento galvanico}
\begin{itemize}
\item trasformatore: isolamento tramite campo magnetico;
\item condensatore: isolamento tramite campo elettrico;
\item fotoaccoppiatore o isolatore fotovoltaico: isolamento ottico.
\end{itemize}

Per il passaggio di segnali in continua l'\textbf{isolamento ottico} non richiede modulazione, al contrario delle altre due soluzioni basate su elementi reattivi.

\subsection{Fotoaccoppiatore}
Il \textbf{fotoaccoppiatore} è un circuito integrato dove il segnale elettrico passa unidirezionalmente attraverso un meccanismo ottico:
\begin{itemize}
\item alla porta d'ingresso vi è un \textbf{diodo LED}: la radiazione luminosa in uscita ha un'intensità proporzionale alla corrente fornita all'ingresso;
\item alla porta d'uscita vi è un \textbf{fototransistore}, cioè un transistore bipolare la cui corrente di base $I_B$ è fornita dall'assorbimento di radiazione luminosa proveniente dal diodo LED $\Rightarrow$ la corrente di base, pur essendo a sua volta proporzionale alla corrente fornita all'ingresso, è isolata da quest'ultima.
\end{itemize}

Il \textbf{CTR} (Current Transfer Ratio) è il rapporto tra la corrente di collettore del transistore bipolare e la corrente d'ingresso nel diodo LED.

Il tempo di commutazione del fotoaccoppiatore è lento, in particolare il tempo di spegnimento a causa del transistore bipolare (bisogna attendere la ricombinazione dei portatori minoritari nella base). Aggiungere una resistenza per velocizzare il tempo di commutazione riduce il parametro CTR.

\subsection{Isolatore fotovoltaico}
L'\textbf{isolatore fotovoltaico} sfrutta lo stesso meccanismo ottico del fotoaccoppiatore con alcune differenze:
\begin{itemize}
\item alla porta d'ingresso sono posti in serie due diodi LED $\Rightarrow$ la tensione alla porta d'ingresso è maggiore;
\item alla porta d'uscita sono posti in serie delle giunzioni \textit{pn} dette \textbf{fotodiodi}, che convertono una radiazione luminosa in un segnale di corrente.
\end{itemize}

Anche se non richiedono alimentazione, i fotodiodi generano una corrente bassa $\Rightarrow$ se messi a pilotare direttamente un transistore MOS, i tempi di commutazione sono molto lunghi.

\section{Comportamento in commutazione}
\subsection{Carico resistivo (\texorpdfstring{$Z_L=R_L$}{ZL = RL})}
In un interruttore low-side a transistore \textit{n}MOS, se il carico è di tipo resistivo la retta di carico è definita come:
\[
V_{DS} = V_{AL} - R_L I_L , \quad I_L \equiv I_D
\]

\begin{multicols}{2}
\paragraph{Stato OFF \textmd{($t=0^-$)}}
\[
\begin{cases} \displaystyle V_L = R_L I_L = 0 \\ \displaystyle V_{DS} = V_{AL} \end{cases}
\]
\vfill
\columnbreak
\paragraph{Stato ON \textmd{($t=0^+$)}}
\[
\begin{cases} \displaystyle V_L = R_L I_L = 0 \\ \displaystyle V_{DS} = R_{\text{on}} I_L \approx 0 \end{cases}
\]
\vspace*{\fill}
\end{multicols}

La retta di carico può trovarsi per un breve periodo di tempo (\textless\ 100 \textmu s) durante la commutazione al di fuori dalla Safe Operating Area (SOA) statica, ma sempre entro quella dinamica caratterizzata da una potenza massima maggiore.

\subsection{Carico induttivo (\texorpdfstring{$Z_L=R_L+j \omega L$}{ZL=RL+jwL}): transitorio di accensione}
La corrente $I_L$ cresce solo esponenzialmente perché non è forzata:\footnote{All'istante $t=0^+$ si trascura il transitorio di accensione sulla tensione $V_{DS}$ del transistore \textit{n}MOS $\Rightarrow$ esso si comporta in modo ideale.}
\begin{multicols}{3}
\paragraph{Stato OFF \textmd{($t=0^-$)}}
\[
\begin{cases} \displaystyle V_L = 0 \\ \displaystyle R_L I_L = 0 \\ \displaystyle V_{DS} = V_{AL} \end{cases}
\]
\vfill
\columnbreak
\paragraph{Stato ON \textmd{($t=0^+$)}}
\[
\begin{cases} \displaystyle I_L \left( 0^+ \right) = I_L \left( 0^- \right) \\ \displaystyle {\left. \frac{dI_L}{dt} \right|}_{t=0^+} >0 \end{cases} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle V_L = L \frac{dI_L}{dt} = V_{AL} \\ \displaystyle R_L I_L = 0 \\ \displaystyle V_{DS} = 0 \end{cases}
\]
\vfill
\columnbreak
\paragraph{Stato ON \textmd{(transitorio esaurito)}}
\[
\begin{cases} \displaystyle V_L = 0 \\ \displaystyle R_L I_L = V_{AL} \\ \displaystyle V_{DS} = 0 \end{cases}
\]
\vspace*{\fill}
\end{multicols}

La potenza dissipata dal transistore rimane sempre dentro la SOA, perché non si hanno mai tensione $V_{DS}$ e corrente $I_L$ entrambe elevate allo stesso istante.

\subsection{Carico induttivo (\texorpdfstring{$Z_L=R_L+j \omega L$}{ZL=RL+jwL}): transitorio di spegnimento}
La corrente $I_L$ diminuisce molto rapidamente perché è forzata dal transistore MOS che è supposto ideale:
\begin{multicols}{3}
\paragraph{Stato ON \textmd{($t=0^-$)}}
\[
\begin{cases} \displaystyle V_L = 0 \\ \displaystyle R_L I_L = V_{AL} \\ \displaystyle V_{DS} = 0 \end{cases}
\]
\vfill
\columnbreak
\paragraph{Stato OFF \textmd{($t=0^+$)}}
\[
\begin{cases} \displaystyle I_L \left( 0^+ \right) = I_L \left( 0^- \right) \\ \displaystyle {\left. \frac{dI_L}{dt} \right|}_{t=0^+} \ll 0 \end{cases} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle V_L = L \frac{dI_L}{dt} \ll V_{AL} \\ \displaystyle R_L I_L = V_{AL} \\ \displaystyle V_{DS} = V_{AL} - V_L - R_L I_L \gg V_{AL} \end{cases}
\]
\vfill
\columnbreak
\paragraph{Stato OFF \textmd{(transitorio esaurito)}}
\[
\begin{cases} \displaystyle V_L = 0 \\ \displaystyle R_L I_L = 0 \\ \displaystyle V_{DS} = V_{AL} \end{cases}
\]
\vspace*{\fill}
\end{multicols}

Nel transitorio, la tensione di drain $V_{DS}$ assume un valore molto superiore alla tensione di alimentazione $V_{AL}$, tanto che la potenza dissipata esce persino dalla SOA dinamica e può anche raggiungere la tensione di breakdown del transistore.

L'energia accumulata nell'induttore:
\[
E_L = \frac{1}{2} R_L {I_L}^2
\]
alla commutazione viene scaricata sul transistore \textit{n}MOS.

Un \textbf{diodo di ricircolo} (o volano) posto in parallelo al carico\footnote{Si noti il verso della tensione sul diodo:
\[
V = - V_L - R_L I_L
\]} protegge il transistore dagli sbalzi di tensione: quando l'interruttore si apre, il diodo da interdetto ($V=-V_{AL} < V_\gamma$) entra in conduzione ($V \approx -V_L \gg V_{AL}$) e si determina un flusso di corrente verso il diodo in modo che l'energia $E_L$ accumulata dall'induttore si scarichi sulla resistenza parassita del diodo e sulla resistenza di carico $R_L$.
