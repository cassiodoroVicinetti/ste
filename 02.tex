\chapter{Gli amplificatori}
\section{Vista dall'esterno}
L'\textbf{amplificatore} restituisce in uscita un certo segnale con più potenza del segnale in ingresso. Deve tendere ad essere un \textbf{modulo lineare}, cioè deve replicare il segnale deteriorandolo il meno possibile.

Gli amplificatori sono specifici in base alla frequenza del segnale.

A seconda se i segnali in ingresso e in uscita sono correnti o tensioni:
\begin{itemize}
\item $V \rightarrow V$: amplificatore di tensione
\item $I \rightarrow I$: amplificatore di corrente
\item $I \rightarrow V$: amplificatore di transresistenza $R_m = {V \over I}$
\item $V \rightarrow I$: amplificatore di transconduttanza $G_m = {I \over V}$
\end{itemize}

In un amplificatore di tensione si possono distinguere 4 terminali:
\begin{itemize}
\item in basso: terminale comune (a cui sono collegate tutte le tensioni);
\item a sinistra: porta di ingresso a cui è applicata la tensione di ingresso $V_{\text{in}} = V_1$;
\item a destra: porta di uscita a cui è applicata la tensione $V_{\text{out}} = V_2$;
\item in alto: alimentazione, costituita da una tensione costante $V_A$.
\end{itemize}

\paragraph{Flusso del segnale:} porta di ingresso $\rightarrow$ amplificatore $\rightarrow$ porta di uscita

La tensione di uscita $V_{\text{out}}$ è misurata sulla \textbf{resistenza di carico},\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Carico_(elettrostatica)}{Carico (elettrostatica)} su Wikipedia in italiano.} che si comporta da utilizzatore\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Utilizzatore_elettrico}{Utilizzatore elettrico} su Wikipedia in italiano.} quando riceve il segnale.

\subsection{Guadagno di potenza}
L'alimentazione fornisce l'energia necessaria per amplificare la tensione di ingresso. Avviene però una dispersione dell'energia $\Rightarrow$ si definisce \textbf{efficienza} il rapporto tra la potenza fornita al carico e la potenza fornita dall'alimentatore.

L'\textbf{amplificazione di tensione} $A_V$ determina di quanto è aumentato il \textbf{valore di picco} (= ampiezza massima) del segnale:
\[
V_{{\text{out}}_p} = A_V \cdot V_{{\text{in}}_p}
\]

Si potrebbe realizzare un amplificatore tramite un trasformatore (ideale), il cui numero di spire sia legato ad $A_V$, ma la potenza di uscita sarebbe uguale a quella in ingresso: $V_1 I_1=V_2 I_2$

Il \textbf{guadagno di potenza}, espresso in decibel (dB), è un modo per calcolare il rapporto tra le potenze:
\[
G_P = K_P\left( \text{dB} \right) = 10 \log_{10} K_p
\]
dove $K_P={P_{\text{out}} \over P_{\text{in}}}$  è una quantità adimensionata detta \textbf{rapporto di potenza}. Se $P_{\text{in}}=P_{\text{out}} \Rightarrow G_P \left( \text{dB} \right)=0$.
Se $P_{\text{out}} = 1 \; \text{mW}$, l'unità di misura del guadagno diventa il dBm.

Esprimendo il guadagno di potenza tramite i rapporti tra tensioni e resistenze:
\[
P = {V^2 \over R} \Rightarrow K_P = {\left( {V_u \over V_i} \right)}^2 \left( {R_i \over R_u} \right)
\]
e supponendo uguali le resistenze $R_i$ e $R_u$:\footnote{Si veda la sezione \href{https://it.wikipedia.org/wiki/Decibel\#Una_nota_di_cautela_sul_fattore_20}{Una nota di cautela sul fattore 20} alla voce ``Decibel'' su Wikipedia in italiano.}
\[
K_P = {\left( {V_u \over V_i} \right)}^2 \Rightarrow G_P = 2 \cdot 10 \log_{10} \left| {V_u \over V_i} \right| = 20 \log_{10} \left| A_V \right| = A_V \left( \text{dB} \right) \Rightarrow A_V = \sqrt{K_P}
\]

Se più amplificatori sono connessi in cascata, le amplificazioni $A_V$ si moltiplicano e i guadagni $G_P=A_V \left( \text{dB} \right)$ si sommano. Si chiama \textbf{tensione picco-picco} la differenza tra i valori massimo e minimo di tensione: $V_{PP}=2V_P$.

Se invece $\begin{cases} V_{\text{in}} = V_{\text{out}} \Rightarrow A_V = 1 \\
R_{\text{in}} \neq R_{\text{out}} \end{cases}$, l'amplificazione del segnale è proporzionale al rapporto tra le resistenze.

\section{Struttura interna}
\subsection{Amplificatore di tensione}
All'interno vi è un generatore di tensione pilotato in uscita.

Idealmente, tutti i generatori sono ideali e non vi sono resistenze $\Rightarrow$ nessuna perdita. In realtà, le resistenze interne $R_i$ e $R_u$ dissipano potenza.

\begin{multicols}{2}
\paragraph{Effetto degli ingressi} Con uscita a vuoto ($R_c = +\infty \Rightarrow V_u = A_V V_1$), la tensione del generatore reale $V_i$ si ripartisce tra la resistenza del generatore $R_g$ e la resistenza di ingresso $R_i$:
\[
V_1=V_i  {R_i \over R_i+R_g }
\]
\vfill
\columnbreak
\paragraph{Effetto delle uscite} Con generatore ideale in ingresso ($R_g = 0 \Rightarrow V_i = V_1$), la tensione $V_1 A_V$ si ripartisce tra la resistenza di uscita $R_u$ e la resistenza di carico $R_c$:
\[
V_u=A_V V_1  {R_c \over R_c+R_u }
\]
\vspace*{\fill}
\end{multicols}

La funzione di trasferimento complessiva tiene conto degli effetti combinati della resistenza d'ingresso $R_i$ e di quella d'uscita $R_u$:
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
\textbf{con uscita a vuoto} & \textbf{con generatore ideale} & \textbf{effetto complessivo} \\
\hline
& & \\ [-1.5ex]
$\displaystyle {V_u \over V_i} = A_V  {R_i \over R_i+R_g}$ & $\displaystyle {V_u \over V_i} = A_V  {R_c \over R_u+R_c}$ & $\displaystyle {V_u \over V_i} = A_V  {R_i \over R_i+R_g} {R_c \over R_u + R_c}$ \\ [2ex]
\hline
\end{tabular}
\end{table}

\subsection{Casi ideali}
Idealmente, i generatori in ingresso devono essere ideali, e il carico deve ricevere tutta la tensione $A_V V_i$:
\begin{itemize}
\item ingressi in tensione: $R_i \rightarrow +\infty \Rightarrow R_G \rightarrow 0$
\item ingressi in corrente: $R_i \rightarrow 0 \Rightarrow R_g \rightarrow +\infty$
\item uscite in tensione: $R_u \rightarrow 0 \Rightarrow V_u = A_V V_i$
\item uscite in corrente: $R_u \rightarrow +\infty \Rightarrow I_2 = I$
\end{itemize}

\subsection{Casi reali}
\begin{itemize}
\item ingressi in tensione: amplificatori per microfono, circuiti logici;
\item ingressi in corrente: sensori ottici (fotodiodi, telecomandi con infrarossi) che convertono l'onda elettromagnetica in corrente;
\item uscite in tensione: alimentazione di circuiti elettronici, lampadine;
\item uscite in corrente: motori, attuatori elettromagnetici, caricabatterie.
\end{itemize}

\section{Da rete a doppio bipolo}
Il modello più semplice di amplificatore è definito dai 3 parametri $R_i$, $R_u$, $A_V$:
\[
\begin{cases} \displaystyle R_i = {V_i \over I_i} \\
\displaystyle R_u = {V_{u_{\mathrm{vuoto}}} \over I_{u_{\mathrm{cortocircuito}}}} \\
\displaystyle A_V = {V_{u_{\mathrm{vuoto}}} \over V_i} \end{cases}
\]

Qualunque doppio bipolo con generatore pilotato, applicando il teorema di Thevenin, si può ricondurre al modello più semplice.

\subsection{Doppi bipoli in cascata}
Due doppi bipoli sono in cascata se la tensione di uscita del primo è quella di ingresso del secondo. Una catena di $N$ doppi bipoli si può ricondurre a un singolo doppio bipolo equivalente di parametri:
\begin{itemize}
\item \ul{funzione di trasferimento complessiva:} è il prodotto delle funzioni di trasferimento dei singoli moduli;
\item \ul{resistenza equivalente di ingresso $R_i$:} coincide con la resistenza di ingresso $R_{i_1}$ del primo modulo:
\[
R_i = R_{i_1}
\]
\item \ul{resistenza equivalente di uscita $R_u$:} coincide con la resistenza di uscita $R_{u_N}$ dell'ultimo modulo:
\[
R_u=R_{u_N}
\]
\end{itemize}