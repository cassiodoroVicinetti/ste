\chapter{Gli amplificatori: comportamento in frequenza}
Uno stadio amplificatore deve avere una certa \textbf{risposta in frequenza}\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Risposta_in_frequenza}{Risposta in frequenza} su Wikipedia in italiano.} in base alla \textbf{frequenza} (= rapidità di variazione) del segnale in ingresso.

Se vi sono elementi dinamici (induttori, condensatori), l'amplificazione non è più una costante ma dipende dalla frequenza: $A_V \left(s\right)$.

\section{Celle del I ordine}
\subsection[Analisi della risposta al gradino nel dominio del tempo]{Analisi della risposta al gradino nel dominio del tempo\footnote{Nel dominio del tempo, il condensatore si comporta come un cortocircuito all'inizio del transitorio ($t = 0^+$) e come un circuito aperto al termine del transitorio, e viceversa per l'induttore.}}
Nel \textbf{dominio del tempo}, si studia il comportamento asintotico durante una \textbf{risposta al gradino} $u\left(t\right)$.

Gli elementi dinamici introducono un ritardo nella risposta del circuito: per esempio, durante la risposta al gradino di un circuito RC\footnote{Si veda la sezione~\ref{sez:Analisi_nel_tempo}.} la tensione ai capi del condensatore non assume istantaneamente il valore del gradino {\small (amplificato nel caso degli amplificatori)}, ma è caratterizzato da un transitorio\footnote{Si veda il file \href{https://it.wikipedia.org/wiki/File:RC_impulso.PNG}{RC impulso.PNG} su Wikipedia in italiano.} che tende asintoticamente a tale valore $\Rightarrow$ lo studio di frequenze elevate richiede rapidi transitori.

\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/03/RC_cell_step_response.png}
\end{figure}

La risposta al gradino di un circuito dinamico del I ordine è definita univocamente dai comportamenti asintotici a $t=0^+$ e a $t \rightarrow +\infty$ e dalla costante di tempo $\tau$:
\[
x\left(t\right) = \left[ x \left( 0^+ \right) - x \left( +\infty \right) \right] e^{- {t \over \tau}} + x \left(+\infty \right) = x_B e^{-{t \over \tau}} + x_A
\]
\FloatBarrier

I circuiti del II ordine hanno invece una risposta al gradino più complessa.

Si dimostra che $\Delta x \left( \tau \right) = \left| x \left( 0^+ \right) - x \left( \tau \right) \right| = 0,63 \cdot \Delta x = 0,63 \cdot \left| x \left( 0^+ \right) - x \left( +\infty \right) \right|$ $\Rightarrow$ graficamente la funzione $x \left( t \right)$ assume in $t = \tau$ un valore che è aumentato/diminuito, a partire da $x \left( 0^+ \right)$, del 63\% rispetto all'intervallo $\Delta x$ tra $x \left( 0^+ \right)$ e $x \left( +\infty \right)$.

Inoltre, la tangente al grafico in $t=0^+$ interseca l'asintoto per $x \left( +\infty \right)$ in $t = \tau$:
\[
\left. {dx \over dt} \right \vert_{t=0} = - {x_B \over \tau} \Rightarrow \begin{cases} \displaystyle x \left( t \right) = - {x_B \over \tau} t + x \left( 0^+ \right) \\
\displaystyle x \left( t \right) = 0 \end{cases} \Rightarrow \begin{cases} \displaystyle t = x \left( 0^+ \right) \cdot {\tau \over x_B} \\
\displaystyle x \left( +\infty \right) = 0 \end{cases} \Rightarrow t = \tau
\]

\subsection[Analisi del comportamento dinamico nel dominio della frequenza]{Analisi del comportamento dinamico nel dominio della frequenza\footnote{Nel dominio della frequenza, il condensatore si comporta come un circuito aperto a bassa frequenza (in continua) e come un cortocircuito ad alta frequenza, e viceversa per l'induttore.}}
Nel \textbf{dominio della frequenza}, si rappresenta tramite il \textbf{diagramma di Bode} il comportamento dinamico del circuito.

La \textbf{funzione di rete} $H \left( s \right)$ è un rapporto di funzioni polinomiali che sono le espressioni di due \textbf{segnali sinusoidali}:
\[
H \left( s \right) = \frac{s^{n_z} \left( z_1 + s \right) \left(z_2 + s \right) \cdots}{s^{n_p} \left( p_1 + s \right) \left( p_2 + s \right) \cdots} = K \cdot s^{n_z - n_p} \frac{\left( 1 + {s \over z_1} \right) \left( 1 + {s \over z_2} \right) \cdots}{\left( 1 + {s \over p_1} \right) \left( 1 + {s \over p_2} \right) \cdots}
\]

Gli \textbf{zeri} sono le radici del numeratore (di cui $n_z$ nulle), i \textbf{poli} sono le radici del denominatore (di cui $n_p$ nulle). I poli sono una caratterizzazione univoca del comportamento del circuito, indipendentemente dalla funzione di rete. Il \textbf{diagramma di Bode} è la rappresentazione grafica della funzione di rete $H \left( s \right)$ nella sua restrizione ai numeri complessi $H \left( j \omega \right)$ (cioè l'amplificazione di tensione $A_V \left( j \omega \right)$ nel caso particolare degli amplificatori), in funzione della frequenza angolare $\omega$ (u.m. rad/s) espressa in scala logaritmica.\footnote{Nella scala logaritmica, si dice \textbf{decade} l'intervallo tra due potenze del 10.}

L'espressione in decibel (dB) del modulo della funzione di rete $\left| H \left( j \omega \right) \right|$ (cioè il guadagno di potenza $G_P$ nel caso particolare degli amplificatori) è, sostituendo $s$ con $j \omega$ e applicando le proprietà dei logaritmi e dei numeri complessi:
\[
\left| H \left( j \omega \right) \right| \left( \text{dB} \right) = 20 \log_{10}{\left| H \left( j \omega \right) \right|} =
\]
\[
= 20 \log_{10}{\left| K \right|} + 20 \left( n_z - n_p \right) \log_{10}{\omega} + 20 \sum\nolimits_i{\log_{10}{\sqrt{1 + \frac{{\omega} ^ 2}{{z_i}^2}}}} - 20 \sum\nolimits_j{\log_{10}{\sqrt{1+ \frac{{\omega}^2}{{p_j}^2}}}}
\]

Il diagramma di Bode è dato dalla somma dei contributi dei singoli termini:
\begin{itemize}
\item la \textbf{costante} $K$ contribuisce con una retta orizzontale;
\item ogni \textbf{zero nullo} contribuisce con una retta di pendenza 20 dB/dec passante per $\left( z , \, 20 \log_{10}{z} \right) = \left( 1 \; \text{rad}/ \text{s} , \, 0 \; \text{dB} \right)$;
\item ogni \textbf{zero non nullo} contribuisce con una spezzata composta da:
\begin{itemize}
\item a sinistra dello zero, una retta orizzontale costante:
\[
\omega \ll z_j \Rightarrow 20 \log_{10}{\sqrt{1 + \frac{{\omega}^2}{{z_j}^2}}} \simeq 20 \log_{10}{1} = 0 \; \text{dB}
\]
\item in corrispondenza dello zero, uno scostamento verticale di entità trascurabile:
\[
\omega \simeq z_j \Rightarrow 20 \log_{10}{\sqrt{1 + \frac{{\omega}^2}{{z_j}^2}}} \simeq 20 \log_{10}{\sqrt{2}} \simeq 3 \; \text{dB}
\]
\item a destra dello zero, una retta di pendenza 20 dB/dec intersecante l'asse orizzontale nello zero:
\[
\omega \gg z_j \Rightarrow 20 \log_{10}{\sqrt{1 + \frac{{\omega}^2}{{z_j}^2}}} \simeq 20 \log_{10}{\frac{\omega}{z_j}} \; \text{dB}
\]
\end{itemize}
\item ogni \textbf{polo} contribuisce con una spezzata analoga a quella degli zeri, ma con pendenza negativa.
\end{itemize}

Per i tratti a pendenza 20 dB/dec, i rapporti tra frequenze e i rapporti tra ampiezze sono uguali.

\subsection{Filtri passa-alto}
Se si pone un \textbf{condensatore in serie} al flusso di segnale si ottiene un \textbf{filtro passa-alto}.

\subsubsection{Cella RC passa-alto}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.2\linewidth]{pic/03/High-pass_RC_cell.png}
\end{figure}

\paragraph{Analisi nel tempo}
\label{sez:Analisi_nel_tempo}
Applicando un \textbf{segnale a gradino} a una \textbf{cella RC passa-alto}, la tensione di uscita $V_2$:
\[
\begin{cases} \displaystyle V_2 \left( t \right) = \left[ V_2 \left( 0^+ \right) - V_2 \left( + \infty \right) \right] e^{-{t \over \tau}} + V_2 \left( + \infty \right) = V_B e^{-{t \over \tau}} + V_A \\
\displaystyle V_2 \left( 0^+ \right) = V_1 \\
\displaystyle V_2 \left( + \infty \right) = 0 \end{cases} \Rightarrow V_2 \left( t \right) = V_1 e^{-{t \over \tau}}
\]
presenta una risposta transitoria che ha nel tempo un andamento esponenziale decrescente.

Il grafico di $V_2$ è toccato in $t=0^+$ dalla retta tangente:
\[
V_2 \left( t \right) = mt+ V_2 \left( 0^+ \right)
\]
di coefficiente angolare $m$:
\[
m = \left. D_t \left( V_2 \left( t \right) \right) \right \vert_{t = 0^+} = D_t \left. \left( V_2 \left( 0^+ \right) e^{- \frac{t}{\tau}} \right) \right \vert_{t = 0^+} = V_2 \left( 0^+ \right) \left. \left( - \frac{1}{\tau} e^{-{t \over \tau}} \right) \right \vert_{t = 0^+} = - \frac{1}{\tau} V_2 \left( 0^+ \right)
\]
che interseca l'asse delle ascisse $t$ nel punto $t = \tau$:
\[
V_2 \left( t \right) = 0 \Rightarrow 0 = V_2 \left( 0^+ \right) \left( - \frac{1}{\tau} t + 1 \right) \Rightarrow t = \tau
\]

Per questo motivo, la costante di tempo $\tau$ è detta \textbf{costante di decadimento}.

\paragraph{Analisi in frequenza}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/03/High-pass_RC_cell_frequency_analysis.png}
\end{figure}

Il condensatore posto in serie al flusso di segnale si comporta come un \textbf{filtro passa-alto}, cioè attenua in dB\footnote{Un'attenuazione del segnale nel diagramma di Bode significa che il logaritmo è negativo $\Rightarrow$ il suo argomento è compreso tra 0 e 1 $\Rightarrow$ la tensione di uscita è minore di quella in ingresso.} a basse frequenze:
\begin{itemize}
\item i segnali a bassa frequenza ($\omega \ll \tfrac{1}{\tau}$) non passano: il condensatore si comporta come un circuito aperto $\Rightarrow$ $V_2=0$;
\item i segnali ad alta frequenza ($\omega \gg \tfrac{1}{\tau}$) passano: il condensatore si comporta come un cortocircuito $\Rightarrow$ $V_2=V_1$.
\end{itemize}

Applicando il partitore di tensione sulla resistenza, si trova la seguente funzione di rete:
\[
H \left( s \right) = \frac{V_2}{V_1} = \frac{s \tau}{s \tau +1}
\]
avente uno zero nell'origine ($\omega = z = 1 \, \text{rad/ms}$) e un polo in $\omega = p = \tfrac{1}{\tau} \, \text{rad/ms}$, con $\tau = RC$.

I segnali ad alta frequenza in realtà vengono epurati della loro componente continua (DC), cioè viene mantenuta solamente la sinusoide azzerando\footnote{L'esempio sulla diapositiva considera una tensione di uscita non collegata a massa ma ad un'ulteriore tensione continua $V_R$.} il valor medio attorno a cui essa oscilla.
\FloatBarrier

\subsubsection{Amplificatore con cella RC passa-alto}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/03/Amplifier_with_high-pass_RC_cell.png}
\end{figure}

\noindent
Introducendo, come impedenza del generatore reale, un condensatore in serie nella linea di ingresso di un amplificatore, esso si comporta come un filtro passa-alto, filtrando le componenti continue dei segnali sinusoidali in ingresso e rispondendo con un transitorio agli ingressi a gradino.
\FloatBarrier

\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/03/Amplifier_with_high-pass_RC_cell_frequency_analysis.png}
\end{figure}

Posto $A_V$ costante, la tensione $V_1$ filtrata dalla cella RC viene poi amplificata nella tensione $V_C = A_V V_1$ di uscita dell'amplificatore. La funzione di rete $\tfrac{V_C}{V_G} \left( s \right)$ complessiva:
\[
\begin{cases} \displaystyle V_1 = H \left( s \right) \cdot V_G \\
\displaystyle V_C = A_V \cdot V_1 \end{cases} \Rightarrow \frac{V_C}{V_G} = A_V \cdot H \left( s \right)
\]
presenta un diagramma di Bode analogo a quello della funzione di rete $H \left( s \right)$ della singola cella RC, ma per le proprietà dei logaritmi con sfalsamento costante pari a $20 \log_{10}{A_V}$:
\[
\left| \frac{V_C}{V_G} \left( j \omega \right) \right| \left( \text{dB} \right) = \left| A_V \cdot H \left( j \omega \right) \right| \left( \text{dB} \right) = A_V \left( \text{dB} \right) + \left| H \left( j \omega \right) \right| \left( \text{dB} \right) = 20 \log_{10}{A_V} + 20 \log_{10}{\left| H \left( j \omega \right) \right|}
\]
\FloatBarrier

\subsubsection{Condensatori di disaccoppiamento tra stadi amplificatore in cascata}
Se due stadi amplificatore sono posti in cascata separati da un \textbf{condensatore di disaccoppiamento}, il condensatore si comporta come un filtro passa-alto. La costante di tempo $\tau$ è uguale al prodotto tra la capacità $C$ e la resistenza equivalente $R_{\text{eq}} = R_{{\text{u}}_1} + R_{{\text{u}}_2}$ vista ai capi del condensatore allo spegnimento di tutti i generatori indipendenti.

\subsection{Filtri passa-basso}
A differenza di quello passa-alto, in un \textbf{filtro passa-basso} l'uscita viene presa ai capi del condensatore.

\subsubsection{Cella RC passa-basso}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.25\linewidth]{pic/03/Low-pass_RC_cell.png}
\end{figure}

\paragraph{Analisi nel tempo}
Nella risposta al gradino, la tensione di uscita $V_2$ ha un andamento esponenziale crescente:
\[
\begin{cases} \displaystyle V_2 \left( t \right) = \left[ V_2 \left( 0^+ \right) - V_2 \left( + \infty \right) \right] e^{- \frac{t}{\tau}} + V_2 \left( + \infty \right) = V_B e^{- \frac{t}{\tau}} + V_A \\
\displaystyle V_2 \left( 0^+ \right) = 0 \\
\displaystyle V_2 \left( + \infty \right) = V_1 \end{cases} \Rightarrow V_2 \left( t \right) = V_1 \left( 1 - e^{-\frac{t}{\tau}} \right)
\]

\paragraph{Analisi in frequenza}
Un \textbf{filtro passa-basso} consente il passaggio di segnali solo al di sotto di una certa frequenza. La funzione di rete $H \left( s \right)$ ha un polo a frequenza $\omega = \tfrac{1}{\tau}$:
\[
H \left( s \right) = \frac{V_2}{V_1} = \frac{1}{s \tau+1}
\]

Il diagramma di Bode presenta un'attenuazione in dB dei segnali ad alta frequenza ($\omega \gg \tfrac{1}{\tau}$).

\subsubsection{Amplificatore con cella RC passa-basso}
\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{pic/03/Amplifier_with_low-pass_RC_cell.png}
\end{figure}

\noindent
La funzione di rete complessiva di un amplificatore passa-basso è:
\[
\begin{cases} \displaystyle V_C = A_V V_1 \frac{Z_C}{Z_C + R_U} \\
\displaystyle V_1 = V_G \frac{R_i}{R_i + R_G} \end{cases} \Rightarrow \frac{V_C}{V_G} = A_{V_1} \left( s \right) = A_V \frac{R_i}{R_i + R_G} \frac{Z_C}{Z_C + R_u} =
\]
\[
= A_V \frac{R_i}{R_i + R_G} \frac{R_C}{R_C + R_u} \frac{1}{1+ s C_1 \frac{R_C R_u}{R_C + R_u}}
\]
con $Z_C=C_1 || R_C$.

A $t \rightarrow + \infty$ nel dominio del tempo e per $\omega \rightarrow 0$ nel dominio della frequenza il condensatore si comporta come un circuito aperto, e il circuito diventa un normale stadio amplificatore con un generatore reale in ingresso e una resistenza di carico $R_C$ in uscita:
\[
\frac{V_C}{V_G} = A_{V_1} \left( \omega \rightarrow 0 \right) = A_V \frac{R_i}{R_i + R_G}\frac{R_C}{R_C + R_u}
\]

Pertanto la funzione di rete complessiva si può scrivere come il prodotto tra il \textbf{valore in banda passante} dell'amplificazione $A_{V_1}$ ($\omega \rightarrow 0$) e un termine legato al comportamento passa-basso:
\[
\frac{V_C}{V_G} = A_{V_1} \left( s \right) = A_{V_1} \left( \omega \rightarrow 0 \right) \cdot \frac{1}{1 + s C_1 \frac{R_C R_u}{R_C + R_u}} = A_{V_1} \left( \omega \rightarrow 0 \right) \cdot \frac{1}{1 + s \tau}
\]
dove vale ancora: $\tau = C_1 \cdot \left( R_C || R_u \right) = R_{\text{eq}} C$.
\FloatBarrier

\subsection{Filtri passa-banda}
\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{pic/03/Band-pass_amplifier_with_RC_cells.png}
\end{figure}

\noindent
Il circuito in figura presenta un condensatore di disaccoppiamento all'ingresso (cella passa-alto) e un carico reattivo all'uscita (cella passa-basso). I due condensatori si dicono disaccoppiati dal generatore di tensione interno all'amplificatore.
\FloatBarrier

\subsubsection{Analisi in frequenza}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/03/Pass-band_amplifier_with_RC_cells_frequency_analysis.png}
\end{figure}

\noindent
Il diagramma di Bode risulta dalla composizione dei due filtri passa-alto e passa-basso, aventi rispettivamente costanti di tempo ${\tau}_1$ e ${\tau}_2$ e poli ${\omega}_1$ e ${\omega}_2$, posti in cascata al flusso di segnale:
\[
V_C = A_V V_G \frac{s C_2 R_2}{1 + s C_2 R_2} \cdot \frac{1}{1 + s C_1 \left( R_1 + R_G \right)} = A_V V_G \frac{s {\tau}_1}{1 + s {\tau}_1} \cdot \frac{1}{1 + s {\tau}_2}
\]

Si definisce \textbf{banda passante} $B_p$ lo spettro di frequenza compreso tra la frequenza di taglio inferiore $f_1$ e la frequenza di taglio superiore $f_2$:
\begin{itemize}
\item la frequenza di taglio inferiore $f_1= \tfrac{{\omega}_1}{2 \pi}$ corrisponde a un'attenuazione di 3 dB rispetto al valore massimo dovuta al filtro passa-alto;
\item la frequenza di taglio superiore $f_2= \tfrac{{\omega}_2}{2 \pi}$ corrisponde a un'attenuazione di 3 dB rispetto al valore massimo dovuta al filtro passa-basso.
\end{itemize}

Mentre per annullare l'effetto passa-alto basta rimuovere il condensatore in ingresso, l'effetto passa-basso non si può eliminare a causa degli effetti di tipo capacitivo propri dei transistori, che sono dei dispositivi a semi-conduttore, di cui è composto l'amplificatore stesso.
\FloatBarrier

\subsubsection{Analisi nel tempo}
La \textbf{risposta} nel tempo \textbf{all'onda quadra} (= successione di gradini alternati con periodo $T$) è una sovrapposizione dei transitori passa-alto e passa-basso. I ritardi e le rapidità con cui si manifestano i comportamenti passa-alto e passa-basso dipendono dall'ordine di grandezza del periodo $T$ dell'onda quadra rispetto agli ordini di grandezza delle costanti di tempo ${\tau}_1$ e ${\tau}_2$ associate rispettivamente ai comportamenti passa-alto e passa-basso:\footnote{Anche se l'onda quadra non è propriamente un segnale sinusoidale, i segnali a gradino e a onda quadra si possono in realtà immaginare come segnali a frequenza variabile che è infinita in corrispondenza degli sbalzi di tensione, e si riduce progressivamente fino ad annullarsi nei tratti di tensione costante. Questa interpretazione spiega intuitivamente i grafici delle risposte al gradino delle celle RC.}
\begin{itemize}
\item se $T \ll {\tau}_1 \Rightarrow \overline{\omega} \ll {\omega}_2$: il comportamento passa-alto è trascurabile perché il suo transitorio si manifesta con troppo ritardo $\Rightarrow$ prevale il comportamento passa-basso: allo sbalzo di tensione il segnale è totalmente attenuato, ma questa attenuazione si riduce durante il tratto costante;
\item se $T \gg {\tau}_2 \Rightarrow \overline{\omega} \ll {\omega}_2$: il comportamento passa-basso è trascurabile perché il suo transitorio si esaurisce subito $\Rightarrow$ prevale il comportamento passa-alto: al termine del breve transitorio passa-basso il segnale è totalmente amplificato, ma questa amplificazione si riduce durante il tratto costante.
\end{itemize}

\subsubsection{Amplificatore in continua}
Gli amplificatori in continua (es. alimentatori) forniscono segnali DC a tensione costante ($\Rightarrow$ $\omega =0$) indipendentemente dal carico, perché hanno una frequenza di taglio inferiore ${\omega}_1 =0$.

\section{Celle del II ordine}
A seconda del tipo di segnale su cui deve operare, un amplificatore deve garantire una banda passante entro la quale rientrino tutte le possibili frequenze del tipo di segnale:
\begin{itemize}
\item \textbf{amplificatori a larga banda} (audio): banda di ampiezza $\sim$Mhz centrata intorno a basse frequenze $\sim$Mhz (es. altoparlante);
\item \textbf{amplificatori accordati} (di potenza RF): banda di ampiezza $\sim$Mhz centrata intorno ad alte frequenze $\sim$Ghz (es. microfono).
\end{itemize}

\subsection{Amplificatori accordati}
Gli amplificatori accordati sono di solito realizzati con celle reattive del II ordine, cioè \textbf{circuiti risonatori} aventi un induttore e un condensatore nella stessa maglia. Un circuito risonatore è caratterizzato da una funzione di trasferimento a poli complessi coniugati del tipo:
\[
H \left( s \right) = K \left( s \right) \cdot \frac{1}{s^2 + 2 \xi {\omega}_n s + {{\omega}_n}^2}
\]
dove:
\begin{itemize}
\item ${\omega}_n$ è la \textbf{frequenza di risonanza};
\item $\xi$ (``xi'') è lo \textbf{smorzamento}.
\end{itemize}

\subsubsection{Amplicatore accordato con cella RC}
L'\textbf{amplificatore con cella LC} è un amplificatore accordato con impedenza di carico costituita dal parallelo di un induttore $L$, un condensatore $C$ e un resistore $R_C$.

\paragraph{Analisi in frequenza}
Tramutando internamente all'amplificatore la serie generatore di tensione-resistore nel parallelo generatore di corrente-resistore, si trova la seguente funzione di rete $H \left( s \right)$:
\[
I_{R_C} = \frac{G_m V_i}{R_u} \cdot \frac{\frac{1}{R_C}}{R_C || R_u || L || C} \Rightarrow H \left( s \right) = \frac{V_u}{V_i} = \frac{G_m}{R_u} \cdot \frac{1}{R || L || C} = \frac{G_m}{R_u} \frac{s}{C} \cdot \frac{1}{s^2 + \frac{1}{RC} s + \frac{1}{LC}}
\]
dove:
\[
\begin{cases} \displaystyle 2 \xi {\omega}_n = \frac{1}{RC} \\ \displaystyle {{\omega}_n}^2 = \frac{1}{LC} \end{cases} \Rightarrow \begin{cases} \displaystyle \xi = \frac{1}{2} \frac{\sqrt{L}}{R \sqrt{C}} \\ \displaystyle {\omega}_n = \frac{1}{\sqrt{LC}} \end{cases}
\]
il cui modulo ha un andamento in frequenza ``a campana'' di ampiezza $\sim \xi$ centrata attorno a ${\omega}_n$.

\paragraph{Analisi nel tempo}
Se la resistenza di perdita è sufficientemente piccola $\Rightarrow$ $\xi$ non è troppo elevato, le risposte al gradino e all'impulso presentano un andamento oscillante di periodo $T= \tfrac{1}{{\omega}_n}$ con uno smorzamento proporzionale a $\xi$.

\section{Da rete a doppio bipolo}
Si opera in maniera analoga agli amplificatori resistivi in continua, tenendo conto che i 3 parametri $Z_i \left( s \right)$, $Z_u \left( s \right)$ e $A_V \left( s \right)$ possono essere complessi.

\section{Linearità e non linearità}
\subsection{Rappresentazione della relazione tra ingresso e uscita}
La relazione tra tensione di ingresso $V_i \left( t \right)$ e tensione di uscita $V_u \left( t \right)$ di un dispositivo si può rappresentare attraverso:
\begin{itemize}
\item due grafici separati di $V_i \left( t \right)$ e $V_u \left( t \right)$;
\item il grafico della funzione di trasferimento $H \left( s \right) = \frac{V_u \left( s \right)}{V_i \left( s \right)}$;
\item il grafico della \textbf{transcaratteristica} $V_u \left( V_i \right)$: è variabile a seconda di $\omega$ $\Rightarrow$ è difficile rappresentare comportamenti dovuti a bipoli dinamici.
\end{itemize}

\subsection{Moduli lineari}
Il grafico della transcaratteristica ideale è una retta passante per l'origine.

Il grafico della transcaratteristica di un \textbf{modulo lineare}\footnote{Per un modulo lineare vale la sovrapposizione degli effetti: l'uscita si può esprimere come la somma delle risposte parziali agli ingressi.} è una retta che si discosta dall'idealità in base a 3 parametri, a 2 a 2 indipendenti:
\begin{itemize}
\item \textbf{guadagno $\Delta K$:} differenza di pendenza tra i due andamenti rettilinei;
\item \textbf{offset $\Delta U$} (verticale): valore di uscita a ingresso nullo;
\item \textbf{offset $\Delta I$} (orizzontale): valore d'ingresso che annulla l'uscita.
\end{itemize}

\subsection{Moduli non lineari}
Il grafico della transcaratteristica di un modulo non lineare non ha andamento rettilineo, e non vale più il principio di sovrapposizione degli effetti.

Il \textbf{circuito raddrizzatore} è un modulo non lineare, ma lineare a tratti, che restituisce in uscita solo le tensioni d'ingresso positive:
\[
V_u = \begin{cases} \displaystyle 0 \quad \text{se} \, V_i < 0 \\ \displaystyle V_i \quad \text{se} \, V_i \geq 0 \end{cases}
\]

Altri moduli permettono di \textbf{saturare} un ingresso di tipo sinusoidale entro un certo intervallo di valori.

\subsection{Moduli reali}
\begin{itemize}
\item \ul{limiti di dinamica:} tutti i moduli reali non sono lineari, ma alcuni sono approssimabili linearmente, cioè la transcaratteristica è approssimabile a una retta (per esempio la tangente) entro una restrizione di valori in ingresso, cioè limitando le \textbf{dinamiche}\footnote{In questo caso, la dinamica è l'intervallo di valori che il segnale di ingresso/uscita può assumere garantendo la linearità della curva generata.} d'ingresso e uscita, che sono strettamente correlate tra loro attraverso la pendenza della retta; in uno stadio amplificatore, la dinamica di uscita è limitata dalla dinamica delle tensioni di alimentazione, in particolare superiormente a $+ {V_{\text{AL}}}_1$ e inferiormente a $- {V_{\text{AL}}}_2$;
\item \ul{limiti fisici:} se la tensione in ingresso è troppo elevata può danneggiare il modulo, soprattutto se la tensione di alimentazione è bassa;
\item \ul{limiti di banda:} mentre la frequenza massima non può superare i limiti fisici del modulo, la frequenza minima può anche essere scelta nulla, ma è consigliabile limitare anche la frequenza minima per escludere le frequenze basse di rumore.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/03/Non-linear_amplifier_signal_harmonic_distortion.png}
\end{figure}

Un amplificatore non lineare introduce una distorsione nel segnale: se per esempio viene fornito in ingresso un segnale con una singola \textbf{frequenza fondamentale} $f_0$, il segnale amplificato sarà caratterizzato anche dalle sue frequenze multiple $2f_0$, $3f_0$, ecc. dette \textbf{armoniche}. La \textbf{distorsione armonica totale} $\text{THD}$ misura il livello di distorsione del segnale amplificato in uscita rispetto alla fondamentale:
\[
\text{THD} = \frac{\sum_{i=2} \left| u_i \right|}{\left| u_1 \right|}
\]
%\FloatBarrier