\chapter{Il transistore MOSFET}
\section{Il sistema MOS}
\subsection{Struttura}
Il \textbf{sistema MOS} è la struttura base del transistore MOSFET, ed è anche detto condensatore MOS perché ha una struttura simile a quella di un condensatore:
\begin{itemize}
\item \textbf{bulk:} è un substrato composto da un materiale \textbf{semiconduttore} drogato, di solito silicio drogato di tipo $p$ ($p$-Si);
\item sopra vi è uno strato molto sottile di materiale isolante/dielettrico di bassa conducibilità elettrica, come il bi\textbf{ossido} di silicio SiO\textsubscript{2};
\item \textbf{gate:} è uno strato \textbf{metallico} conduttore di elevata conducibilità elettrica, come l'alluminio o più recentemente il silicio policristallino\footnote{Poiché il biossido di silicio nella base sottostante ha un passo reticolare differente, si formano tanti piccoli cristalli di silicio, di struttura regolare se presi uno a uno, che però non riescono a congiungersi tra loro in un'unica struttura regolare.} con forte drogaggio $n$ (\textbf{poly}).
\end{itemize}

\subsection{Regioni di funzionamento}
A seconda della tensione $V_G$ applicata al terminale del gate, si hanno tre regioni di funzionamento: ($V_{FB} < 0$, $V_{th} > V_{FB}$)
\begin{itemize}
\item \textbf{accumulo di lacune} ($V_G < V_{FB}$): in prossimità dell'interfaccia tra gate e ossido si forma uno strato superficiale di carica negativa, dovuto agli elettroni attirati verso l'ossido; le lacune del semiconduttore sono di conseguenza attirate all'interfaccia ossido-bulk;
\item \textbf{svuotamento di lacune} ($V_{FB} < V_G < V_{th}$): gli elettroni del metallo vengono allontanati dall'interfaccia gate-ossido, formando così all'interfaccia stessa uno strato di spessore infinitesimo di carica positiva, che viene bilanciata dalla carica negativa che si forma nella regione di svuotamento del bulk, sotto l'interfaccia con l'ossido, con la sottrazione di lacune dalla BV di ${N_A}^-$ atomi che diventano accettatori ionizzati;
\item \textbf{inversione} ($V_G > V_{th}$): la regione svuotata si amplia sempre di più, finché la tensione applicata $V_G$ non supera la tensione di soglia $V_{th}$ e inizia ad aggiungersi in prossimità dell'interfaccia ossido-bulk un sottile strato di elettroni liberi in BC, che aumenta la carica negativa della regione svuotata stessa.
\end{itemize}

\subsection{Condizioni di equilibrio termodinamico}
\subsubsection{\texorpdfstring{$V_G=0$}{VG = 0}: Svuotamento di lacune}
I livelli di Fermi $E_F$ del metallo e del semiconduttore non sono allineati: il livello di Fermi del metallo è praticamente coincidente con il livello $E_c$ poiché drogato $n\textsuperscript{+}$, mentre quello del semiconduttore è vicino al livello $E_v$ poiché drogato $p$. Il lavoro di estrazione $q \phi_M$ degli elettroni nel metallo è quindi minore del lavoro di estrazione $q {\phi_S}_p$ nel semiconduttore, e la loro differenza definisce la \textbf{tensione di banda piatta} $V_{FB}$:
\[
V_{FB} = \phi_M - {\phi_S}_p \simeq - 0,9 \; V
\]

Alla formazione della giunzione con $V_G=0$, per raggiungere l'equilibrio termodinamico il livello di Fermi deve essere costante poiché non c'è corrente che scorre $\Rightarrow$ si ha uno spostamento di elettroni dal metallo al semiconduttore, o equivalentemente di lacune nel verso opposto:
\begin{itemize}
\item \ul{semiconduttore:} si forma una regione svuotata caricata negativamente dagli atomi accettatori ionizzati;
\item \ul{ossido:} la barriera di energia è molto ampia e la carica elettrica al suo interno è nulla poiché è un isolante;
\item \ul{metallo:} si forma uno strato svuotato di elettroni, con una carica quindi positiva che, poiché il sistema dev'essere globalmente neutro, compensa la carica negativa $-qN_A x_p$ nel semiconduttore; dato che un metallo ideale è equipotenziale, gli elettroni si concentrano su una superficie infinitesima, rappresentata matematicamente con una delta di Dirac, in prossimità dell'interfaccia metallo-ossido.
\end{itemize}

Ricavando il campo elettrico $\vec{\mathcal{E}}$ dall'equazione di Poisson:
\[
\frac{d \mathcal{E}}{dx} = \frac{\rho}{\epsilon}
\]
all'interfaccia ossido-semiconduttore è presente una discontinuità, poiché la componente normale del campo elettrico ai lati dell'interfaccia si conserva anche se le costanti dielettriche $\epsilon_{ox}$ e $\epsilon_s$ dei due materiali sono differenti:
\[
\epsilon_{ox} {\mathcal{E}}_{ox} = \epsilon_s { {\mathcal{E}}_s }_0
\]
dove:
\[
\begin{cases} \displaystyle \epsilon_{ox} = {\epsilon_r}_{ox} \cdot \epsilon_0, & {\epsilon_r}_{ox} = 3,9 \text{ (biossido di silicio)} \\
\displaystyle \epsilon_s = {\epsilon_r}_s \cdot \epsilon_0 , & {\epsilon_r}_s = 11,7 \text{ (silicio)} \end{cases}
\]

Poiché il campo elettrico è inversamente proporzionale alla costante dielettrica, l'uso di dielettrici ad alta costante dielettrica permette di ridurre il campo elettrico in modo da mantenere costante la tensione di soglia ${\mathcal{E}}_{ox} t_{ox}$ del transistore ad un maggiore spessore $t_{ox}$ in modo da minimizzare l'effetto tunnel.

Integrando ancora si ricava il potenziale:
\[
\frac{d \varphi}{dx} = - \mathcal{E}
\]

La tensione di banda piatta $V_{FB}$ è la differenza di potenziale tra il bulk e il gate:
\[
-V_{FB}=V_{ox} +V_s
\]

\subsection{Assenza di equilibrio in regime stazionario nel tempo}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/10/Sistema_MOS_in_assenza_di_equilibrio.png}
\end{figure}

\noindent
Lo strato isolante intermedio impedisce il passaggio di corrente $\Rightarrow$ il transistore si comporta come un circuito aperto: scorre corrente $I_G$ nulla $\Rightarrow$ non c'è caduta di potenziale sulle regioni neutre, cioè $V_1=0$ sul gate e $V_2=0$ nella parte non svuotata del bulk, indipendentemente dalla tensione $V_G$ applicata $\Rightarrow$ la tensione $V_G$ si sovrappone solo alla tensione di banda piatta $-V_{FB}$ che si ha all'equilibrio sullo strato isolante ($V_{ox}$) e sulla regione svuotata del bulk ($V_s$):
\[
V_3=V_{ox}+V_s=-V_{FB}+V_G
\]

\subsubsection{\texorpdfstring{$V_G=V_{FB}$}{VG = VFB}: Condizione di banda piatta}
La \textbf{condizione di banda piatta} si ha quando si applica una tensione $V_G$ pari alla tensione di banda piatta $V_{FB}$, quindi quando è nulla la caduta di potenziale $V_3$ agli estremi della densità di carica $\rho$. Per l'equazione di Poisson la densità $\rho$ è nulla $\Rightarrow$ si annullano le cariche della regione di svuotamento e sull'interfaccia. Il diagramma a bande è rettilineo e analogo a quello precedente alla formazione della giunzione, perché il salto di energia $-qV_{FB}$ viene compensato totalmente dall'esterno. Il livello di Fermi non è più costante, ma continua a esserlo se si considerano singolarmente le due regioni neutre.

\subsubsection{\texorpdfstring{$V_{FB} < V_G < V_{th}$}{VFB < VG < Vth}: Svuotamento di lacune}
Al crescere della tensione applicata $V_G$ aumenta la curvatura delle bande, fino a che $V_G$ raggiunge il valore di soglia $V_{th}$ e il livello di Fermi intrinseco ${E_F}_i$ arriva ad intersecare il livello di Fermi del semiconduttore ${E_F}_s$.

La tensione di soglia $V_{th}$ vale:
\[
V_{th} = V_{FB} + 2 \phi_p + \gamma_B \sqrt{2 \phi_p - V_B}
\]
dove:
\begin{itemize}
\item $\phi_p$ è il salto di potenziale tra il livello di Fermi e quello intrinseco della regione neutra del bulk:
\[
q \phi_p = {E_F}_i - E_F = V_T \ln{\frac{N_A}{n_i}}
\]
\item $\gamma_B$ è il \textbf{coefficiente di substrato} (o di effetto body):
\[
\gamma_B = \frac{\sqrt{2q \epsilon_s N_A}}{C_{ox}}
\]
\item $V_B$ è il potenziale a cui si trova il bulk. L'\textbf{effetto body} (o di substrato) è legato alla variazione, in funzione di $V_B$, della tensione di soglia $V_{th}$ rispetto alla tensione di soglia ${V_{th}}_0$ che si ha quando il bulk è a massa ($V_B=0$):
\[
V_{th} = {V_{th}}_0 + \Delta V_{th} \left( V_B \right) = { \left.V_{th} \right| }_{V_B =0} + \gamma_B \left[ \sqrt{2 \phi_p - V_B} - \sqrt{2 \phi_p} \right]
\]
\end{itemize}

\subsubsection{\texorpdfstring{$V_G > V_{th}$}{VG > Vth}: Inversione di popolazione}
Oltrepassando il valore di soglia $V_{th}$, all'interfaccia ossido-semiconduttore ($x=0^+$) il livello di Fermi intrinseco ${E_F}_i$ scende sotto il livello di Fermi ${E_F}_s$, e la concentrazione $n(x)$ di elettroni liberi non è più trascurabile ma cresce esponenzialmente:
\[
{E_F}_i \left( 0^+ \right) < {E_F}_s \Rightarrow n \left( 0^+ \right) = n_i e^{\frac{{E_F}_s - {E_F}_i \left( 0^+ \right)}{k_B T}} > n_i
\]

Alla carica $Q_d=-qN_A x_p$ dovuta allo svuotamento di lacune si aggiunge quindi una carica $Q_n$ legata a questi elettroni liberi, detta \textbf{carica di inversione}. Lo strato di elettroni liberi, detto \textbf{canale}, ha uno spessore infinitesimo a causa della relazione di tipo esponenziale; nell'approssimazione di carica superficiale, il canale assume uno spessore nullo rappresentato con una delta di Dirac che, per la condizione di neutralità, rimane compensata dalla delta di Dirac nel metallo associata alla carica positiva $Q_t=-Q_d-Q_n$.

La carica di inversione $Q_n$ cresce {\small (in valore assoluto)} linearmente con la tensione $V_G> V_{th}$:\footnote{In realtà c'è un piccolo cambiamento graduale in $V_G \approx V_{th}$ che però si trascura nell'approssimazione a spezzata.}
\[
Q_n=-C_{ox} \left( V_G-V_{th} \right)
\]
dove il coefficiente angolare della retta è la capacità per unità di superficie dell'ossido $C_{ox}$ (u.m. $\tfrac{F}{ {cm}^3}$):
\[
C_{ox} = \frac{\epsilon_{ox}}{t_{ox}}
\]
\FloatBarrier

\section{Il transistore MOSFET}
\subsection{Definizione e struttura}
Il sistema \textit{n}MOS,\footnote{La $n$ specifica che il substrato è drogato $p$ e nel canale di inversione si formano elettroni liberi.} usato in regione di inversione ($V_G>V_{th}$), è la base dei transistori \textit{n}MOS \textbf{a effetto di campo} (FET), che sono dei dispositivi a 3 terminali dove la caratteristica di uscita $I_{DS} \left( V_G \right)$ dipende dalla tensione di ingresso $V_G$ ma non dalla tensione di uscita $V_{DS}$. Il transistore MOSFET è un \textbf{dispositivo monopolare} perché la corrente dipende dal flusso di un solo tipo di portatori.

Il \textbf{canale conduttivo} di elettroni liberi di lunghezza $L$ che si forma in regione di inversione può essere sfruttato per originare una \textbf{corrente di drain} $I_{DS}$ che, attraverso la carica $Q_n$, è controllata dalla tensione di ingresso $V_G$. Gli elettroni si muovono lungo il canale, per trascinamento dovuto al campo elettrico $\vec{\mathcal{E}}$, dal terminale di \textbf{source} a quello di \textbf{drain}, e danno origine così a una corrente $I_{DS}$ positiva dal drain al source.\footnote{Il dispositivo poiché simmetrico funziona analogamente con corrente in direzione opposta.} In corrispondenza dei due terminali vi sono due regioni con forte drogaggio $n$ allineate ai lati del gate, che grazie alla loro elevata conducibilità elettrica garantiscono continuità spaziale al passaggio di elettroni. Di solito il source e il bulk sono cortocircuitati ($V_{BS}=0$), così da garantire che la tensione di soglia $V_{th}$ non cambi al variare del potenziale di bulk $V_B=V_S$.

\subsection{Regime stazionario nel tempo}
Si definisce \textbf{potenziale di canale} $\varphi_{ch} (x)$ il potenziale della sezione $\Delta x$ infinitesima del canale rispetto al terminale di source. Il potenziale di canale $\varphi_{ch}$ è una funzione crescente\footnote{Caso particolare: è identicamente nullo se i terminali sono allo stesso potenziale: $V_S = V_D \Rightarrow V_{DS} = 0$.} lungo $x$ man mano che ci si allontana dal terminale di source,\footnote{In questo esempio, il source è collegato a massa: $V_s = 0$.} e agli estremi vale:\footnote{Si trascura l'effetto delle resistenze parassite $R_s$ e $R_d$ nelle regioni drogate $n^+$.}
\[
\begin{cases} \displaystyle \varphi_{ch} \left( 0 \right) = 0 \text{ (source)} \\
\displaystyle \varphi_{ch} \left( L \right) = V_{DS} \text{ (drain)} \end{cases}
\]

\subsubsection{Regione lineare (\texorpdfstring{$V_{DS} \to 0^+$}{VDS -> 0+})}
Il canale si comporta come un resistore a cui è applicata di caduta di potenziale $V_{DS}$. Se il terminale di drain è posto a un potenziale lievemente superiore rispetto al potenziale del terminale di source ($V_{DS} \to 0^+$) in modo che l'effetto del potenziale di canale $\varphi_{ch}$ sia minimo, la corrente $I_{DS}$ cresce linearmente con la tensione $V_{DS}$.

A parità di tensione $V_{DS}$, la pendenza della caratteristica inoltre aumenta al crescere della tensione $V_{GS}$ perché aumenta il numero di elettroni liberi da spostare nel canale e quindi cresce la transcaratteristica $I_{DS} \left( V_{GS} \right)$.

\subsubsection{Regione triodo/quadratica ($0 \ll V_{DS} < {V_{DS}}_S$)}
Il potenziale di canale $\varphi_{ch}$, sottraendosi sempre di più alla tensione $V_{GS}$ lungo $x$, ostacola il flusso di elettroni verso il drain:
\[
Q_n \left( x \right) = - C_{ox} \left( V_{GS} - \varphi_{ch} \left( x \right) - V_{th} \right)
\]
e frena quindi il crescere lineare della corrente $I_{DS}$ a un andamento parabolico con concavità verso il basso:
\[
I_{DS} = \beta_n \left[ \left( V_{GS} - V_{th} \right) V_{DS} - \frac{1}{2} {V_{DS}}^2 \right]
\]
dove: ($W$ è la larghezza del canale)
\[
\beta_n = \frac{W}{L} \mu_n C_{ox}
\]

\subsubsection{Punto di strozzamento (\texorpdfstring{$V_{DS} = {V_{DS}}_S$}{VDS = VDSS})}
Si verifica lo \textbf{strozzamento} (o pinch-off) del canale se è applicata al terminale di drain una tensione ${V_{DS}}_S$ che annulla il numero di elettroni liberi in corrispondenza del terminale di drain ($x=L$):
\[
Q_n \left( L \right) = - C_{ox} \left( V_{GS} - \varphi_{ch} \left( L \right)  - V_{th} \right) = 0 \Rightarrow {V_{DS}}_S = \varphi_{ch} \left( L \right) = V_{GS} - V_{th}
\]

L'andamento parabolico si arresta per la seguente tensione di strozzamento ${V_{DS}}_S$:
\[
{\left. \frac{d I_{DS}}{d V_{DS}} \right|}_{V_{DS} = {V_{DS}}_S} = \beta_n \left( V_{GS} - V_{th} - {V_{DS}}_S \right) = 0 \Leftrightarrow {V_{DS}}_S = V_{GS} - V_{th}
\]
corrispondente al seguente punto di massimo di $I_D$:
\[
I_D \left( {V_{DS}}_S \right) = \beta_n \left[ {\left( V_{GS} - V_{th} \right)}^2 - \frac{1}{2} {\left( V_{GS} - V_{th} \right)}^2 \right] = \frac{\beta_n}{2} {\left( V_{GS} - V_{th} \right)}^2
\]

\subsubsection{Regione di saturazione (\texorpdfstring{$V_{DS} > {V_{DS}}_S$}{VDS > VDSS})}
Il punto di strozzamento ($Q_n=0$) arretra in $L' < L$ tale che $\varphi_{ch} \left( L' \right) = {V_{DS}}_S$.

Poiché $I_{DS} \propto \tfrac{1}{L}$, l'accorciamento, detto \textbf{modulazione della lunghezza di canale}, aumenta la corrente $I_{DS}$.

Tuttavia, la distanza $\Delta L=L-L'$ converge a un valore non troppo grande, cioè l'influenza dell'accorciamento del canale sulla corrente è trascurabile $\Rightarrow$ in regione di saturazione ($V_{DS} > {V_{DS}}_S$), al variare della tensione di uscita $V_{DS}$ la caratteristica di uscita $I_{DS}$ rimane approssimativamente costante e pari a $I_{DS}  \left( {V_{DS}}_S \right)$ $\Rightarrow$ questa è la regione in cui il sistema \textit{n}MOS viene usato come transistore digitale.

Questo effetto di non idealità è tanto meno trascurabile quanto più è corto il canale, perché variazioni $\Delta L$ su una lunghezza $L$ piccola diventano significative. Il coefficiente correttivo $\lambda$ è legato alla pendenza (idealmente nulla) della caratteristica in regione di saturazione:
\[
I_D = \frac{\beta_n}{2} {\left( V_{GS} - V_{th} \right)}^2 \left[ 1+ \lambda \left( V_{DS} - {V_{DS}}_S \right) \right]
\]

\subsubsection{Regione di interdizione (\texorpdfstring{$V_{GS} \leq V_{th}$}{VGS <= Vth})}
Se il sistema \textit{n}MOS si trova al di fuori della regione di inversione, esso si comporta circuitalmente come un circuito aperto ($I_{DS} =0$), perché non si sono formati gli elettroni liberi da spostare nel canale.

A seconda del segno della tensione di soglia $V_{th}$, i transistori \textit{n}MOSFET si distinguono in:
\begin{itemize}
\item $V_{th} < 0$: \textit{n}MOS \textbf{a svuotamento} o \textbf{normalmente on} (all'equilibrio $V_G=0$ il canale è già presente);
\item $V_{th} >0$: \textit{n}MOS \textbf{ad arricchimento} o \textbf{normalmente off} (all'equilibrio $V_G=0$ il canale non si è ancora formato).
\end{itemize}

Siccome lo strato di ossido è molto sottile ed eventuali cariche fisse non ideali intrappolate al suo interno o sulla superficie possono variare la carica totale e la tensione di banda piatta, è possibile regolare la tensione di soglia $V_{th}$ impiantando $N_D'$ atomi donatori nel canale:
\[
V_{FB}' = V_{FB} + \Delta V_{th} = V_{FB} + \frac{q}{C_{ox}} \left( N_A' - N_D' \right)
\]
in modo che la riduzione della tensione di banda piatta $V_{FB}$ renda negativa la tensione di soglia $V_{th}$:
\[
V_{th} = V_{FB}' + 2 \phi_p + \gamma_B \sqrt{2 \phi_p - V_B} < 0
\]
e il transistore \textit{n}MOS diventi a svuotamento con \textbf{canale preformato}.

\subsubsection{Breakdown (\texorpdfstring{$V_{DS} \gg {V_{DS}}_S$}{VDS >> VDSS} o \texorpdfstring{$V_{GS} \gg V_{th}$}{VGS >> Vth})}
La corrente non deve entrare nella regione neutra del bulk attraverso le due giunzioni pn formate tra i terminali drogati $n^+$ e la regione neutra del bulk drogata $p$: per avere una corrente nulla ($I \simeq 0$) quindi queste due giunzioni non devono essere in polarizzazione diretta ($V \leq 0$), cioè il un potenziale di bulk $V_B$ dev'essere minore o uguale sia del potenziale di source $V_S$ sia di quello di drain $V_D$. Se entrambi i terminali di bulk e di source sono posti a massa, sulla giunzione source-bulk è applicata una tensione nulla ($V_S=V_B$) $\Rightarrow$ basta che la tensione $V_{DB} = V_{DS}$ applicata alla giunzione drain-bulk sia positiva o nulla.\footnote{Si noti il verso della tensione $V_{DS}$: se positiva polarizza inversamente la giunzione.}

\begin{itemize}
\item $V_{DS} \gg {V_{DS}}_S$: Una tensione di uscita $V_{DS}$ eccessiva può però portare una delle due giunzioni al breakdown. Inoltre, l'elevato campo elettrico generato nel canale può provocare un effetto valanga. Elevate tensioni di ingresso $V_{GS}$ tendono ad amplificare questo effetto, perché il breakdown si instaura prima per tensioni $V_{GS}$ crescenti.
\item $V_{GS} \gg V_{th}$: Elevate tensioni di ingresso $V_{GS}$ possono provocare campi elettrici che superano la rigidità dielettrica\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Rigidità_dielettrica}{Rigidità dielettrica} su Wikipedia in italiano.} dell'ossido distruggendolo.
\end{itemize}

Inoltre non si devono superare la tensione, la corrente e la potenza dissipata massime specificate dal produttore del dispositivo.

\subsection{Regime dinamico nel tempo}
\subsubsection{Modello statico di piccolo segnale}
Il modello statico di piccolo segnale viene ricavato in regione di saturazione:
\[
{i_D}_{ss} (t) = g_m {v_{GS}}_{ss} (t) + g_o {v_{DS}}_{ss} (t) g_{mB} {v_{BS}}_{ss} (t)
\]
dove:
\begin{itemize}
\item $g_m$ è la \textbf{transconduttanza}:\footnote{Qui il coefficiente $\lambda$ è stato trascurato.}
\[
g_m = {\left. \frac{\partial i_D}{\partial v_{GS}} \right|}_{\left( {V_{GS}}_0 , {V_{DS}}_0 , {V_{BS}}_0 \right)} = \sqrt{2 \beta_n {I_D}_0}
\]
\item $g_o$ è la conduttanza di uscita:
\[
g_o = {\left. \frac{\partial i_D}{\partial v_{DS}} \right|}_{\left( {V_{GS}}_0 , {V_{DS}}_0 , {V_{BS}}_0 \right)} = \lambda {I_D}_0
\]
\item $g_{mB}$ è la transconduttanza di substrato:\footnotemark[\value{footnote}]
\[
g_{mB} = {\left. \frac{\partial i_D}{\partial v_{BS}} \right|}_{\left( {V_{GS}}_0 , {V_{DS}}_0 , {V_{BS}}_0 \right)} = \frac{\lambda_B g_m}{2 \sqrt{2 \varphi_p - {V_{BS}}_0}}
\]
\end{itemize}

\subsubsection{Modello dinamico di piccolo segnale}
Nel modello dinamico, gli effetti capacitivi sono rappresentati da condensatori che si aggiungono al circuito equivalente. A frequenze troppo elevate possono però essi degradare le prestazioni del dispositivo.