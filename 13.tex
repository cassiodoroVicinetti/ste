\chapter{Circuiti logici: specifiche funzionali}
\section{Segnali logici}
In elettronica gli stati logici 0 e 1 sono associati ai due valori di tensione alta $V_H \approx V_{AL}$ e bassa $V_L \approx \text{GND} =0$:
\begin{itemize}
\item convenzione \textbf{logica positiva}: $1 \longleftrightarrow V_H$  |  $0 \longleftrightarrow V_L$
\item convenzione \textbf{logica negativa}: $0 \longleftrightarrow V_H$  |  $1 \longleftrightarrow V_L$
\end{itemize}

Si definisce una tensione di soglia $V_{TR}$ al di sotto della quale il segnale analogico viene riconosciuto nello stato logico $L$, e viceversa.

\subsection{Compatibilità tra porte}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/13/Compatibilita_tra_porte_logiche.png}
\end{figure}

\noindent
Per \textbf{compatibilità} si intende la capacità di circuiti connessi in cascata di scambiarsi correttamente stati logici: gli ingressi devono interpretare correttamente i livelli di tensione.

Per un segnale analogico proveniente dall'uscita di un circuito digitale, sono definiti due valori limite di tensione:
\begin{itemize}
\item $V_{OH}$ è il valore di tensione minimo per l'uscita $U$ allo stato $H$;
\item $V_{OL}$ è il valore di tensione massimo per l'uscita $U$ allo stato $L$.
\end{itemize}

Affinché questo segnale venga riconosciuto correttamente all'ingresso di un circuito digitale, sono definiti altri due valori limite:
\begin{itemize}
\item $V_{IH} \leq V_{OH}$ è il valore di tensione minimo per l'ingresso $I$ allo stato $H$;
\item $V_{IL} \geq V_{OL}$ è il valore di tensione massimo per l'ingresso $I$ allo stato $L$.
\end{itemize}

Due circuiti logici appartenenti alla stessa \textbf{famiglia logica} hanno le stesse caratteristiche elettriche (alimentazione, tensioni e correnti di uscita e di ingresso, ritardi, consumo) $\Rightarrow$ sono elettricamente compatibili tra di loro.
\FloatBarrier

\subsection[Comparatore di soglia]{Comparatore di soglia\footnote{Per approfondire, si veda il capitolo ``Comparatori di soglia'' negli appunti di \textit{Elettronica applicata e misure}.}}
Conviene evitare valori limite ingresso-uscita uguali garantendo un \textbf{margine di rumore} per ridurre l'effetto dei disturbi:
\[
\begin{cases} \displaystyle V_{IH} < V_{OH} \\
\displaystyle V_{IL} > V_{OL} \end{cases} \Rightarrow \begin{cases} \displaystyle {\text{NM}}_H = V_{OH} - V_{IH} \\
\displaystyle {\text{NM}}_L = V_{IL} - V_{OL} \end{cases}
\]

Per recuperare un segnale digitale disturbato, si può interporre fra due circuiti digitali un \textbf{comparatore di soglia}, un modulo che realizza una funzione a gradino: converte un ingresso analogico, in base a un unico valore di soglia $S$, in un valore logico/binario in uscita.

Se il segnale è molto disturbato e oscilla frequentemente attorno al valore di soglia, è preferibile usare un comparatore di soglia con \textbf{istèresi}, che ha due valori di soglia $S_1$ e $S_2$: il valore di soglia $S_1$ viene attivato quando il segnale è crescente nel tempo (da $L$ a $H$), e viceversa. Il trigger di Schmitt è un circuito che approssima il comportamento del comparatore di soglia con isteresi.

\section{Invertitori \textit{R}-switch}
\subsection{Invertitore \textit{n}MOS: uscita a vuoto (\texorpdfstring{$R_L \rightarrow + \infty$}{RL +inf})}
L'invertitore a transistore \textit{n}MOS è costituito da una \textbf{resistenza di pull-up} $R_{PU}$ verso l'alimentazione $V_{AL}$ e da un interruttore a transistore \textit{n}MOS verso massa $\text{GND}$:
\begin{itemize}
\item $I = L, U = H$: la tensione di ingresso $V_I=V_{GS}=0$ è minore della tensione di soglia $V_{th}$ $\Rightarrow$ il transistore è aperto e in interdizione $\Rightarrow$ la corrente che scorre nella resistenza di pull-up $R_PU$ è nulla $\Rightarrow$ la tensione di uscita $V_O$ è ``portata su'' alla tensione di alimentazione $V_{AL}$;
\item $I = H, U = L$: la tensione di ingresso $V_I=V_{GS}$ è maggiore della tensione di soglia $V_{th}$ $\Rightarrow$ il transistore è chiuso e in conduzione $\Rightarrow$ la tensione $V_{DS}$ di pull-down è nulla $\Rightarrow$ la tensione di uscita $V_O$ è ``portata giù'' a massa ($V_O=0$).
\end{itemize}

Se si considerano anche le non idealità dell'invertitore:
\begin{itemize}
\item $U = L$: la tensione $V_{AL}$ si ripartisce anche sulla resistenza $R_{\text{on}}$;
\item $U = H$: una parte della corrente si disperde nella corrente di perdita $I_{\text{off}}$.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/13/Caratteristica_nm_invertitore.jpg}
	\caption[]{Transcaratteristica di un invertitore reale\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Caratteristica_nm_invertitore.jpg}{Caratteristica nm invertitore.jpg}), è stata realizzata da \href{https://it.wikipedia.org/wiki/Utente:Hachreak}{Hachreak} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0}.}

La \textbf{transcaratteristica} $V_O \left( V_I \right)$ di un invertitore reale non è brusca ma segue una variazione continua e graduale attraverso uno stato logico non definito. Quando l'uscita è allo stato basso $L$ la resistenza di pull-up dissipa potenza.

Sulla transcaratteristica, i valori limite di tensione sono definiti di solito come i punti in cui le tangenti al grafico hanno pendenza 45\textsuperscript{$\circ$}.
\FloatBarrier

\subsection{Invertitore \textit{n}MOS: carico resistivo \texorpdfstring{$R_L$}{RL}}
Se il carico $R_L$ è collegato verso massa, la tensione $V_O$ è inferiore a quella che si ottiene nel caso di carico ideale:
\begin{table}[H]
\centering
\centerline{\begin{tabular}{|c|c|}
\hline
$\boldsymbol{U=L}$ & $\boldsymbol{U=H}$ \\
\hline
$\displaystyle V_O= \frac{R_L || R_{\text{on}}}{R_L || R_{\text{on}} + R_{PU}} V_{AL} > \frac{R_{\text{on}}}{R_{\text{on}} + R_{PU}} V_{AL}$ & $\displaystyle V_O = \frac{R_L}{R_L + R_{PU}} V_{AL} - I_{\text{off}} \left( R_{PU} || R_L \right) > V_{AL} - I_{\text{off}} R_{PU}$ \\
\hline
\end{tabular}}
\end{table}

Viceversa, se il carico è collegato verso massa, la tensione $V_O$ è superiore a quella che si ottiene nel caso di carico ideale.

\subsection{Invertitore \textit{p}MOS}
Poiché nel transistore \textit{p}MOS le posizioni di source e drain risultano scambiate,\footnote{La corrente deve scorrere dall'alimentazione verso massa.} se esso venisse collegato verso massa la sua tensione di gate $V_{GS}$ sarebbe controllata anche dalla tensione di uscita $V_{DS} \equiv V_O$ $\Rightarrow$ non sarebbe un invertitore:
\[
V_{GS}=V_I-V_{DS}
\]

L'invertitore a transistore \textit{p}MOS ha quindi una \textbf{resistenza di pull-down} $R_{PD}$ verso massa, in modo che il controllo della commutazione dell'invertitore sia esercitato dalla sola tensione di ingresso:
\[
V_{GS} =V_I-V_{AL}
\]

La scelta della tensione di soglia $V_{th}$ ha delle restrizioni:
\begin{itemize}
\item aperto/interdizione: $V_I=V_{AL} \Rightarrow V_{GS} =0 > V_{th} \Rightarrow V_O=0$ (la tensione di soglia $V_{th}$ dev'essere negativa);
\item chiuso/conduzione: $V_I=0 \Rightarrow V_{GS} =-V_{AL} < V_{th}  \Rightarrow V_O=V_{AL}$ (la tensione di soglia $V_{th}$ dev'essere minore (in valore assoluto) della tensione di alimentazione $V_{AL}$).
\end{itemize}

\section{Invertitore CMOS}
\subsection{Uscita a vuoto}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/13/CMOS_inverter.png}
\end{figure}

\noindent
In un \textbf{invertitore CMOS} (o a MOS complementari) entrano alternativamente in funzione due transistori di polarità opposte: l'\textit{n}MOS sostituisce la resistenza di pull-down $R_{PD}$, il \textit{p}MOS la resistenza di pull-up $R_{PU}$, e ciascun transistore ha il terminale di drain verso quello dell'altro. Ogni transistore è caratterizzato da una propria tensione di soglia:
\begin{itemize}
\item \textit{n}MOS: la tensione di soglia ${V_{th}}_n$ deve essere positiva e minore della tensione di alimentazione $V_{AL}$;
\item \textit{p}MOS: la tensione di soglia ${V_{th}}_p$ deve essere negativa e maggiore della tensione di alimentazione $-V_{AL}$.
\end{itemize}

La tensione d'ingresso $V_I={V_{GS}}_n$ è applicata al transistore \textit{n}MOS di pull-down $\Rightarrow$ sul transistore \textit{p}MOS di pull-up è applicata una tensione:
\[
{V_{GS}}_p=V_I-V_{AL}
\]

Se $\left| {V_{th}}_p \right| < V_{AL} - {V_{th}}_n$, a seconda della tensione di ingresso $V_I$ si distinguono tre casi:
\begin{itemize}
\item $0<V_I< {V_{th}}_n$: \textit{n}MOS interdetto, \textit{p}MOS in conduzione $\Rightarrow$ $U = H$: il carico è portato all'alimentazione;\footnote{Si suppone un carico ideale: $R_L \to + \infty \Rightarrow I= 0$.}
\item ${V_{\text{th}}}_p <V_I<0$: \textit{n}MOS in conduzione, \textit{p}MOS interdetto $\Rightarrow$ $U = L$: il carico è portato a massa;
\item ${V_{th}}_p <V_I< {V_{th}}_n$: \textit{n}MOS in conduzione, \textit{p}MOS in conduzione $\Rightarrow$ l'uscita non corrisponde a uno stato logico definito.
\end{itemize}

Ciascun transistore ammette la rappresentazione circuitale di perdita.

Un invertitore CMOS è anche rappresentabile con due interruttori \textit{p}-switch e \textit{n}-switch oppure con un unico deviatore.

\paragraph{Vantaggi rispetto agli invertitori \textit{R}-switch}
\begin{itemize}
\item sono fisicamente molto più piccoli rispetto agli invertitori \textit{R}-switch;
\item hanno una struttura simmetrica e un comportamento simmetrico negli stati $H$ e $L$;
\item in condizioni statiche hanno in entrambi gli stati un consumo di potenza trascurabile;
\item hanno una transcaratteristica $V_O \left( V_I \right)$ di pendenza maggiore rispetto a quella degli invertitori \textit{R}-switch $\Rightarrow$ la regione in cui lo stato logico non è definito è molto più stretta $\Rightarrow$ migliori prestazioni logiche. Se la pendenza è idealmente verticale, lo stato logico non definito coincide con l'unica tensione di soglia $V_TR$.
\end{itemize}
%\FloatBarrier