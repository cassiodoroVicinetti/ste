\chapter{I semiconduttori}
\paragraph{Classificazione dei materiali per conducibilità elettrica}
I materiali \textbf{semiconduttori} (soprattutto il silicio) sono usati per i transistori.

La \textbf{conducibilità elettrica} $\sigma = \tfrac{J}{\mathcal{E}}$\footnote{$J= \tfrac{I}{A}$ è la \textbf{densità di corrente}, cioè la corrente che scorre per unità di area; $\vec{\mathcal{E}}$ è il campo elettrico.} e la \textbf{resistività elettrica} $\rho = \tfrac{1}{\sigma}$ sono i parametri che caratterizzano la capacità di condurre corrente di un materiale:
\begin{itemize}
\item \ul{isolanti/dielettrici:} (es. quarzo) la corrente che scorre è nulla qualunque sia la caduta di potenziale ($\sigma \to 0$);
\item \ul{conduttori:} (es. metalli) la caduta di potenziale è nulla qualunque sia la corrente che scorre ($\rho \to 0$);
\item \ul{semiconduttori:} (es. silicio) hanno delle caratteristiche intermedie tra isolanti e conduttori.
\end{itemize}

La corrente, poiché è la variazione di quantità di carica nel tempo, viene generata dallo spostamento di cariche elettriche: idealmente negli isolanti nessuna carica elettrica può essere messa in moto, mentre nei conduttori anche un campo elettrico infinitesimo può mettere in moto infinite cariche elettriche.

\section{Semiconduttori intrinseci}
\subsection{Elettroni in una struttura cristallina}
La maggior parte dei materiali è composta da atomi tutti uguali e idealmente disposti in una \textbf{struttura cristallina}, cioè disposti non in modo casuale ma su un reticolo periodico e alla stessa distanza (= \textbf{passo reticolare}) l'uno dall'altro. Nel singolo atomo l'energia può assumere solo certi valori discreti. In un reticolo cristallino, se il passo reticolare è abbastanza piccolo da permettere agli atomi di non rimanere isolati e interagire tra loro, i valori discreti si aprono in intervalli energetici consentiti detti \textbf{bande di energia}.

Anche per le bande di energia vale il \textbf{principio di Pauli}: non possono coesistere più di due elettroni all'interno della stessa banda di energia. Gli elettroni contenuti nei gusci più interni rimangono legati all'atomo e non possono essere messi in moto; quelli nei gusci più esterni, detti \textbf{elettroni di valenza},\footnote{Con ``elettroni di valenza'' non s'intendono solo gli elettroni in BV, ma sono compresi anche gli elettroni in BC.} possono invece essere messi in moto sotto opportune condizioni e dare origine a un flusso di corrente.
	
\subsection{Modello di Shockley}
\label{sez:modello_shockley}
Molti elementi semiconduttori si trovano nel IV gruppo $\Rightarrow$ ogni atomo isolato ha 4 elettroni di valenza: 2 elettroni nel livello $s$ e 2 nei livelli $p$ (4 posti liberi). In una struttura cristallina, i livelli $s$ e $p$ di ogni atomo si aprono in due bande di energia: la \textbf{banda di valenza} (BV) e la \textbf{banda di conduzione} (BC). Le bande hanno ciascuna banda 4 \textbf{stati} (= posti) disponibili, e sono separate dalla \textbf{banda proibita} (BP).

La \textbf{conducibilità elettrica} è la capacità del materiale di mettere in moto elettroni liberi in BC quando sottoposto a un campo elettrico.

Il campo elettrico $\vec{\mathcal{E}}$ esercita una forza opposta ad esso su tutti gli elettroni:
\[
\vec{F} = -q \vec{\mathcal{E}} = m^* \cdot \vec a
\]
dove $m^*$ è la \textbf{massa efficace} dell'elettrone, che è una proprietà del reticolo cristallino e serve per semplificare lo studio dell'azione di un campo elettrico esterno su un elettrone escludendo le interazioni di quest'ultimo con le altre cariche elettriche nel solido.

A temperatura $0 \; K$, tutti i 4 elettroni di valenza occupano l'intera BV, lasciando vuota la BC che ha un'energia superiore. Gli elettroni si possono scambiare, ma lo spostamento di carica di uno bilancia quello opposto dell'altro $\Rightarrow$ il materiale è un isolante perfetto.

Se la temperatura è sufficientemente elevata da permettergli di superare la BP, un elettrone viene promosso nella BC, lasciando un posto libero in BV, e così si può muovere liberamente per tutto il materiale senza essere vincolato a un atomo (\textbf{elettrone libero}/delocalizzato). Invece, gli elettroni in BV si possono muovere solo nei posti lasciati liberi; in realtà non si considera il moto dell'elettrone, ma il moto in verso opposto di una carica positiva $+q$ virtuale, detta \textbf{lacuna}.

Ogni elettrone in BV è legato in modo covalente a un elettrone di un altro atomo. La promozione in BC significa lo spezzarsi di un legame: gli elettroni in BC quindi non hanno più legami e si possono muovere liberamente nel cristallo. Lo spostamento di una lacuna significa che un elettrone si lega a un elettrone di un altro atomo. A temperature troppo elevate, il numero di legami covalenti spezzati può essere tanto grande da ``sciogliere'' la struttura cristallina del materiale.

Gli elettroni in BC si addensano negli stati a energia più bassa, a partire da $E_c$. Le lacune (positive) in BV si addensano negli stati a energia dal punto di vista degli elettroni più alta, fino a $E_v$, e dal punto di vista delle lacune più bassa.

\paragraph{Meccanismo diretto o banda-banda}
La promozione di un elettrone in BC corrisponde alla \textbf{generazione} di una coppia elettrone-lacuna. Viceversa, il ``declassamento'' di un elettrone in BV corrisponde alla \textbf{ricombinazione} di una coppia elettrone-lacuna.

Il \textbf{tasso netto di ricombinazione} $U$ è la differenza tra il \textbf{tasso di ricombinazione} $R$ e il \textbf{tasso di generazione} $G$, cioè il numero di coppie rispettivamente generate/ricombinate per unità di tempo e volume.

\subsection{Concentrazione intrinseca}
Un semiconduttore si dice \textbf{intrinseco} o puro se all'equilibrio termodinamico la concentrazione degli elettroni liberi $n$ è uguale a quella delle lacune $p$:
\[
n=p=n_i
\]
dove:
\begin{itemize}
\item $n$ è la concentrazione/cm\textsuperscript{3} di elettroni in BC;
\item $p$ è la concentrazione/cm\textsuperscript{3} di lacune in BV;
\item $n_i$ è la \textbf{concentrazione intrinseca}.
\end{itemize}

Vale la \textbf{legge dell'azione di massa}:
\[
np = {n_i}^2
\]
e continuerà a valere anche in un semiconduttore non intrinseco (drogato).

Il salto da BV a BC è ostacolato dalla \ul{banda proibita}: più ampia è la banda proibita, meno elettroni passano in BC.

Un aumento di \ul{temperatura} riduce l'ampiezza di banda proibita $E_g=E_c-E_v$, che così aiuta a spezzare i legami covalenti.

La concentrazione intrinseca cresce esponenzialmente al diminuire dell'ampiezza della banda proibita, e rapidamente al crescere della temperatura.

La concentrazione intrinseca misura la sensibilità alle variazioni di equilibrio: i fenomeni di generazione e di ricombinazione all'equilibrio si bilanciano tra di loro ($R=G \Rightarrow U=0$), cioè a una generazione corrisponde una ricombinazione, perché lo spostamento di una carica elettrica, se non venisse controbilanciato dallo spostamento di un'altra carica in verso opposto, provocherebbe una variazione di energia che farebbe perdere l'equilibrio termodinamico.

\subsection{Dispositivi a semiconduttore}
\begin{itemize}
\item I dispositivi elettronici non possono lavorare a temperature troppo elevate, perché a causa delle loro piccole dimensioni dissipano molta energia termica $\Rightarrow$ aumento di temperatura $\Rightarrow$ diminuzione della conducibilità elettrica $\Rightarrow$ diminuzione delle prestazioni.

\item I \textbf{trasduttori} sono dei dispositivi fotorivelatori che convertono segnali ottici in elettrici fornendo agli elettroni energia sotto forma di fotoni e promuovendoli di banda.

\item L'ampiezza di banda proibita di un materiale semiconduttore è intorno a $1 \; eV$.\footnote{L'\textbf{elettronvolt} è un'unità di misura per l'energia: $1 \; eV$ è l'energia potenziale che assume una carica $q$ quando sottoposta a una differenza di potenziale applicata di $1 \; V$ ($1 \; eV = 1,6 \times 10^{-19} \; J$).} Se il fotone ha una energia inferiore all'ampiezza di banda proibita, non viene assorbito dal materiale che appare così trasparente.

\item L'ampiezza della banda proibita è la minima energia che deve assorbire un elettrone per salire di banda, nonché la minima energia ceduta dalla ricombinazione della coppia elettrone-lacuna $\Rightarrow$ grazie alla tecnologia dei semiconduttori si possono realizzare LED e laser che possono emettere fotoni aventi un'energia pari o superiore all'ampiezza di banda proibita.
\end{itemize}

\section{Drogaggio}
\textbf{Drogare} un materiale significa sostituire un certo numero di atomi con \textbf{atomi droganti} appartenenti a un altro gruppo, che ionizzandosi variano il numero di portatori liberi e così la conducibilità elettrica del materiale.

Una variazione significativa della conducibilità elettrica richiede una variazione del numero di portatori liberi di un ordine di grandezza maggiore o uguale a quello della concentrazione intrinseca. I semiconduttori sono più facili da drogare perché hanno una concentrazione intrinseca molto inferiore a quella dei metalli, i quali richiederebbero l'inserimento di un numero eccessivo di atomi droganti che farebbe perdere le proprietà/caratteristiche intrinseche del cristallo.

Il \textbf{livello di ionizzazione} è il rapporto tra $N^\pm$, la concentrazione/cm\textsuperscript{3} degli atomi droganti che si sono effettivamente ionizzati, e $N$, la concentrazione totale/cm\textsuperscript{3} degli atomi droganti inseriti. A temperatura abbastanza elevata (per il silicio è sufficiente la temperatura ambiente $T = 300 \; K$) si raggiunge la \textbf{completa ionizzazione}, cioè tutti gli atomi droganti si ionizzano: $N^\pm \approx N$.

\subsection{Equazioni di Boltzmann}
All'equilibrio termodinamico, nei semiconduttori non degeneri\footnote{Un semiconduttore è non degenere se il suo livello di Fermi $E_F$ si trova nella banda proibita BP.} le concentrazioni $n$ e $p$ sono legate tra loro attraverso $E_F$ dalle \textbf{equazioni di Boltzmann}:
\[
\begin{cases} \displaystyle n \approx N_c \cdot e^{- \frac{E_c - E_F}{k_B T}} \\
\displaystyle p \approx N_v \cdot e^{- \frac{E_F - E_v}{k_B T}} \end{cases}
\]
dove:
\begin{itemize}
\item $N_c$ e $N_v$ sono le \textbf{densità efficaci degli stati} in BC e BV:
\[
\begin{cases} \displaystyle N_c = K_c \sqrt{T^3} \\
\displaystyle N_v = K_v \sqrt{T^3} \end{cases}
\]
dove $K_c$ e $K_v$ sono delle proprietà intrinseche del materiale che non dipendono dalla temperatura;
\item $k_B$ è la \textbf{costante di Boltzmann} (a $T=300 \; K$: $k_B T=26 \; meV$);
\item $E_c$ è la minima energia in BC, e $E_v$ è la massima energia in BV;
\item $E_F$ è detta \textbf{livello di Fermi}, e dipende dal livello di drogaggio.
\end{itemize}

La concentrazione intrinseca $n_i$ è indipendente dal livello di Fermi $E_F$ e quindi si mantiene costante al variare del drogaggio:
\[
{n_i}^2 = np = N_c N_v e^{- \frac{E_c - E_v}{k_B T}} = N_c N_v e^{- \frac{E_g}{k_B T}}
\]

Le concentrazioni $n$ e $p$ sono inversamente proporzionali: all'equilibrio termodinamico, un aumento della concentrazione $n$ dovuto al drogaggio implica una diminuzione della concentrazione $p$, e viceversa.

\subsection{Equazioni di Shockley}
Il \textbf{livello di Fermi intrinseco} ${E_F}_i$ è la posizione del livello di Fermi $E_F$ in assenza di drogaggio, e se {\small (come nel silicio)} $N_c \approx N_v$, esso si trova circa a metà della banda proibita:
\[
n= p \Rightarrow {E_F}_i = \frac{E_c + E_v}{2} - \frac{1}{2} k_B T \ln{\frac{N_c}{N_v}} \approx \frac{E_c + E_v}{2}
\]

Sostituendo nelle equazioni di Boltzmann $N_c$ e $N_v$ ricavati nel caso intrinseco, si ottengono le \textbf{equazioni di Shockley}:
\[
\begin{cases} \displaystyle n \approx n_i \cdot e^{\frac{E_F - {E_F}_i}{k_B T}} \\
\displaystyle p \approx n_i \cdot e^{\frac{{E_F}_i - E_F}{k_B T}} \end{cases}
\]

\subsection{Drogaggio di tipo \texorpdfstring{$n$}{n}}
Si inseriscono atomi droganti, in concentrazione $N_D$, appartenenti a un gruppo superiore: gli elettroni eccedenti degli atomi droganti \textbf{donatori}, in concentrazione ${N_D}^+$ hanno così estrema facilità di essere ceduti alla BC del cristallo, aumentando la concentrazione $n$ e quindi la conducibilità elettrica. Gli atomi droganti si ionizzano con una carica positiva: essa non è una lacuna, ma è un protone in eccesso che rimane nel nucleo.

All'aumentare del livello di drogaggio di tipo $n$, il livello di Fermi $E_F$ si avvicina a $E_c$ e si allontana da $E_v$.

Le concentrazioni di elettroni liberi e di lacune dovute al drogaggio di tipo $n$ si indicano rispettivamente con $n_n$ e $p_n$.

\subsection{Drogaggio di tipo \texorpdfstring{$p$}{p}}
Si inseriscono atomi droganti, in concentrazione $N_A$, appartenenti un gruppo inferiore $\Rightarrow$ aumenta la concentrazione $p$ di lacune $\Rightarrow$ gli atomi droganti \textbf{accettatori}, in concentrazione ${N_A}^+$, sottraggono elettroni dalla BV di altri atomi e si ionizzano negativamente, riuscendo a realizzare i legami covalenti mancanti. È necessario scegliere un tipo di atomo che non sottragga elettroni dalla BC ma solo dalla BV.

All'aumentare del livello di drogaggio di tipo $p$, il livello di Fermi $E_F$ si avvicina a $E_v$ e si allontana da $E_c$.

Le concentrazioni di elettroni liberi e di lacune dovute al drogaggio di tipo $p$ si indicano rispettivamente con $n_p$ e $p_p$.

\subsection{Drogaggio netto donatore}
Un materiale è \ul{globalmente} neutro se la sua carica totale è nulla, cioè il numero totale di cariche positive è uguale a quello di cariche negative. Se il semiconduttore neutro è \textbf{omogeneo} {\small (= le sue proprietà non cambiano con la posizione)}, allora la proprietà di neutralità vale anche \ul{localmente} {\small (= punto per punto)} (\textbf{condizione di neutralità locale}): le concentrazioni di carica positiva $p$ e ${N_D}^+$ sono localmente compensate da quelle di carica negativa $n$ e ${N_A}^-$:\footnote{Si considera la completa ionizzazione a temperatura ambiente: ${N_D}^+ \approx N_D$ e ${N_A}^- \approx N_A$.}
\[
n+N_A=p+N_D \Rightarrow n-p=N_D-N_A=N^+
\]
dove $N^+$ è il drogaggio netto donatore.

Se un semiconduttore subisce sia il drogaggio di tipo $n$ sia quello di tipo $p$, si comporta complessivamente come se avesse subito un drogaggio con una concentrazione equivalente di \textbf{portatori di maggioranza} pari al valore assoluto del drogaggio netto donatore $N^+$, perché il drogaggio minore compensa una parte del drogaggio maggiore (\textbf{legge di compensazione}). Se però prevale uno dei drogaggi, l'effettiva concentrazione dei portatori di maggioranza tende a $\left| N^+ \right|$:
\begin{itemize}
\item prevalenza di drogaggio di tipo $n$: (i portatori di maggioranza sono gli elettroni liberi)
\[
\begin{cases} \displaystyle N^+ = n-p > 0 \\
\displaystyle np = {n_i}^2 \end{cases} \Rightarrow N^+ = n - \frac{{n_i}^2}{n} \Rightarrow \begin{cases} \displaystyle n = \frac{N^+}{2} \left[ 1+ \sqrt{1+ {\left( \frac{2n_i}{N^+} \right)}^2} \right] \\ \displaystyle n_i \ll N^+ \end{cases} \Rightarrow \begin{cases} \displaystyle n \approx N^+ \\
\displaystyle p = \frac{{n_i}^2}{n} \approx \frac{{n_i}^2}{N^+} \end{cases}
\]
\item prevalenza di drogaggio di tipo $p$: (i portatori di maggioranza sono le lacune)
\[
\begin{cases} \displaystyle N^+ = n-p < 0 \\
\displaystyle np = {n_i}^2 \end{cases} \Rightarrow \left| N^+ \right| = p - \frac{{n_i}^2}{p} \Rightarrow \begin{cases} \displaystyle p = \frac{N^+}{2} \left[ 1+ \sqrt{1+ {\left( \frac{2n_i}{\left| N^+ \right|} \right)}^2} \right] \\ \displaystyle n_i \ll \left| N^+ \right| \end{cases} \Rightarrow \begin{cases} \displaystyle p \approx \left| N^+ \right| \\
\displaystyle n = \frac{{n_i}^2}{p} \approx \frac{{n_i}^2}{\left| N^+ \right|} \end{cases}
\]
\end{itemize}

\subsection{Condizioni di non degenerazione}
Un eccessivo drogaggio può portare a un materiale degenere se l'energia di Fermi $E_F$ esce dall'intervallo tra $E_c$ e $E_v$:
\begin{itemize}
\item drogaggio di tipo $n$:
\[
n=N_c \cdot e^{- \frac{E_c - E_F}{k_B T}} \approx N_D \Rightarrow E_F=E_c-k_B T \cdot \ln{\frac{N_c}{N_D}} < E_c \Leftrightarrow N_D < N_c
\]
\item drogaggio di tipo $p$:
\[
p=N_v \cdot e^{- \frac{E_F - E_v}{k_B T}} \approx N_A \Rightarrow E_F=E_v+k_B T \cdot \ln{\frac{N_v}{N_A}} > E_v \Leftrightarrow N_A < N_v
\]
\end{itemize}

\subsection{Mobilità}
La \textbf{mobilità} $\mu$ è un parametro caratteristico del tipo di materiale ed è proporzionale alla sua conducibilità elettrica. In un dispositivo a semiconduttore, la mobilità $\mu$ è inversamente proporzionale al \textbf{tempo di transito} $\tau$, definito come il tempo impiegato da un elettrone per attraversare una certa lunghezza fisica $L$. \marginpar{da chiarire}Siccome il dispositivo fisico impone un limite minimo $\tau$ all'intervallo di tempo in cui la corrente può variare, la mobilità è proporzionale alle prestazioni in frequenza, ovvero alla frequenza (= velocità di variazione) massima $f_{\text{max}}$ che il dispositivo può processare:
\[
\begin{cases} \displaystyle v= \frac{L}{\tau} \\ \displaystyle v = \mu E \end{cases} \Rightarrow \begin{cases} \displaystyle \mu \propto \frac{1}{\tau} \\
\displaystyle \omega_{\text{max}} = 2 \pi f_{\text{max}} = \frac{1}{\tau}  \end{cases} \Rightarrow f_{\text{max}} \propto \mu
\]

Elevate temperature comportano una riduzione della mobilità e quindi un peggioramento delle prestazioni.

La mobilità $\mu$ diminuisce significativamente al crescere del drogaggio totale $N_T$, cioè la concentrazione totale di atomi droganti $N_A+N_D$: fissato un drogaggio netto $N^+$, un semiconduttore avrà mobilità massima se il drogaggio non è compensato e i portatori minoritari danno un contributo nullo. La mobilità è legata empiricamente al drogaggio netto mediante la seguente espressione:
\[
\mu = \mu_{\text{min}} + \frac{\mu_{\text{max}} - \mu_{\text{min}}}{1+ {\left( \frac{N_T}{N_{\text{ref}}} \right)}^\alpha}
\]

\section{Assenza di equilibrio termodinamico}
Nella realtà i dispositivi a semiconduttore non lavorano in condizioni di equilibrio termodinamico, ma per processare segnali scambiano energia con l'esterno. Le concentrazioni di carica libera $n$ e $p$ in assenza di equilibrio termodinamico non sono più costanti, ma anche in un campione omogeneo variano in base alla posizione $x$ (ad es. il semiconduttore è illuminato da un solo lato) e al tempo $t$ (ad es. viene introdotta una corrente/tensione variabile nel tempo).

Fissando in $n_0$ e $p_0$ le concentrazioni di portatori liberi all'equilibrio termodinamico, si definiscono le \textbf{concentrazioni in eccesso} $n'$ e $p'$ come le variazioni di concentrazione rispetto alla condizione di equilibrio:
\[
\begin{cases} \displaystyle n=n_0+n' \\ \displaystyle p=p_0 +p' \end{cases}
\]
\begin{itemize}
\item \textbf{iniezione:} le concentrazioni in eccesso sono positive $\Rightarrow$ si ha un eccesso di portatori liberi: $n,p>n_0,p_0$;
\item \textbf{svuotamento:} le concentrazioni in eccesso sono negative $\Rightarrow$ si ha una carenza di portatori liberi: $n,p<n_0,p_0$.
\end{itemize}

\subsection{Corrente di trascinamento \texorpdfstring{$J_{tr}$}{Jtr}}
La \textbf{corrente di trascinamento} $J_{tr}$ è determinata da un campo elettrico $\vec{\mathcal{E}}$ che esercita una forza $\vec F = \mp q \vec{\mathcal{E}}$ rispettivamente sugli elettroni liberi e sulle lacune. Poiché per convenzione la corrente è il flusso di carica positiva, uno spostamento di elettroni genera un flusso di corrente in direzione opposta. La media delle velocità istantanee dei singoli portatori liberi è detta \textbf{velocità media di trascinamento} $\vec v$:
\begin{itemize}
\item in assenza di campo elettrico, i portatori liberi sono in moto casuale con velocità media nulla;
\item in presenza di un campo elettrico esterno $\vec{\mathcal{E}}$, le velocità istantanee vengono turbate da esso e la velocità media $\vec v$ non è più nulla:
\[
\begin{cases} \displaystyle \vec{v_n} = - \mu_n \vec{\mathcal{E}} \\ \displaystyle \vec{v_p} = \mu_p \vec{\mathcal{E}} \end{cases}
\]
\end{itemize}

Nella curva che rappresenta, su scale logaritmiche, la velocità media in funzione del campo elettrico, si possono individuare due regioni particolari:
\begin{itemize}
\item regioni a \textbf{mobilità di basso campo} $\mu_0$: per campi elettrici sufficientemente piccoli esiste una proporzionalità diretta tra la velocità media e il campo elettrico, poiché il coefficiente di mobilità di basso campo $\mu_0$ è approssimativamente costante;
\item regioni a \textbf{mobilità differenziale negativa} $\mu_d$: per campi elettrici sufficientemente elevati la velocità media satura, cioè tende a un valore di asintoto orizzontale detto \textbf{velocità di saturazione} (tipicamente dell'ordine di $10^7 \; cm/s$), e a un aumento del campo elettrico corrisponde una diminuzione della velocità media.
\end{itemize}

Complessivamente il silicio SI ha delle prestazioni in frequenza peggiori di quelle dell'arseniuro di gallio GaAs e del germanio Ge, ma i dispositivi con il silicio sono di più facile realizzazione.

Supponiamo un volume di lunghezza infinitesima $ds$ e di sezione $A$ trasversale al flusso di corrente, riempito con un semiconduttore drogato di tipo $n$ in modo che la corrente di trascinamento ${J_{\text{tr}}}_p$ dei portatori minoritari (lacune) sia trascurabile. Applicando un campo elettrico $\vec{\mathcal{E}}$ uniforme, vale la \textbf{legge di Ohm microscopica} che lega la conducibilità elettrica $\sigma$ alla mobilità $\mu$:
\[
{J_{\text{tr}}}_n=\frac{I}{A}=\frac{1}{A} \cdot \frac{dQ}{ds} \frac{ds}{dt} = \frac{1}{A} \cdot \left( A \frac{dQ}{dV} \right) \left( v_n \right) = \left( - qn \right) \left( - \mu_n \mathcal{E} \right) \Rightarrow \begin{cases} \displaystyle {J_{\text{tr}}}_n = qn \mu_n \mathcal{E} \\ \displaystyle \sigma = \frac{J}{E} \end{cases} \Rightarrow \sigma = qn \mu_n
\]

Quindi, in presenza del solo drogaggio di tipo $n$ il flusso di corrente è costituito da elettroni liberi in concentrazione $n$ con mobilità $\mu_n$. In generale, gli effetti dei contributi di cariche libere e di lacune si sovrappongono:
\[
J_{\text{tr}} = {J_{\text{tr}}}_n + {J_{\text{tr}}}_p \Rightarrow \sigma = qn \mu_n + qp \mu_p
\]

\subsection{Corrente di diffusione \texorpdfstring{$J_{\text{diff}}$}{Jdiff}}
La \textbf{corrente di diffusione} $J_{\text{diff}}$ è determinata dal moto per diffusione di cariche da una zona a maggiore concentrazione a una zona a minore concentrazione, per raggiungere l'equilibrio termodinamico. La corrente di diffusione è il gradiente (= derivata prima) della concentrazione; man mano che le cariche si diffondono e le concentrazioni tendono a uguagliarsi la corrente di diffusione si riduce sempre di più, e alla fine la concentrazione diventa costante e la corrente di diffusione è nulla:\footnote{I segni dipendono dall'orientamento dell'asse $x$; in questo caso si suppone che sia più vicina all'origine la concentrazione maggiore.}
\[
\begin{cases} \displaystyle {J_{\text{diff}}}_n =qD_n \frac{\partial n}{\partial x} \\
\displaystyle {J_{\text{diff}}}_p =-qD_p \frac{\partial p}{\partial x} \end{cases}
\]
dove:
\begin{itemize}
\item $\tfrac{\partial n}{\partial x}$ e $\tfrac{\partial p}{\partial x}$ sono le variazioni nello spazio delle concentrazioni di portatori liberi;
\item $D_n$ e $D_p$ sono detti \textbf{coefficienti di diffusione} o \textbf{diffusività} (u.m. $cm^2/s$\footnote{Analisi dimensionale:
\[
\left[ J \right] = \frac{A}{{cm}^2} = C \cdot \left[ D \right] \cdot \frac{{cm}^{-3}}{cm}; \; \left[ D \right] = \frac{A}{C} \cdot \frac{ {cm}^4}{ {cm}^2} = \frac{C}{s \cdot C} \cdot {cm}^2 = \frac{ {cm}^2}{s}
\]}), e vicino all'equilibrio termodinamico soddisfano la \textbf{relazione di Einstein}:
\[
\begin{cases} \displaystyle D_n = V_T \mu_n \\ \displaystyle D_p = V_T \mu_p \end{cases}
\]
dove $V_T= \tfrac{\left( k_B T \right) }{q}$ è detto \textbf{equivalente elettrico della temperatura} (u.m. $V$) (a $T=300 \; K$: $V_T=26 \; mV$).
\end{itemize}

\subsection[Modello matematico per le variazioni di concentrazione di carica libera]{Modello matematico per le variazioni di concentrazione di carica libera\footnote{Vi sono dei flussi di \textbf{corrente di spostamento dielettrico} di densità $\vec J$:
\[
\begin{cases} \displaystyle \vec J = \frac{\partial \vec D}{\partial t} \\ \displaystyle \vec D = \epsilon \vec{\mathcal{E}} \end{cases} \Rightarrow \begin{cases} \displaystyle \vec J = \epsilon \frac{\partial \vec{\mathcal{E}}}{\partial t} \\ \displaystyle \vec{\mathcal{E}} = {\mathcal{E}}_0 \sin{\left( \omega t \right)} \end{cases} \Rightarrow \vec J = \epsilon {\mathcal E}_0 \omega \cos{\left( \omega t \right)} \Rightarrow \left| J \right| \leq \epsilon \left| {\mathcal{E}}_0 \right| \omega
\]
Quindi, in un semiconduttore il suo contributo, se la frequenza $\omega$ del campo tempo-variante non è troppo elevata, è trascurabile rispetto alle correnti di trascinamento e di diffusione. Invece, in un isolante (es. condensatore) non ci sono portatori liberi $\Rightarrow$ le correnti di trascinamento e di diffusione sono nulle $\Rightarrow$ il contributo della corrente di spostamento dielettrico è dominante.}}
I moti per diffusione e per trascinamento dei portatori liberi generano un flusso di corrente di densità $J$:
\[
J=J_n + J_p = J_{\text{tr}} + J_{\text{diff}}
\]

Nell'approssimazione a deriva-diffusione:
\[
\begin{cases} \displaystyle J_n = {J_{\text{tr}}}_n + {J_{\text{diff}}}_n = qn \mu_n \mathcal{E} + q D_n \frac{\partial n}{\partial x} \\ \displaystyle J_p = {J_{\text{tr}}}_p + {J_{\text{diff}}}_p = qp \mu_p \mathcal{E} - q D_p \frac{\partial p}{\partial x} \end{cases}
\]

I fenomeni di generazione e di ricombinazione non si bilanciano più tra loro $\Rightarrow$ non è più nullo il tasso netto di ricombinazione\footnote{Si veda la sezione~\ref{sez:modello_shockley}.} $U$, che è approssimabile al rapporto tra la concentrazione in eccesso e il \textbf{tempo di vita medio} $\tau$, cioè il tempo che una coppia di portatori trascorre prima di essere ricombinata:\footnote{Si distingue se si fa riferimento agli elettroni liberi ($U_n$) o alle lacune ($U_p$).}
\[
\begin{cases} \displaystyle U_n \approx \frac{n-n_0}{\tau_n} = \frac{n'}{\tau_n} \\ \displaystyle U_p \approx \frac{p-p_0}{\tau_p} = \frac{p'}{\tau_p} \end{cases}
\]

Le \textbf{equazioni di continuità}, basate sul principio di conservazione di particelle in un flusso attraverso una superficie, esprimono le variazioni nel tempo delle concentrazioni di carica libera dovute sia alle correnti di spostamento e di diffusione, sia ai fenomeni di generazione e di ricombinazione:
\[
\begin{cases} \displaystyle \frac{\partial n}{\partial t} = + \frac{1}{q} \frac{\partial J_n}{\partial x} - U_n \\ \displaystyle \frac{\partial p}{\partial t} = - \frac{1}{q} \frac{\partial J_p}{\partial x} - U_p \end{cases}
\]

Le incognite più significative sono le concentrazioni $n$ e $p$ e il campo elettrico $\vec{\mathcal{E}}$.

Alle equazioni di continuità si aggiunge l'\textbf{equazione di Poisson} per ricavare il campo elettrico incognito $\vec{\mathcal{E}}$:
\[
\frac{\partial \mathcal{E}}{\partial x} = \frac{\rho}{\epsilon} , \quad \begin{cases} \displaystyle \mathcal{E} = - \frac{\partial \varphi}{\partial x} \\ \displaystyle \rho = q \cdot \left( p + {N_D}^+ - n - {N_A}^- \right) \end{cases}
\]
dove:
\begin{itemize}
\item $\epsilon$ è la \textbf{costante dielettrica} del materiale;
\item $\rho$ è la \textbf{densità di carica netta positiva} all'interno del materiale, cioè la quantità di carica elettrica per unità di volume (u.m. $C/{cm}^3$).
\end{itemize}

\subsection{Regioni neutre con campo elettrico nullo}
Il \textbf{potenziale elettrostatico} $\varphi \left( x \right)$ rappresenta la condizione al contorno dell'equazione differenziale di Poisson e dipende dalle condizioni fisiche del semiconduttore, in particolare dalla differenza di potenziale $\Delta V$ applicata:
\[
\Delta V = \varphi \left( x_1 \right) - \varphi \left( x_2 \right)
\]

La \textbf{regione neutra} di un semiconduttore è una parte di esso in cui la densità di carica netta $\rho$ è nulla. Ai capi di una regione neutra compresa tra $x_1$ e $x_2$ si ha una differenza di potenziale $\Delta V$:
\[
\begin{cases} \displaystyle \frac{\partial \mathcal{E}}{\partial x} = \frac{\rho}{\epsilon} \\ \displaystyle \rho = 0 \end{cases} \Rightarrow \begin{cases} \displaystyle \mathcal{E} = {\mathcal{E}}_0 = \text{ cost.} \\ \displaystyle \frac{\partial \varphi}{\partial x} = - {\mathcal{E}}_0 \end{cases} \Rightarrow \begin{cases} \displaystyle \varphi \left( x \right) = - {\mathcal{E}}_0 x+C \\ \displaystyle \Delta V= \varphi \left( x_1 \right) - \varphi \left( x_2 \right) \end{cases} \Rightarrow \Delta V = - {\mathcal{E}}_0 \left( x_2 - x_1 \right)
\]

Se la differenza di potenziale $\Delta V$ ai capi di una regione neutra è nulla, il campo elettrico $\vec{\mathcal{E}}$ è anch'esso nullo e le equazioni di continuità si disaccoppiano in modo da non richiedere più l'equazione di Poisson:

\paragraph{Resistore}
Una regione drogata in modo uniforme è caratterizzata da una resistenza $R$ associata a una resistività $\rho$:\footnote{La resistività non va confusa con la densità di carica netta positiva.}
\[
R= \rho \frac{L}{A}
\]

Se tale regione resistiva è attraversata da una corrente $I$ nulla, la differenza di potenziale $\Delta V$ è nulla:
\[
I=0 \Rightarrow \Delta V=R \cdot I=0 \Rightarrow \mathcal{E} =0
\]

\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.5\linewidth]{pic/07/Charge_density_and_electric_field_of_a_capacitor.png}
\end{figure}

\paragraph{Condensatore}
Due cariche $q$ e $-q$ uguali ed opposte, separate da una regione dielettrica $x_1<x<x_2$ a densità di carica netta $\rho$ nulla, sono concentrate su due superfici di spessori infinitesimi (rispettivamente $\delta \left( x-x_1 \right)$ e $\delta \left( x-x_2 \right)$) $\Rightarrow$ integrando le delta di Dirac, nella regione dielettrica il campo elettrico è una costante ${\mathcal{E}}_0$ che dipende da $q$ $\Rightarrow$ la differenza di potenziale $\Delta V$ è nulla solo a condensatore scarico ($q=0$).

\subsection{Livelli di iniezione}
\label{sez:Livelli_iniezione}
In un semiconduttore drogato in \textbf{condizione di quasi neutralità} ($n' \approx p'$):
\begin{itemize}
\item se il \textbf{livello di iniezione} non è troppo alto, solo la concentrazione dei portatori minoritari risente della variazione di equilibrio, perché la concentrazione in eccesso è a un ordine di grandezza trascurabile rispetto a quello della concentrazione dei portatori maggioritari (ad es. nel caso prevalga di drogaggio di tipo $n$: $n' \ll n_0 \approx n$);
\item ad un alto livello di iniezione, le concentrazioni $n$ e $p$ sono confrontabili tra loro e molto distanti dall'equilibrio.
\end{itemize}

In basso livello di iniezione, le componenti di trascinamento sono dominate dai portatori maggioritari; in alto livello di iniezione, le componenti di diffusione sono dello stesso ordine di grandezza.

Si ha un campione di silicio drogato uniformemente di tipo $p$, in condizione di quasi neutralità ($\Rightarrow$ il campo elettrico $\vec{\mathcal{E}}$ è trascurabile). Se si compie all'estremità $x=0$ un'iniezione di basso livello di portatori minoritari (in questo caso $n$), questi ultimi assumeranno la seguente distribuzione spaziale:
\[
n_p' \left( x \right) = n_p' \left( 0 \right) \frac{ \sinh{\left( \frac{L-x}{L_n} \right)}}{\sinh{\left( \frac{L}{L_n} \right)}} , \quad 0 \leq x \leq L
\]

\paragraph{Casi limite}
A seconda della lunghezza $L$ del campione\footnote{All'estremità $x=L$ si suppone che valga la condizione di \textbf{contatto ohmico}: $n_p' \left( L \right) =0$.} rispetto alla \textbf{lunghezza di diffusione} dei portatori minoritari $L_n= \sqrt{D_n \tau_n}$:
\begin{itemize}
\item \textbf{campione corto} ($L \ll L_n$): la distribuzione è \textbf{lineare}:
\[
n_p' \left( x \right) \approx n_p' \left( 0 \right) \frac{L-x}{L_n}
\]
\item \textbf{campione lungo} ($L \gg L_n$): la distribuzione è \textbf{esponenziale}:
\[
n_p' \left( x \right) \approx n_p' \left( 0 \right) e^{- \frac{x}{L_n}}
\]
\end{itemize}