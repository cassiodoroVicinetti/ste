\chapter{Circuiti logici combinatori}
I circuiti si suddividono in:
\begin{itemize}
\item \textbf{combinatori:} lo stato dell'uscita dipende dagli stati correnti degli ingressi;
\item \textbf{sequenziali:} lo stato dell'uscita, oltre che dagli stati correnti degli ingressi, dipende anche da stati precedenti nel tempo (di solito al colpo di clock precedente) $\Rightarrow$ richiedono degli elementi di memoria detti \textbf{flip-flop}.
\end{itemize}

\section{Porte a livello singolo}
Le porte logiche si suddividono in invertenti e non invertenti a seconda se il segnale viene rispettivamente negato o no:
\begin{multicols}{2}
\paragraph{Invertenti}
\begin{itemize}
\item inverter
\item NOR
\item NAND
\item EXNOR
\end{itemize}
\vfill
\columnbreak
\paragraph{Non invertenti}
\begin{itemize}
\item buffer\footnote{Uno stadio buffer si limita a rigenerare il segnale senza modificarlo dal punto di vista funzionale. Quando il segnale di uscita ha una potenza maggiore, il buffer è un amplificatore.}
\item OR
\item AND
\item EXOR
\end{itemize}
\vspace*{\fill}
\end{multicols}

\subsection{Porte NAND e NOR \textit{R}-switch}
\subsubsection{Porta NAND \textit{n}MOS}
È costituita da più transistori collegati in serie e da una resistenza di pull-up $R_{PU}$. L'uscita va a massa se tutti gli interruttori sono chiusi, ovvero se tutti i segnali di ingresso sono allo stato alto $H$.

\subsubsection{Porta NOR \textit{n}MOS}
È analoga alla porta NAND, ma i transistori \textit{n}MOS sono collegati in parallelo. L'uscita va a massa se almeno uno degli interruttori è chiuso, ovvero se almeno uno dei segnali di ingresso è allo stato $H$.

\subsubsection{Porte \textit{p}MOS}
\label{sez:porte_pmos}
Una porta \textit{p}MOS rappresenta la stessa funzione di una porta \textit{n}MOS se presenta delle caratteristiche opposte:
\begin{itemize}
\item chiuso se $I = L$
\item aperto se $I = H$
\item serie $\rightarrow$ parallelo
\item parallelo $\rightarrow$ serie
\item verso massa $\rightarrow$ verso alimentazione
\end{itemize}

\subsection{Porte NAND e NOR CMOS}
Le tipologie \textit{n}MOS e \textit{p}MOS si complementano nelle porte CMOS, che non richiedono più resistenze di pull-up o di pull-down $\Rightarrow$ anche se rispetto alle porte \textit{R}-switch si ha un numero doppio di transistori, le dimensioni fisiche risultano sempre più ridotte.

\subsubsection{Porta NAND}
È l'unione di una porta NAND \textit{n}MOS come elemento di pull-down e di una porta NAND \textit{p}MOS come elemento di pull-up. Poiché in un CMOS gli elementi devono essere cortocircuitati uno alla volta, ogni segnale d'ingresso deve sdoppiarsi agli ingressi di una coppia di transistori di tipo opposto: ``bufferato'' al transistore \textit{n}MOS e negato al transistore \textit{p}MOS.

\subsubsection{Porta NOR}
Entrambi gli elementi di pull-down e pull-up invertono il tipo di collegamento dei transistori.

\section{Porte a livelli multipli}
Qualsiasi funzione logica può essere ricondotta a una somma di prodotti o a un prodotto di somme $\Rightarrow$ bastano sempre al massimo due livelli di porte logiche, cioè con due porte poste in serie.\footnote{Già le porte NAND e NOR CMOS in realtà erano a due livelli, perché la negazione degli ingressi comporta la presenza di un invertitore in serie.}

\subsection{Porte AND e OR in cascata}
\subsubsection{Porte \textit{R}-switch}
Avendo una resistenza di pull-up $R_{PU}$, si possono mettere in cascata più transistori nell'elemento di pull-down:
\begin{itemize}
\item la funzione OR si realizza con un collegamento in parallelo;
\item la funzione AND si realizza con un collegamento in serie.
\end{itemize}

L'elemento di pull-down è sempre invertente $\Rightarrow$ data una funzione logica, è necessario ricavare attraverso le leggi di De Morgan la corrispondente funzione negata.

Le prestazioni si riducono fortemente all'aumentare del numero di transistori inseriti, perché si amplificano gli effetti di non idealità.

\subsubsection{Porte CMOS}
L'elemento di pull-up complementare si realizza invertendo le caratteristiche di quello di pull-down nel modo consueto.\footnote{Si veda la sezione~\ref{sez:porte_pmos}.}

\section{Pass gate}
Il \textbf{pass gate} è un interruttore in serie al segnale, realizzato con una coppia di transistori \textit{p}MOS e \textit{n}MOS in parallelo. Il segnale di controllo $S$ apre un transistore chiudendo l'altro e viceversa, e l'uscita assume uno dei due valori di ingresso $A$ o $B$:
\[
U=A \cdot \bar S +B \cdot S
\]

Se il secondo segnale è il negato del primo ($B= \bar A$), si realizza una porta XOR.

La resistenza di perdita $R_{\text{on}}$ del transistore in conduzione e la capacità equivalente $C_P$ associate al pass gate aumentano la costante di tempo $\Rightarrow$ inserire più pass gate in serie porta a un eccessivo ritardo di propagazione.

Rispetto a un collegamento di porte logiche, il pass gate è vantaggioso perché richiede internamente meno transistori. A differenza delle porte logiche, però non rigenera il segnale perché si limita a restituire direttamente uno dei segnali di ingresso $\Rightarrow$ i disturbi non vengono filtrati.

\section{Consumo}
Ogni modulo consuma energia:
\begin{itemize}
\item una parte è usata per il funzionamento interno del modulo;
\item una parte viene usata per i segnali esterni;
\item una parte viene dissipata in calore.
\end{itemize}

L'energia viene fornita al modulo attraverso la tensione di alimentazione $V_{AL}$: l'indicatore del consumo è la corrente assorbita dall'alimentazione.

Conviene evitare un consumo di potenza eccessivo:
\begin{itemize}
\item forti correnti comportano dei disturbi elettromagnetici;
\item per portare una corrente elevata con una bassa dissipazione di potenza, il tratto di filo conduttore in cui scorre deve avere una resistenza equivalente molto bassa $\Rightarrow$ la resistenza equivalente è inversamente proporzionale alla sezione di conduttore;
\item per i dispositivi portatili è importante l'autonomia delle batterie;
\item un alto consumo di potenza comporta un'elevata quantità di calore disperso, specialmente se i componenti elettronici sono di piccole dimensioni.
\end{itemize}

\subsection{Potenza statica \texorpdfstring{$P_S$}{PS}}
La \textbf{potenza statica} $P_S$ è la potenza assorbita in assenza di commutazione, cioè quando la corrente $I_{DC}$ è costante nel tempo:
\[
P=V I_{DC}
\]

Varia con la temperatura e con la tensione di alimentazione $V_{AL}$.

Dipende dalla tecnologia del dispositivo e dai carichi resistivi.

La potenza statica di un dispositivo \textit{R}-switch è dovuta principalmente alla resistenza di perdita $R_{\text{on}}$ del transistore che dissipa potenza durante l'intervallo di tempo in cui l'uscita è portata a massa ($I = H$, $U = L$), definito in funzione del tempo totale (di solito l'intervallo tra due colpi di clock) attraverso una costante $D$ detta \textbf{duty cycle}:
\[
P_S = D_I \frac{ {V_{AL}}^2 }{R_{PU}} , \quad D_I = \frac{T_H}{T}
\]

La potenza statica di un dispositivo CMOS, a differenza di quella della tecnologia \textit{R}-switch, è approssimativamente nulla perché, in assenza di carico resistivo,\footnote{Vale anche per un carico puramente capacitivo.} in entrambi gli stati logici vi è un transistore in interdizione.

\subsection{Potenza dinamica \texorpdfstring{$P_D$}{PD}}
La \textbf{potenza dinamica} $P_D$ è la potenza assorbita per eseguire una commutazione. Con un carico capacitivo, la transizione $L \Rightarrow H$ dal livello basso al livello alto richiede che il condensatore venga caricato con una corrente proveniente dall'alimentazione, e viceversa con una corrente che va verso massa.

Dipende dalla tecnologia del dispositivo e soprattutto dal carico capacitivo. Di solito è molto maggiore della potenza statica $P_S$.

Se il condensatore $C$ a cui è applicata una differenza di potenziale $V$ viene caricato e scaricato $F$ volte al secondo, scorre una corrente pari a $F$ volte la quantità di carica $Q$ (s)caricata ogni volta:
\[
I= F \cdot Q = F \cdot CV
\]
con una potenza dinamica media:
\[
P=VI=FC V^2
\]

La potenza dinamica $P_D$ è per definizione proporzionale alla frequenza di clock $\Rightarrow$ il consumo è proporzionale alla frequenza.

Come ridurre la potenza dinamica $P_D$?
\begin{itemize}
\item Non conviene ridurre la frequenza di commutazione $F$ perché altrimenti per portare a termine un'operazione sarebbero richiesti più colpi di clock $\Rightarrow$ l'energia totale consumata sarebbe sempre la stessa, oltre al fatto che le prestazioni del circuito sarebbero peggiori.
\item Ridurre la capacità equivalente $C$, che essendo nel transistore MOS proporzionale a $\tfrac{W}{L}$ comporta una riduzione delle dimensioni.
\item Non conviene ridurre troppo la tensione di alimentazione $V_{AL}$ perché comporterebbe una riduzione eccessiva dei margini di rumore.
\end{itemize}

\subsection{Prodotto potenza \texorpdfstring{$P_D$}{PD} \texorpdfstring{$\times$}{x} ritardo \texorpdfstring{$t_P$}{tP}}
Un circuito logico ideale è caratterizzato da potenza dissipata nulla ($P_D=0$) e ritardo nullo ($t_P=0$). In un dispositivo reale si cerca un compromesso tra:
\begin{itemize}
\item correnti elevate: alta velocità e forte dissipazione;
\item correnti deboli: bassa velocità e bassa dissipazione.
\end{itemize}

Si può dimostrare che il prodotto $P_D \times t_P$ di una porta, una volta fissata la sua tecnologia, dipende solo dalla tensione di alimentazione $V_{AL}$ e dalla capacità equivalente $C$ del carico $\Rightarrow$ definisce una iperbole nel piano $\left( P_d,t_P \right)$.