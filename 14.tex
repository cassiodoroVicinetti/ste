\chapter{Circuiti logici: caratteristiche elettriche e interfacciamento}
\section{Invertitore CMOS con carico resistivo \texorpdfstring{$R_L$}{RL}}
\begin{figure}[H]
\centering
\begin{subfigure}[b]{.48\textwidth}
  \centering
  \includegraphics[width=.66\linewidth]{pic/14/H-state_CMOS_inverter_with_resistive_load.png}
  \caption{Circuito equivalente di un invertitore CMOS allo stato alto $H$ collegato a un carico resistivo $R_L$ verso massa.}
\end{subfigure}
\ \
\begin{subfigure}[b]{.48\textwidth}
  \centering
  \includegraphics[width=.66\linewidth]{pic/14/L-state_CMOS_inverter_with_resistive_load.png}
  \caption{Circuito equivalente di un invertitore CMOS allo stato basso $L$ collegato a un carico resistivo $R_C$ verso l'alimentazione.}
\end{subfigure}
\end{figure}

\subsection{Uscita a stato \texorpdfstring{$H$}{H}, carico verso massa}
\begin{figure}
	\centering
	\includegraphics[width=0.35\linewidth]{pic/14/Characteristic_of_a_H-state_CMOS_inverter.png}
	\caption{Caratteristica di un invertitore CMOS allo stato alto $H$ collegato a un carico resistivo $R_L$ verso massa.}
\end{figure}

\noindent
Il circuito impone una retta di carico sulla resistenza $R_L$ di carico:\footnote{È chiamata resistenza $R_{OH}$ la resistenza di perdita ${R_{\text{on}}}_p$ del transistore \textit{p}MOS quando l'uscita dell'inverter è allo stato alto $H$, e viceversa per la resistenza $R_{OL}$. L'altro effetto di non idealità, la corrente di perdita $I_{\text{off}}$, verrà sempre trascurato.}
\[
\begin{cases} \displaystyle V_O = V_{AL} + R_{OH} I_O \\
\displaystyle V_O = - R_L I_O \end{cases} \Rightarrow I_O = - \frac{V_{AL}}{R_{OH} + R_L}
\]

La resistenza di carico $R_L$ non deve essere troppo piccola affinché il punto di funzionamento del circuito non esca dal valore limite $V_{OH}$:
\[
\begin{cases} \displaystyle U=H \Rightarrow V_O > V_{OH} \\ \displaystyle I_O \left( V_O \right) = \frac{V_O - V_{AL}}{R_{OH}}  \end{cases} \Rightarrow \begin{cases} \displaystyle I_O \left( V_O \right) > I_O \left( V_{OH} \right) \equiv I_{OH} \\
\displaystyle I_O \left( V_O \right) = - \frac{V_{AL}}{R_{OH} + R_L \left( V_O \right) } \end{cases} \Rightarrow R_L \left( V_O \right) > R_L \left( V_{OH} \right)
\]
\FloatBarrier

\subsection{Uscita a stato \texorpdfstring{$L$}{L}, carico verso alimentazione}
\begin{figure}
	\centering
	\includegraphics[width=0.3\linewidth]{pic/14/Characteristic_of_a_L-state_CMOS_inverter.png}
	\caption{Caratteristica di un invertitore CMOS allo stato basso $L$ collegato a un carico resistivo $R_C$ verso l'alimentazione.}
\end{figure}

\noindent
Il circuito impone una retta di carico sulla resistenza $R_{OL}$ di perdita dell'inverter:
\[
\begin{cases} \displaystyle V_O = V_{AL} - R_C I_O \\
\displaystyle V_O = R_{OL} I_O \end{cases} \Rightarrow I_O = \frac{V_{AL}}{R_{OL} + R_C}
\]

La resistenza di carico $R_C$ non deve essere troppo piccola affinché il punto di funzionamento del circuito non esca dal valore limite $V_{OL}$:
\[
\begin{cases} \displaystyle U=L \Rightarrow V_O < V_{OL} \\
\displaystyle I_O \left( V_O \right) = \frac{V_O - V_{AL}}{R_C} \end{cases} \Rightarrow \begin{cases} \displaystyle I_O \left( V_O \right) < I_O \left( V_{OL} \right) \equiv I_{OL} \\
\displaystyle I_O \left( V_O \right) = \frac{V_{AL}}{R_{OL} + R_C \left( V_O \right)} \end{cases} \Rightarrow R_C \left( V_O \right) > R_C \left( V_{OL} \right)
\]
\FloatBarrier

\section{Invertitori con carico capacitivo}
La presenza di una capacità nel carico introduce degli effetti capacitivi di ritardo: le commutazioni non sono istantanee.

Nella realtà sono presenti anche degli effetti induttivi che introducono delle piccole oscillazioni nel segnale.

\subsection{Ritardi di transizione}
Si definisce \textbf{tempo di transizione} l'intervallo di tempo impiegato dal segnale per variare la sua ampiezza tra il 10\% e il 90\%. Si distinguono il \textbf{tempo di salita} $t_r$ e il \textbf{tempo di discesa} $t_f$:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{pic/14/Ideal_transition_times.png}
\end{figure}

Il tempo di transizione $\Delta t$, sia nel fronte di salita sia nel fronte di discesa, è direttamente proporzionale alla costante di tempo $\tau$:
\[
\Delta t = \tau \ln{\frac{0,1 \left( V_H - V_L \right) - V_H}{0,9 \left( V_H - V_L \right) - V_H}} = 2,2 \tau
\]

\subsubsection{Transizione \texorpdfstring{$L \rightarrow H$}{L H}}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.38\linewidth]{pic/14/Circuito_transizione_LH_invertitore_CMOS_con_carico_capacitivo.png}
\end{figure}

Il condensatore passa da circuito aperto a cortocircuito $\Rightarrow$ la tensione di uscita $V_B \left( t \right)$ ha un andamento esponenziale crescente che parte dalla tensione $V_{OL}$ e tende alla tensione $V_{OH}$ con costante di decadimento $\tau_{HL} =C_I R_{OH} || R_I \simeq C_I R_{OH}$:\footnote{La resistenza di carico $R_I$ non deve essere troppo piccola perché, quando ad esempio il condensatore è un circuito aperto, la tensione di alimentazione $V_A$ si ripartirebbe in una tensione di uscita $V_B$ troppo piccola $\Rightarrow$ la qualità del segnale viene degradata.}
\[
V_B \left( t \right) = V_{OH} + \left( V_{OL} - V_{OH} \right) e^{- \frac{t}{\tau_{LH}}}
\]

\begin{figure}[H]
	\centering
	\includegraphics[width=0.22\linewidth]{pic/14/Graphs_LH_transition_CMOS_inverter_with_capacitive_load.png}
\end{figure}

\subsubsection{Transizione \texorpdfstring{$H \rightarrow L$}{H L}}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.38\linewidth]{pic/14/Circuito_transizione_HL_invertitore_CMOS_con_carico_capacitivo.png}
\end{figure}

Il condensatore passa da cortocircuito a circuito aperto $\Rightarrow$ la tensione di uscita $V_B \left(  t \right)$ ha un andamento esponenziale decrescente che parte dalla tensione $V_{OH}$ e tende alla tensione $V_{OL}$ con costante di decadimento $\tau_{LH} = C_I R_{OL} || R_I \simeq C_I R_{OL}$:\footnotemark[\value{footnote}]
\[
V_B \left( t \right) = V_{OL} + \left( V_{OH} - V_{OL} \right) e^{- \frac{t}{\tau_{HL}}}
\]

\begin{figure}[H]
	\centering
	\includegraphics[width=0.22\linewidth]{pic/14/Graphs_HL_transition_CMOS_inverter_with_capacitive_load.png}
\end{figure}

\subsubsection{Invertitore \textit{n}MOS}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/14/NMOS_transition_times.png}
\end{figure}

\noindent
La resistenza equivalente di uscita vale $R_{PU}$ nello stato $H$ e $R_{OL} || R_{PU} \simeq R_{OL}$ nello stato $L$ $\Rightarrow$ è molto più piccola quando l'interruttore è chiuso $\Rightarrow$ la costante di tempo $\tau_{LH}$ del fronte di salita risulta molto più piccola $\Rightarrow$ il tempo di transizione $L \rightarrow H$ è maggiore del tempo di transizione $H \rightarrow L$.
\FloatBarrier

\subsubsection{Invertitore CMOS}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/14/CMOS_transition_times.png}
\end{figure}

\noindent
L'elemento di pull-up non è più passivo ma attivo: cambia il suo valore di resistenza equivalente in funzione dell'ingresso esattamente come fa l'elemento di pull-down $\Rightarrow$ il comportamento dinamico è simmetrico e i tempi di transizione sono entrambi piccoli.

Si possono minimizzare i ritardi riducendo la costante di tempo, in particolare:
\begin{itemize}
\item la resistenza equivalente $R_O$ vista ai morsetti del condensatore $\Rightarrow$ la corrente $I_O$ che scorre all'uscita diventa elevata;
\item la capacità equivalente $C_I$ $\Rightarrow$ il dispositivo deve essere piccolo (ad esempio, nel transistore MOS la capacità $C_{ox}$, cioè la capacità equivalente per unità di superficie,\footnote{Nel sistema MOS, la superficie è perpendicolare alla lunghezza del canale $L$.} si estende all'intero volume moltiplicandola per l'area $A=W \cdot L$) $\Rightarrow$ conferma la legge di Moore.
\end{itemize}
\FloatBarrier

\subsection{Ritardi di propagazione}
Una variazione all'ingresso viene propagata all'uscita con un certo ritardo: si definisce \textbf{tempo di propagazione} della porta l'intervallo di tempo tra l'istante in cui il segnale d'ingresso ha il 50\% di ampiezza e l'istante in cui il segnale d'uscita ha il 50\% di ampiezza:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.27\linewidth]{pic/14/Propagation_times.png}
	\caption{Tempo di propagazione di un invertitore.}
\end{figure}

%\noindent\rule[0.5ex]{\linewidth}{0.75pt}
\hrule

\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\linewidth]{pic/14/Esempio_di_circuiti_a_piu_ingressi.png}
\end{figure}

La costante di tempo, e quindi il tempo di transizione, dipende anche dalla parte capacitiva del carico: collegare l'invertitore a un circuito digitale con un numero di ingressi, detto \textbf{fan out}, troppo grande aumenta la capacità equivalente di carico, rischiando che il tempo di transizione superi il tempo di propagazione e il segnale non abbia il tempo di commutare.\marginpar{da chiarire}

\section{Collegamento a bus}
In un collegamento a bus non è noto a priori il numero di dispositivi logici connessi $\Rightarrow$ bisogna evitare le collisioni, cioè due dispositivi non devono comunicare sul bus in contemporanea.

\subsection{Uscita totem pole (TP)}
\begin{figure}
	\centering
	\includegraphics[width=0.25\linewidth]{pic/14/CMOS_inverter_with_TP_output.png}
	\caption{Uscita totem pole.}
\end{figure}

\noindent
Collegare tra loro le uscite di più invertitori CMOS può essere pericoloso: siccome i vari segnali di controllo sono indipendenti tra loro, un'errata combinazione di essi può far andare l'alimentazione in cortocircuito.
\FloatBarrier

\subsection{Uscita a tre stati (3S)}
\begin{figure}
	\centering
	\includegraphics[width=0.22\linewidth]{pic/14/Collegamento_CMOS_a_bus_con_uscite_3S.png}
	\caption{Collegamento a bus con uscite a tre stati.}
\end{figure}

\noindent
In un collegamento a bus con \textbf{uscite a tre stati}, ogni circuito ha un \textbf{segnale di enable}, e i segnali di enable vengono attivati uno alla volta da un modulo di controllo per evitare le collisioni:
\begin{itemize}
\item segnale di enable OE allo stato basso $L$: l'uscita del circuito è abilitata (come totem pole);
\item segnale di enable OE allo stato basso $H$: l'uscita del circuito viene disabilitata e assume un terzo stato Hi-$Z$ (ad alta impedenza).
\end{itemize}
\FloatBarrier

Il segnale di enable può essere rappresentato circuitalmente con un unico deviatore a 3 posizioni, di cui una corrisponde allo stato Hi-$Z$, oppure con un altro deviatore in serie all'uscita che abilita o disabilita l'uscita a seconda se chiuso o aperto:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\linewidth]{pic/14/CMOS_inverter_with_3S_output.png}
	\caption{Due rappresentazioni circuitali per l'uscita a tre stati.}
\end{figure}

La non idealità del circuito aperto interpretato nello stato Hi-$Z$ è rappresentabile con una corrente di perdita $I_{OZ}$.

L'uscita a tre stati è pericolosa se non si può garantire di poter attivare i segnali di enable solo uno alla volta.

\subsection{Uscita a collettore aperto (OC)}
\begin{figure}
	\centering
	\includegraphics[width=0.18\linewidth]{pic/14/Connection_CMOSes_to_bus_through_OC_outputs.png}
	\caption{Collegamento a bus con uscite a collettore aperto.}
\end{figure}

\noindent
L'\textbf{uscita a collettore aperto} (open drain) trova applicazione nella gestione delle richieste di interrupt, dove possono verificarsi più richieste alla volta.

Ogni stadio di uscita è realizzato con un solo interruttore \textit{n}MOS verso massa.

Tutti gli stadi di uscita sono in parallelo e condividono un'unica resistenza di pull-up $R_{PU}$:
\begin{itemize}
\item \textbf{wired or:} la linea va nello stato basso $L$ se anche una sola uscita è chiusa;
\item \textbf{wired and:} la linea va nello stato alto $H$ solo se tutte le uscite sono aperte.
\end{itemize}

Nel caso dell'inverter, l'interruttore è chiuso se l'ingresso è allo stato alto $H$ e viceversa:
\begin{itemize}
\item \textbf{operatore NOR} (uscita 0 quando almeno uno degli ingressi è 1): basta che uno solo degli interruttori sia chiuso perché la linea scenda allo stato basso $L$;
\item \textbf{operatore NAND} (uscita 0 quando tutti gli ingressi sono 1): tutti gli interruttori devono essere aperti perché la linea salga allo stato alto $H$.
\end{itemize}

Collegando $n$ carichi a $m$ uscite OC, è necessario scegliere una resistenza di pull-up $R_{PU}$ che garantisca la compabilità statica, cioè la corrente e la tensione non devono superare i valori limite riconosciuti dai carichi:
\[
\begin{cases} \displaystyle I_R = m I_O - n I_I : & I_O < I_{OL} \vee I_O > I_{OH} \\
\displaystyle V_O = V_{AL} - I_R R_{PU} : & V_O < V_{OL} \vee V_O > V_{OH} \end{cases} \Rightarrow I_R = \frac{V_{AL} - V_O}{R_{PU}} \begin{cases} \displaystyle > mI_{OH} + nI_{IH} \Rightarrow R_{PU} < R_{\text{max}} \\
\displaystyle < m I_{OL} + n I_{IL} \Rightarrow R_{PU} > R_{\text{min}} \end{cases}
\]

La scelta del valore di resistenza $R_{PU}$ è quindi un compromesso tra due caratteristiche del dispositivo:
\begin{itemize}
\item $R_{PU} = R_{\text{min}}$: massimizza la velocità perché è minore la resistenza equivalente e quindi la costante di tempo $\tau$;
\item $R_{PU} = R_{\text{max}}$: minimizza la potenza dissipata perché la corrente che scorre attraverso la resistenza $R_{PU}$ è minore.
\end{itemize}
\FloatBarrier

\section{Segnali differenziali digitali}
Anche i segnali digitali possono essere trasmessi in modo differenziale: lungo due fili, entrambi riferiti al terzo filo di massa, scorrono due segnali digitali uno invertito all'altro, e il segnale logico di informazione è dato dalla loro differenza.

\paragraph{Vantaggi}
\begin{itemize}
\item immunità ai disturbi dall'esterno;
\item minor consumo: i gradini dei singoli segnali hanno metà ampiezza del segnale differenziale allo stato alto $H$ $\Rightarrow$ è richiesta una tensione di alimentazione minore di quella richiesta da un singolo segnale di modo comune di ampiezza doppia.
\end{itemize}