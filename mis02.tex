\chapter{Incertezze di misura}
\section{Misurazione}
\subsection{Definizioni}
Eseguire una \textbf{misurazione} significa seguire un certo numero di passi per ottenere il risultato, detto \textbf{misura}, cioè l'informazione che va comunicata.

\subsection{Nomenclatura}
\begin{description}
\item[misurando] la grandezza da misurare, espressa con un modello matematico oppure descrittivo (es. lunghezza della scrivania)
\item[sistema misurato] il sistema fisico che viene misurato, che può avere misurandi (es. la scrivania ha lunghezza e grandezza)
\item[operatore] chi esegue la misurazione
\item[campione di riferimento] la misura è per via diretta/indiretta il confronto tra un campione scelto e la grandezza misurata (es. metro)
\item[grandezze di influenza] le grandezze ambientali che alterano in modo apprezzabile il risultato della misurazione (es. temperatura, umidità, disturbi di tipo elettrico)
\end{description}

\section{Definizioni}
L'\textbf{incertezza} è un'informazione che stima la bontà della misurazione. Sotto certe condizioni coincide con la fascia di valori. Lo sperimentatore ha anche il compito di stimare correttamente l'incertezza.

\subsection{Rappresentazioni}
\begin{itemize}
\item fascia di valori: $x= \left[ 9,98 \div 10,02 \right] \; V$
\item semiampiezza della fascia di valori:
\begin{itemize}
\item valore assoluto: $x=10,00 \; V \pm 0,02 \; V$, errore assoluto: $E_x=0,004 \; A$
\item valore relativo: $e_x=\tfrac{E_x}{x} \cdot x=0,13\%$
\end{itemize}
\end{itemize}

\subsection{Componenti di incertezza}
L'incertezza non è mai uguale a 0, perché avrebbe un costo infinito e ci vorrebbe un tempo infinito per eseguirla:
\begin{itemize}
\item il misurando ha una \textbf{incertezza intrinseca}, anche dovuta all'approssimazione del modello matematico che lo descrive, generalmente trascurabile;
\item tutti i campioni hanno un loro \textbf{incertezza}, che è dichiarata dal costruttore;
\item anche i \textbf{dispositivi di misura} hanno delle incertezze, scritte nel manuale;
\item lo \textbf{stato} è un insieme di grandezze che interagiscono nella misurazione e che definiscono le condizioni in cui si sta lavorando; lo stato non è perfettamente definito (servirebbero troppe grandezze) e varia al variare delle condizioni ambientali.
\end{itemize}

Alcuni scarti sono calcolabili con modelli teorici, per correggere la misura se la correzione risulta conveniente e non trascurabile nei confronti dell'incertezza. Alcune correzioni possono essere necessarie nell'utilizzo di alcuni strumenti apportanti ``errori'' di \textbf{consumo} (carico strumentale), ovvero quelli che quando messi in funzionamento variano lo stato del sistema rispetto a quando sono spenti.

Le grandezze di influenza sono tutte le grandezze di incertezza che sono dell'ordine di qualche percento. Esse si sommano secondo un certo modello.

\subsection{Cifre significative}
Si mettono solo le cifre significative che danno informazione. Siccome l'incertezza è stimata con qualche percento, servono non più di due cifre significative.

\section{Modello deterministico di stima dell'incertezza}
\subsection{Definizioni}
Il \textbf{modello deterministico} prevede una misurazione a lettura singola, quello \textbf{probabilistico} è a letture ripetute del valore di output dello strumento. La \textbf{funzione di taratura}, scritta nel manuale dello strumento, è un'informazione che, data una certa lettura, permette di eseguire una stima della grandezza (es. un grafico, un tabella, o anche un semplice coefficiente).

L'operatore prudente compie una stima pessimistica delle componenti di incertezza. Il valore centrale nella fascia di valori non è quello più probabile, ma tutti i valori sono ugualmente ragionevoli.

Ogni tanto lo strumento dev'essere ritarato.

\subsection{Incertezza delle misurazioni indirette}
Il risultato di una \textbf{misurazione indiretta} è ottenuta elaborando i risultati di una o più \textbf{misurazioni dirette}, ovvero effettuate con un campione (es. la velocità è calcolata con lunghezza e tempo).

Ciascuna delle misurazioni dirette ha una incertezza.

Se le grandezze dirette sono ragionevolmente indipendenti, l'incertezza assoluta massima della misurazione indiretta è data da:
\[
\delta x = \left| \frac{\partial f}{\partial x_{1o}} \right| \delta x_1 + \ldots + \left| \frac{\partial f}{\partial x_{mo}} \right| \delta x_m
\]
con $x_{io}$ valori centrali.

\subsubsection{Casi particolari}
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
\textbf{Somma} & \textbf{Prodotto} & \textbf{Potenza} \\
\hline
& & \\ [-1.5ex]
$\displaystyle x=a+b$ & $\displaystyle x = a \cdot b$ & $\displaystyle x=a^n$ \\
$\displaystyle \delta x= \delta a+ \delta b$ & $\displaystyle \frac{\delta x}{\left| x \right|} = \frac{\delta a}{\left| a \right|} + \frac{\delta b}{\left| b \right|}$ & $\displaystyle \frac{\delta x}{\left| x \right|} = n \frac{\delta a}{\left| a \right|}$ \\ [2ex]
\hline
\textbf{Differenza} & \textbf{Quoziente} & \textbf{Radice} \\
\hline
& & \\ [-1.5ex]
$\displaystyle x=a-b$ & $\displaystyle x = \frac{a}{b}$ & $\displaystyle x= \sqrt[n]{a}$ \\
$\displaystyle \delta x = \delta a  + \delta b$ & $\displaystyle \frac{\delta x}{\left| x \right|} = \frac{\delta a}{\left| a \right|} + \frac{\delta b}{\left| b \right|}$ & $\displaystyle \frac{\delta x}{\left| x \right|} = \frac{1}{n} \frac{\delta a}{\left| a \right|}$ \\ [2ex]
\hline
\end{tabular}
\end{table}

% \begin{table}[H]
% \centering
% \begin{tabular}{|c|c|c|}
% \hline
% \textbf{Somma} & \textbf{Prodotto} & \textbf{Potenza} \\
% \hline
% & & \\ [-1.5ex]
% \begin{tabular}[t]{@{}c@{}}$\displaystyle x=a+b$\\$\displaystyle \delta x= \delta a+ \delta b$\end{tabular} & \begin{tabular}[t]{@{}c@{}}$\displaystyle x = a \cdot b$\\$\displaystyle \frac{\delta x}{\left| x \right|} = \frac{\delta a}{\left| a \right|} + \frac{\delta b}{\left| b \right|}$\end{tabular} & \begin{tabular}[t]{@{}c@{}}$\displaystyle x=a^n$\\$\displaystyle \frac{\delta x}{\left| x \right|} = n \frac{\delta a}{\left| a \right|}$\end{tabular} \\ [2ex]
% \hline
% \textbf{Differenza} & \textbf{Quoziente} & \textbf{Radice} \\
% \hline
% & & \\ [-1.5ex]
% \begin{tabular}[t]{@{}c@{}}$\displaystyle x=a-b$\\$\displaystyle \delta x = \delta a  + \delta b$\end{tabular} & \begin{tabular}[t]{@{}c@{}}$\displaystyle x = \frac{a}{b}$\\$\displaystyle \frac{\delta x}{\left| x \right|} = \frac{\delta a}{\left| a \right|} + \frac{\delta b}{\left| b \right|}$\end{tabular} & \begin{tabular}[t]{@{}c@{}}$\displaystyle x= \sqrt[n]{a}$\\$\displaystyle \frac{\delta x}{\left| x \right|} = \frac{1}{n} \frac{\delta a}{\left| a \right|}$\end{tabular} \\ [2ex]
% \hline
% \end{tabular}
% \end{table}

\section{Compatibilità delle misure}
Non si può mai dire che due misure sono uguali. Si introduce la definizione di compatibilità: due misure sono compatibili quando le fasce di valori relative alla stessa quantità misurate in varie occasioni hanno intersezione non nulla.

Valgono le proprietà riflessiva ($a$ è compatibile con $a$) e simmetrica (se $a$ è compatibile con $b$, $b$ è compatibile con $a$), ma non quella transitiva (se $a$ è compatibile con $b$ e $b$ è compatibile con $c$, non necessariamente $a$ è compatibile con $c$). Sono \textbf{mutualmente compatibili} le misure che hanno almeno un intervallo in comune tra le fasce di valori.