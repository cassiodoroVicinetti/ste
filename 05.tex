\chapter{Circuiti con amplificatori operazionali ideali}
\section{Amplificatori reazionati con elementi reattivi}
L'analisi di un circuito amplificatore con elementi reattivi richiede di considerare le loro impedenze equivalenti.

Si può disegnare il diagramma di Bode dell'amplificazione (analisi in frequenza) o studiare la risposta al gradino (analisi nel tempo).

\section{Sommatori}
Il \textbf{sommatore} è uno stadio amplificatore che restituisce in uscita la combinazione lineare di più ingressi $V_1$, $V_2$\textellipsis:
\[
V_u=AV_1+BV_2+CV_3+ \cdots
\]
dove i coefficienti $A$, $B$\textellipsis\ sono negativi o positivi a seconda se l'ingresso è collegato al morsetto invertente o non invertente rispettivamente. L'amplificatore differenziale è un caso particolare con $A=-B$ e $C=D= \cdots =0$.

\subsection{Sommatore (invertente)}
\label{sez:Sommatore_invertente}
\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/05/Inverting_adder.png}
\end{figure}

\noindent
Ogni generatore di tensione $V_i$ fornisce la corrente $I_i= \tfrac{V_i}{R_i}$ per il principio di sovrapposizione degli effetti:\footnote{Il segnale di uscita si ottiene sommando le uscite parziali che si ottengono applicando uno solo dei generatori quando tutti gli altri sono spenti.}
\[
I_f=\frac{V_1}{R_1} + \frac{V_2}{R_2} + \cdots + \frac{V_n}{R_n} \Rightarrow V_u=-R_f I_f=- \frac{R_f}{R_1}  V_1- \frac{R_f}{R_2 } V_2- \cdots - \frac{R_f}{R_n}  V_n
\]
\FloatBarrier

\subsection{Amplificatore differenziale}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/05/Differential_amplifier.png}
\end{figure}

\noindent
Per il principio di sovrapposizione degli effetti:
\begin{itemize}
\item spegnendo il generatore $V_2$: il parallelo $R_2 || R_4$ non è attraversato da corrente $\Rightarrow$ amplificatore di tensione invertente:
\[
{V_u}_1 = V_1 \left( - \frac{R_3}{R_1} \right)
\]
\item spegnendo il generatore $V_1$, la tensione $V_2$ si ripartisce in $V_a$ su $R_4$, e si combina con la reazione all'altro morsetto dell'amplificatore di tensione non invertente:
\[
{V_u}_2 = V_a \left( 1 + \frac{R_3}{R_1} \right) = V_2 \frac{R_4}{R_2 + R_4} \left( 1+  \frac{R_3}{R_1} \right)
\]
\end{itemize}

Ponendo l'uguaglianza del rapporto delle resistenze:
\[
\begin{cases} \displaystyle V_u = {V_u}_1 + {V_u}_2 \\
\displaystyle \frac{R_3}{R_1} = \frac{R_4}{R_2} = A_d \end{cases} \Rightarrow V_u = V_1 \left( - A_d \right) + V_2 \frac{A_d}{\cancel{1 + A_d}} \left( \cancel{1 + A_d} \right) = A_d \left( - V_1 + V_2 \right) = A_d V_d
\]
\FloatBarrier

\subsection{Sommatore generalizzato}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/05/Generalized_adder.png}
\end{figure}

\noindent
Nel \textbf{sommatore generalizzato} le tensioni sono applicate a entrambi i terminali in ingresso:
\[
\begin{cases} \displaystyle {V_u}_- = - R_f I_f = - R_f \sideset{}{_{i-}}\sum{\frac{V_i}{R_i}} \\
\displaystyle {V_u}_+ = A_V \cdot \sideset{}{_{i+}}\sum{{V_e}_i} = \left( 1  + \frac{R_f}{R_{1-} || R_{2-} || \cdots} \right) \cdot \sideset{}{_{i+,j+}}\sum{V_i \frac{R_e || {R_{\text{eq}}}_{j \neq i}}{R_e || {R_{\text{eq}}}_{j \neq i} + R_i }} \end{cases} \Rightarrow V_u = {V_u}_- + {V_u}_+
\]

\begin{itemize}
\item \ul{lato invertente:} il contributo ${V_u}_-$ è analogo a quello del sommatore invertente,\footnote{Si veda la sezione~\ref{sez:Sommatore_invertente}} poiché il parallelo delle resistenze al morsetto non invertente, attraversate da corrente nulla, non influisce sulla tensione in uscita;
\item \ul{lato non invertente:} lasciando acceso solo il generatore $V_{i+}$, la sua tensione si ripartisce in ${V_e}_{i+}$ sul parallelo ${R_{\text{eq}}}_{j+ \neq i+}$ costituito dalle altre resistenze applicate al morsetto non invertente, quindi viene amplificata in ${V_u}_+$ secondo il modello dell'amplificatore non invertente.
\end{itemize}
\FloatBarrier

\section{Modo differenziale e modo comune}
\subsection{Segnale differenziale e di modo comune}
Le tensioni $V_1$ e $V_2$ possono essere espresse come ``scostamento'' $\tfrac{V_D}{2}$ dalla media $V_C$:
\[
\begin{cases} \displaystyle V_C = \frac{V_1 + V_2}{2} \\
\displaystyle V_D = V_2 - V_1 \end{cases} \Rightarrow \begin{cases} \displaystyle V_1 = V_C - \frac{V_D}{2} \\
\displaystyle V_2 = V_C + \frac{V_D}{2} \end{cases}
\]
dove:
\begin{itemize}
\item $V_C$ è il \textbf{segnale di modo comune}, cioè la differenza di potenziale tra la media tra $V_1$ e $V_2$ e la massa;
\item $V_D$ è il \textbf{segnale differenziale}, cioè la differenza di potenziale tra $V_1$ e $V_2$.
\end{itemize}

Un'informazione può essere trasferita in due modi:
\begin{itemize}
\item lungo un filo singolo, l'informazione utile dipende solamente dal modo comune: siccome è semplicemente riferito a massa, qualsiasi disturbo (es. fulmine) che si presenta lungo la linea risulta nella perturbazione del segnale di uscita;
\item lungo due fili posti a una specifica distanza, l'informazione utile è codificata solamente nel segnale differenziale: la trasmissione nel modo differenziale è meno soggetta a disturbi, soprattutto su lunghe distanze, grazie al fatto che il segnale differenziale non è riferito a massa, e quindi i disturbi perturbano in egual misura i modi comuni dei singoli segnali ma non la differenza di potenziale tra i due.
\end{itemize}

\subsection{Guadagno differenziale e di modo comune}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.55\linewidth]{pic/05/Common_and_differential_modes_in_an_amplifier.png}
\end{figure}

La tensione in uscita di un amplificatore si può esprimere in funzione dei modi comune e differenziale degli ingressi:
\[
\begin{cases} \displaystyle V_u = A V_1 + B V_2 \\
\displaystyle V_1 = V_C - \frac{V_D}{2} , \, V_2 = V_C + \frac{V_D}{2} \\
\displaystyle A_C = A + B , \, A_D = \frac{B - A}{2} \end{cases} \Rightarrow V_u = A_C V_C + A_D V_D
\]
dove $A_C$ è il \textbf{guadagno di modo comune} e $A_D$ è il \textbf{guadagno differenziale}.

Il \textbf{rapporto di reiezione di modo comune} (CMRR), definito come il rapporto tra il guadagno differenziale $A_D$ e il guadagno di modo comune $A_C$, misura la tendenza a rigettare i segnali d'ingresso di modo comune a favore di quelli differenziali.

\subsection{Amplificatore differenziale}
Un amplificatore differenziale ideale è caratterizzato da un guadagno di modo comune nullo:
\[
A_C = 0 \Rightarrow \frac{A_D}{A_C} \rightarrow + \infty
\]

L'obiettivo è massimizzare il guadagno $A_D$ e minimizzare il guadagno $A_C$, in modo che il primo sia trascurabile rispetto al secondo:
\[
A_C << A_D \Rightarrow \frac{A_D}{A_C} >> 1
\]

Esistono tuttavia delle cause di errore che allontanano l'amplificatore differenziale reale dall'idealità, derivanti dal fatto che i valori nominali di resistenza hanno una certa incertezza:
\begin{itemize}
\item è difficile scegliere una combinazione di resistenze che garantisca la condizione ideale dell'uguaglianza dei rapporti:
\[
\frac{R_3}{R_1} = \frac{R_4}{R_2}
\]
\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\linewidth]{pic/05/Differential_amplifier_with_non-ideal_generators.png}
\end{figure}

\item le resistenze interne ai generatori reali in ingresso, se non sono uguali tra loro, convertono una parte del segnale di modo comune (${V_S}_1 = {V_S}_2 = V_S \Rightarrow V_D = 0$) in segnale differenziale ($V_1 \neq V_2 \Rightarrow V_D \neq 0$), che viene così incorrettamente amplificato:
\[
\begin{cases} \displaystyle V_1 = V_S \frac{{R_i}_1}{{R_i}_1 + {R_g}_1} \\
\displaystyle V_2 = V_S \frac{{R_i}_2}{{R_i}_2 + {R_g}_2} \end{cases} \Rightarrow V_D = V_1 - V_2 =
\]
\[
=V_S \left( \frac{{R_i}_1}{{R_i}_1 + {R_g}_1} - \frac{{R_i}_2}{{R_i}_2 + {R_g}_2} \right) = 0 \Leftrightarrow \begin{cases} \displaystyle {R_g}_1 = {R_g}_2 \\
\displaystyle {R_i}_1 = {R_i}_2 \Rightarrow R_1 = R_2 + R_4 \end{cases}
\]
\end{itemize}