\chapter{La giunzione \textit{pn}}
Una \textbf{giunzione \textit{pn}} è una regione di semiconduttore perfettamente cristallino nella quale si abbia una parte drogata $p$ ed una drogata $n$. Si parla idealmente di \textbf{giunzione brusca} quando non c'è alcuna regione di transizione tra le due regioni e le concentrazioni di atomi droganti passano subito da $N_A$ a $N_D$.

\section{Condizioni di equilibrio termodinamico}
Supponiamo di riuscire idealmente a realizzare una giunzione \textit{pn} saldando assieme due semiconduttori, sebbene in realtà la tecnologia non consenta tecnicamente tale operazione. I due semiconduttori presi inizialmente isolati sono omogenei e localmente neutri $\Rightarrow$ trascurando il contributo dei portatori minoritari, le cariche dei portatori maggioritari si compensano con quelle degli atomi ionizzati.

Dopo la formazione della giunzione, siccome i portatori minoritari in ciascuna regione sono trascurabili, i portatori maggioritari si spostano per diffusione nel lato opposto, ma l'incontro tra una lacuna e un elettrone libero in corrispondenza della \textbf{giunzione metallurgica} provoca una ricombinazione dei due $\Rightarrow$ tra le regioni inizia a formarsi una \textbf{regione svuotata} (o di carica spaziale).

\begin{figure}
	\centering
	\includegraphics[width=0.33\linewidth]{pic/08/Graphs_for_a_pn_junction_on_depletion.png}
\end{figure}

La regione svuotata è priva di portatori liberi, e quindi la carica degli atomi ionizzati non è più bilanciata $\Rightarrow$ la regione svuotata ha una densità di carica $\rho$ non nulla (in particolare pari a $-qN_A$ nella metà verso il lato $p$, e $qN_D$ nella metà verso il lato $n$), e internamente a essa si crea un campo elettrico $\vec{\mathcal{E}}$ detto \textbf{di built-in} (o di contatto), associato a un potenziale $V_{\text{bi}}$ \ul{non} applicato dall'esterno detto \textbf{di built-in} (o di contatto). Siccome è negativo,\footnote{Si suppone che l'asse $x$ sia orientato dalla regione drogata di tipo $p$ a quella drogata $n$, con l'origine in corrispondenza della giunzione metallurgica.} il campo elettrico oppone al moto di diffusione di portatori liberi una forza di trascinamento sempre maggiore, agente sui pochissimi portatori minoritari rimasti nella regione svuotata, fino a raggiungere l'\textbf{equilibrio dettagliato}:
\[
\begin{cases} \displaystyle {J_{\text{diff}}}_n + {J_{\text{tr}}}_n =0 \\
\displaystyle {J_{\text{diff}}}_p + {J_{\text{tr}}}_p =0 \end{cases}
\]

Non c'è un confine netto tra la regione neutra e la regione svuotata; l'ampiezza della regione di transizione viene però considerata trascurabile nell'\textbf{approssimazione di completo svuotamento}: la regione svuotata è esattamente compresa tra $-x_p$ e $x_n$.
\FloatBarrier

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/08/Pn_junction_on_depletion.png}
\end{figure}

Applicando il teorema di Gauss:
\[
\oint_\Omega \rho dV = \oint_\Sigma \vec D \cdot \hat n d \sigma = \oint_\Sigma \epsilon \vec{\mathcal E} \cdot \hat n d \sigma
\]
a un cilindro $\Omega$ che racchiude la regione svuotata:
\begin{itemize}
\item le basi tagliano la giunzione in $-x_p$ e $x_n$, cioè nelle regioni neutre dove il campo elettrico $\vec{\mathcal{E}}$ è nullo;
\item il vettore normale $\hat n$ alla superficie cilindrica $\Sigma$ è ortogonale all'asse del cilindro, a sua volta parallelo all'asse $x$ e al campo elettrico $\vec{\mathcal E}$ $\Rightarrow$ il prodotto scalare $\vec{\mathcal E} \cdot \hat n =0$.
\end{itemize}
\FloatBarrier

La regione racchiusa dal cilindro è quindi in condizione di \textbf{neutralità globale}:
\[
\oint_\Omega \rho dV = 0 \Rightarrow \int_{- x_p}^{x_n} \rho \left( x \right) dx = 0
\]
e ciò impone delle condizioni ai grafici di $\rho \left( x \right)$ e $\mathcal E \left( x \right)$:
\[
\left( -q N_A \right) \left( -x_p \right) = \left( q N_D \right) x_A \Rightarrow \begin{cases} \displaystyle N_A x_p = N_D x_n \\ \displaystyle \mathcal E \left( 0^- \right) = \mathcal E \left( 0^+ \right) = - {\mathcal{E}}_{\text{max}} \end{cases}
\]

Se $N_D \ll N_A \Rightarrow x_p \to 0$, si può trascurare la parte di regione svuotata appartenente al campione drogato di tipo $p$ (e viceversa).

In equilibrio termodinamico, la regione svuotata presenta perciò un campo elettrico $\vec{\mathcal{E}} \left( x \right)$:
\[
\frac{d \mathcal E}{dx} = \frac{\rho}{\epsilon} \Rightarrow \mathcal{E} \left( x \right) = \begin{cases} \displaystyle - \frac{q N_A}{\epsilon} \left( x + x_p \right) & - x_p \leq x \leq 0 \\ \displaystyle + \frac{qN_D}{\epsilon} \left( x-x_n \right) & 0 \leq x \leq x_n \end{cases}
\]
che, per la condizione di neutralità globale, in corrispondenza della giunzione metallurgica equivale a:
\[
\mathcal{E} \left( 0^- \right) = \mathcal{E} \left( 0^+ \right) \Rightarrow {\mathcal{E}}_{\text{max}} = \frac{qN_A}{\epsilon} x_p = \frac{qN_D}{\epsilon} x_n
\]
da cui si ricava un potenziale elettrostatico $\varphi \left( x \right)$ a tratti parabolici:
\[
\frac{d \varphi}{dx} = - \mathcal{E} \Rightarrow \varphi \left( x \right) = \begin{cases} \displaystyle 0  & x \leq - x_p \\ \displaystyle + \frac{qN_A}{2 \epsilon} {\left( x+x_p \right)}^2 & -x_p \leq x \leq 0 \\ \displaystyle - \frac{qN_D}{2 \epsilon} {\left( x-x_n \right)}^2 + \frac{qN_A}{2 \epsilon} {x_p}^2 + \frac{qN_D}{2 \epsilon} {x_n}^2 & 0 \leq x \leq x_n \\ \displaystyle \varphi \left( x_n \right) - \varphi \left( x_p \right) = \frac{qN_A}{2 \epsilon} {x_p}^2 + \frac{qN_D}{2 \epsilon} {x_n}^2 & x \geq x_n \end{cases}
\]

\subsection{Diagramma a bande}
Il \textbf{diagramma a bande} di energia è una rappresentazione grafica dell'energia potenziale $U$ alla quale sono sottoposti gli elettroni. Nella regione svuotata, l'energia potenziale $U(x)$\footnote{Tutte le energie definite nelle bande di energia ($E_c$, $E_v$, $E_g$, $E_F$, ${E_F}_i$) seguono l'andamento della generica $U(x)$.} segue l'andamento inverso del potenziale elettrostatico $\varphi (x)$:
\[
U(x)=-q \varphi (x)
\]
determinando ai capi della regione svuotata una barriera di energia potenziale $qV_{\text{bi}}$, che si oppone al flusso di portatori, con potenziale di built-in $V_{\text{bi}}$ pari a:
\[
V_{\text{bi}} = \varphi \left( x_n \right) - \varphi \left( -x_p \right) =- \frac{U \left( x_n \right)}{q} + \frac{U \left( - x_p \right)}{q} = \frac{- {E_F}_i \left( x_n \right) + {E_F}_i \left( -x_p \right)}{q}
\]

Si può dimostrare che siccome il dispositivo non è attraversato da corrente, il livello di Fermi $E_F$ si trova a un livello di energia potenziale costante lungo $x$. Nelle regioni neutre, le equazioni di Shockley legano il livello di Fermi $E_F$ al livello di Fermi intrinseco ${E_F}_i (x)$:
\[
\begin{cases} \displaystyle n=N_D = n_i \cdot e^{\frac{E_F-{E_F}_i}{k_BT}} \\
\displaystyle p = N_A = n_i \cdot e^{\frac{{E_F}_i - E_F}{k_BT}} \end{cases} \Rightarrow \begin{cases} \displaystyle E_F - {E_F}_i = k_BT \ln{\frac{N_D}{n_i}} & n \geq x_n \\
\displaystyle {E_F}_i -E_F = k_B T \ln{\frac{N_A}{n_i}} & x \leq - x_p \end{cases}
\]

Sfruttando anche la condizione di neutralità globale $N_A x_p=N_D x_n$, si ricava che le ampiezze $x_n$ e $x_p$ sono proporzionali alla radice quadrata del potenziale di built-in $V_{\text{bi}} = \varphi \left( x_n \right) - \varphi \left( -x_p \right)$ che sta ai capi della regione svuotata:\footnote{Le espressioni di $x_n$, $x_p$ e $x_d$ ricavate qui varranno anche in assenza di equilibrio termodinamico con $\varphi \left( x_n \right) - \varphi \left( -x_p \right) = V_{\text{bi}} -V$.}
\[
\begin{cases} \displaystyle V_{\text{bi}} = \frac{- {E_F}_i \left( x_n \right) + {E_F}_i \left( -x_p \right)}{q} = \frac{k_B T}{q} \ln{\frac{N_AN_D}{{n_i}^2}} \\
\displaystyle V_{\text{bi}} = \varphi \left( x_n \right) - \varphi \left( - x_p \right) = \frac{qN_A}{2 \epsilon} {x_p}^2 + \frac{qN_D}{2 \epsilon} {x_n}^2 \\
\displaystyle N_A x_p = N_D x_n \end{cases} \Rightarrow \begin{cases} \displaystyle x_n = \sqrt{\frac{2 \epsilon}{q}} \cdot \frac{\sqrt{N_{\text{eq}}}}{N_D} \cdot \sqrt{\varphi \left( x_n \right) - \varphi \left( -x_p \right)} \\
\displaystyle x_p = \sqrt{\frac{2 \epsilon}{q}} \cdot \frac{\sqrt{N_{\text{eq}}}}{N_A} \cdot \sqrt{\varphi \left( x_n \right) - \varphi \left( -x_p \right)} \\
\displaystyle x_d = x_n + x_p = \sqrt{\frac{2 \epsilon}{q}} \cdot \frac{1}{\sqrt{N_{\text{eq}}}} \cdot \sqrt{\varphi \left( x_n \right) - \varphi \left( -x_p \right)} \end{cases}
\]
dove $N_{\text{eq}}$ è il ``parallelo'' tra le concentrazioni $N_A$ e $N_D$:
\[
N_{\text{eq}} =N_A ||N_D= \frac{N_A \cdot N_D}{N_A+N_D}
\]

\subsection{Reinterpretazione del diagramma a bande}
Per il diagramma a bande viene preso come riferimento assoluto il \textbf{livello del vuoto} $E_0$, che è la minima energia che deve assumere un elettrone per liberarsi e uscire all'esterno del cristallo. Si definiscono i seguenti salti di energia:
\begin{itemize}
\item \textbf{affinità elettronica} $q \chi_S$: il salto di energia rispetto ad $E_c$:
\[
q \chi_S=E_0-E_c
\]
\item \textbf{lavoro di estrazione} $q \phi_S$: il salto di energia rispetto al livello di Fermi $E_F$:
\[
q \phi_S=E_0-E_F=q \chi_S+E_c-E_F
\]
\end{itemize}

Il lavoro di estrazione $q \phi_S$ dipende dal drogaggio; l'affinità elettronica è propria del materiale (ad es. per il silicio vale: $q \chi_S=4,05 \; eV$).

I due campioni presi isolati presentano un diverso livello di Fermi $E_F$: il campione drogato di tipo $p$ ha un livello di Fermi inferiore a quello intrinseco ${E_F}_i$, e l'altro di tipo $n$ lo ha superiore.

Formata la giunzione, l'equilibrio impone che il livello di Fermi $E_F$ sia costante $\Rightarrow$ il salto tra i due livelli di Fermi dei campioni isolati deve essere annullato da uno sfalsamento pari a $qV_{\text{bi}}$ del livello del vuoto $E_0$ $\Rightarrow$ siccome anche l'affinità elettronica $q \chi_S$ dev'essere costante, tutti gli altri livelli di energia subiscono pari sfalsamento. Nella regione svuotata, la variazione del livello di Fermi $E_F$ rispetto a quello intrinseco ${E_F}_i$ determina il flusso di portatori liberi.

Ricordando le equazioni di Boltzmann, i due campioni, aventi uguale affinità elettronica $q \chi_S$ perché dello stesso materiale, quando presi isolati hanno i seguenti lavori di estrazione:
\[
\begin{cases} \displaystyle q {\phi_S}_p = q \chi_S + E_g - \left( E_F - E_v \right) = q \chi_S + E_g - k_B T \ln{\frac{N_v}{N_A}} \\
\displaystyle q {\phi_S}_n = q \chi_S + \left( E_c - E_F \right) = q \chi_S + k_B T \ln{\frac{N_c}{N_D}} \end{cases}
\]

La barriera di energia potenziale $qV_{\text{bi}}$ all'equilibrio della giunzione coincide con la differenza dei lavori di estrazione:
\[
qV_{\text{bi}} = q {\phi_S}_p -q {\phi_S}_n \Rightarrow \begin{cases} \displaystyle q V_{\text{bi}} = E_g - k_B T \ln{\frac{N_v N_c}{N_A N_D}} \\
\displaystyle {n_i}^2 = N_c N_v e^{- \frac{E_g}{k_B T}} \end{cases} \Rightarrow
\]
\[
\Rightarrow q V_{\text{bi}} = k_B T \left( \ln{\frac{N_c N_v}{{n_i}^2}} - \ln{\frac{N_v N_c}{N_A N_D}} \right) = k_B T \ln{\frac{N_A N_D}{{n_i}^2}}
\]

\section{Assenza di equilibrio in regime stazionario nel tempo}
Applicando dall'esterno una tensione $V$ costante alla giunzione \textit{pn} si determina una corrente $I$.

Nella giunzione \textit{pn} si possono riconoscere 3 regioni, che sono in serie perché sono attraversate da una corrente $I$ costante che:
\begin{itemize}
\item entra dalla parte a potenziale più alto, senza venire dispersa perché si assume la condizione di \textbf{contatto ohmico};
\item attraversa la regione neutra tra $-w_p$ e $-x_p$, su cui vi è una caduta di potenziale $V_1$;
\item attraversa la regione svuotata tra $-x_p$ e $x_n$, su cui vi è una caduta di potenziale $V_2$;
\item attraversa la regione neutra tra $x_n$ e $w_n$, su cui vi è una caduta di potenziale $V_3$;
\item esce dalla parte a potenziale più basso, sempre in condizione di contatto ohmico, e per la legge di Kirchhoff il suo valore è pari al valore iniziale.
\end{itemize}

Le regioni neutre sono drogate in modo omogeneo $\Rightarrow$ le conducibilità $\sigma_n = qN_D \mu_n$ e $\sigma_p = qN_A \mu_p$ sono costanti $\Rightarrow$ le regioni neutre sono di tipo resistivo, e vengono chiamate \textbf{resistenze parassite} ${R_p}_n$ e ${R_p}_p$:
\[
R_p = \frac{1}{\sigma} \cdot \frac{w-x}{A}
\]

Siccome le regioni sono in serie, la somma delle cadute di potenziale $V_1$, $V_2$ e $V_3$ coincide con la tensione applicata $V$:
\[
\begin{cases} \displaystyle V=V_1+V_2+V_3 \\
\displaystyle R_p= {R_p}_n + {R_p}_p \end{cases} \Rightarrow V=R_pI+V_2
\]

Per semplicità, si trascurerà la corrente che scorre nelle resistenze parassite, in modo che queste ultime non intervengano nella giunzione \textit{pn}:
\[
I \to 0 \Rightarrow \begin{cases} \displaystyle V \simeq V_2 \\ \displaystyle \mathcal{E} \to 0 \end{cases}
\]

\subsection{Polarizzazione inversa}
Applicando una tensione $V$\footnote{Si ricorda che la tensione $V$ del generatore si scarica tutta sulla tensione $V_2$ della regione svuotata perché si trascurano le regioni neutre.} negativa, essa si sovrappone nella regione svuotata alla tensione di built-in $V_{\text{bi}}$:
\[
\varphi \left( x_n \right) - \varphi \left( - x_p \right)  = V_{\text{bi}} -V=V_{\text{bi}} + \left| V \right| > V_{\text{bi}}
\]
allargando i confini $x_n$ e $x_p$ della regione stessa e aumentando il campo elettrico $\vec{\mathcal{E}}$ di trascinamento e la barriera di energia potenziale $q \left( V_{\text{bi}} -V \right)$ in opposizione al flusso per diffusione dei portatori maggioritari. Prevale quindi la forza di trascinamento che agisce sui pochissimi portatori minoritari rimasti nella regione di svuotamento $\Rightarrow$ si determina una corrente $I$ negativa (cioè positiva verso il lato $p$) che, poiché il numero di portatori minoritari spostati è molto ridotto, è di intensità molto piccola e indipendente dalla tensione V.

\subsection{Polarizzazione diretta}
Applicando una tensione $V$ positiva, essa si sovrappone nella regione svuotata alla tensione di built-in $V_{\text{bi}}$, restringendo i confini e riducendo la barriera di energia potenziale $\Rightarrow$ viene favorito il flusso per diffusione dei portatori maggioritari $\Rightarrow$ si determina una corrente $I$ positiva (verso il lato $n$) che, vista la grande disponibilità di portatori maggioritari, è fortemente crescente con la tensione $V$.

\subsection{Contributi di trascinamento \texorpdfstring{$J_{\text{tr}}$}{Jtr} dei portatori minoritari}
Siccome nelle regioni neutre si suppone che il campo elettrico $\vec{\mathcal{E}}$ sia trascurabile e che il livello di iniezione sia basso ($n_p \ll p_p$ al lato $p$ e $p_n \ll n_n$ al lato $n$), i contributi di trascinamento $J_{\text{tr}}$ dei portatori minoritari si possono ignorare:
\[
\begin{cases} \displaystyle x < -x_p : \; \mathcal{E} \left( x \right) \to 0 \wedge n_p \left( x \right) \ll p_p \left( x \right) \Rightarrow {J_{\text{tr}}}_n = qn_p \left( x \right) \mu_n \mathcal{E} (x) \to 0 \\
\displaystyle x>x_n : \; \mathcal{E} (x) \to 0 \wedge p_n (x) \ll n_n (x) \Rightarrow {J_{\text{tr}}}_p = qp_n (x) \mu_p \mathcal{E} (x) \to 0 \end{cases}
\]

\subsection{Contributi di diffusione \texorpdfstring{$J_{\text{diff}}$}{Jdiff} dei portatori minoritari}
\label{sez:diap27}
In equilibrio termodinamico:
\[
\begin{cases} \displaystyle \begin{cases} \displaystyle {p_p}_0 \left( -x_p \right) = N_A \\
\displaystyle {n_p}_0 \left( -x_p \right) = \frac{{n_i}^2}{N_A} \end{cases} \\
\displaystyle \begin{cases} \displaystyle {n_n}_0 \left( x_n \right) = N_D \\
\displaystyle {p_n}_0 \left( x_n \right) = \frac{{n_i}^2}{N_D} \end{cases} \\
\displaystyle V_{\text{bi}} = V_T \ln{\frac{N_AN_D}{{n_i}^2}} \end{cases} \Rightarrow \begin{cases} \displaystyle V_{\text{bi}} = V_T \ln{\frac{{p_p}_0 \left( - x_p \right)}{{p_n}_0 \left( x_n \right)}} \\
\displaystyle V_{\text{bi}} = V_T \ln{\frac{{n_n}_0 \left( x_n \right)}{{n_p}_0 \left( -x_p \right)}} \end{cases} \Rightarrow \begin{cases} \displaystyle {p_n}_0 \left( x_n \right) = {p_p}_0 \left( -x_p \right) e^{- \frac{V_{\text{bi}}}{V_T}} \\
\displaystyle {n_p}_0 \left( -x_p \right) = {n_n}_0 \left( x_n \right) e^{- \frac{V_{\text{bi}}}{V_T}} \end{cases}
\]

Supponendo un basso livello di iniezione:
\[
\begin{cases} \displaystyle n_n \left( x_n \right) \approx {n_n}_0 \left( x_n \right) \\
\displaystyle p_p \left( -x_p \right) \approx {p_p}_0 \left( - x_p \right) \end{cases}
\]
in assenza di equilibrio termodinamico vale la \textbf{legge della giunzione}:
\[
e^{- \frac{V_{\text{bi}} -V}{V_T}} = e^{- \frac{V_{\text{bi}}}{V_T}} \cdot e^{\frac{V}{V_T}} \Rightarrow \begin{cases} \displaystyle p_n \left( x_n \right) \approx \left( {p_p}_0 \left( -x_p \right) e^{- \frac{V_{\text{bi}}}{V_T}} \right) \cdot e^{\frac{V}{V_T}} = {p_n}_0 \left( x_n \right) e^{\frac{V}{V_T}} = \frac{{n_i}^2}{N_D} e^{\frac{V}{V_T}} \\
\displaystyle n_p \left( - x_p \right) \approx \left( {n_n}_0 \left( x_n \right) e^{- \frac{V_{\text{bi}}}{V_T}} \right) \cdot e^{\frac{V}{V_T}} = {n_p}_0 \left( -x_p \right) e^{\frac{V}{V_T}} = \frac{{n_i}^2}{N_A} e^{\frac{V}{V_T}} \end{cases} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle p_n' \left( x_n \right) = p_n \left( x_n \right) - {p_n}_0 \left( x_n \right) = \frac{{n_i}^2}{N_D} \left( e^{ \frac{V}{V_T}} - 1 \right) \\
\displaystyle n_p' \left( -x_p \right) = n_p \left( -x_p \right) - {n_p}_0 \left( -x_p \right) = \frac{{n_i}^2}{N_A} \left( e^{\frac{V}{V_T}} -1 \right) \end{cases}
\]

È possibile vedere ciascun lato della giunzione come un campione drogato uniformemente, in condizione di quasi neutralità, sottoposto a un'iniezione di basso livello di portatori minoritari all'estremità $x= \pm x_{n,p}$.\footnote{Si veda la sezione~\ref{sez:Livelli_iniezione}.} Supponendo che i due lati $w_p$ e $w_n$ siano lunghi rispetto alla lunghezza di diffusione dei portatori minoritari $L_{n,p} = \sqrt{D_{n,p}  \tau_{n,p}}$, la distribuzione è esponenziale:
\[
\begin{cases} \displaystyle p_n' (x) \approx p_n' \left( x_n \right) e^{- \frac{x-x_n}{L_p}} = \frac{{n_i}^2}{N_D} \left( e^{\frac{V}{V_T}} -1 \right) e^{- \frac{x-x_n}{L_p}} \\
\displaystyle n_p' \left( x \right) \approx n_p' \left( -x_p \right) e^{\frac{x- \left( -x_p \right)}{L_n}} = \frac{{n_i}^2}{N_A} \left( e^{\frac{V}{V_T}} -1 \right) e^{\frac{x- \left( -x_p \right)}{L_n}} \end{cases}
\]

Ai confini $-x_p$ e $x_n$ della giunzione metallurgica:
\begin{itemize}
\item polarizzazione inversa: si verifica uno svuotamento di portatori minoritari perché i loro eccessi sono negativi:
\[
V<0 \Rightarrow \begin{cases} \displaystyle p_n' \left( x_n \right) < 0 \\
\displaystyle n_p' \left( -x_p \right) < 0 \end{cases}
\]
\item polarizzazione diretta: si verifica un'iniezione di portatori minoritari perché i loro eccessi sono positivi:
\[
V > 0 \Rightarrow \begin{cases} \displaystyle p_n' \left( x_n \right) > 0 \\
\displaystyle n_p' \left( - x_p \right) > 0 \end{cases}
\]
\end{itemize}

I portatori minoritari danno perciò ai confini della giunzione i seguenti contributi di diffusione $J_{\text{diff}}$:
\[
\begin{cases} \displaystyle {J_{\text{diff}}}_p \left( x_n \right) = qD_p {\left. \frac{d p_n'}{dx} \right|}_{x=x_n} \\
\displaystyle {J_{\text{diff}}}_n \left( -x_p \right) = q D_n {\left. \frac{d n_p'}{dx} \right|}_{x=-x_p} = q \frac{D_n}{L_n} \cdot \frac{{n_i}^2}{N_A} \left( e^{\frac{V}{V_T}} -1 \right) \end{cases}
\]

\subsection{Contributi \texorpdfstring{$J_{\text{diff}} + J_{\text{tr}}$}{Jdiff + Jtr} dei portatori maggioritari}
\paragraph{Al di fuori della regione svuotata}
La densità di corrente totale $J_{\text{tot}}$ è costante poiché la corrente $I$ è costante lungo $x$ $\Rightarrow$ i contributi dei portatori maggioritari $J_p (x)$ e $J_n (x)$ si possono ricavare come i ``complementari'' dei contributi di diffusione ${J_{\text{diff}}}_p$ e ${J_{\text{diff}}}_n$ dei minoritari.

\paragraph{All'interno della regione svuotata}
Trascurando:
\begin{itemize}
\item le variazioni nel tempo delle concentrazioni di carica libera perché si suppone di lavorare in condizioni stazionarie:
\[
\begin{cases} \displaystyle \frac{\partial n}{\partial t} = 0 \\
\displaystyle \frac{\partial p}{\partial t} = 0 \end{cases}
\]
\item il contributo dei fenomeni di generazione e ricombinazione nella regione svuotata:
\[
\begin{cases} \displaystyle U_n = \frac{n-n_0}{\tau_n} = 0 \\
\displaystyle U_p = \frac{p-p_0}{\tau_p} = 0 \end{cases}
\]
\end{itemize}
le equazioni di continuità impongono che le densità di corrente $J_p$ e $J_n$ devono essere costanti:\footnote{Inoltre, anche all'interno della regione svuotata la densità di corrente totale è costante.}
\[
\begin{cases} \displaystyle \frac{\partial n}{\partial t} = + \frac{1}{q} \frac{\partial J_n}{\partial x} - U_n \\
\displaystyle \frac{\partial p}{\partial t} = - \frac{1}{q} \frac{\partial J_p}{\partial x} - U_p \end{cases} \Rightarrow \begin{cases} \displaystyle \frac{\partial J_n}{\partial x} = 0 \\
\displaystyle \frac{\partial J_p}{\partial x} = 0 \end{cases} \Rightarrow \begin{cases} J_n \left( x \right) = \text{cost.} \\
\displaystyle J_p (x) = \text{cost.} \end{cases}
\]

\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{pic/08/Contributi_portatori_maggioritari_giunzione_pn.png}
\end{figure}

Graficamente, si ottengono i contributi $J_p$ e $J_n$ dei portatori maggioritari ai confini $-x_p$ e $x_n$ della regione svuotata:
\[
\begin{cases} \displaystyle J_p \left( -x_p \right) = {J_{\text{diff}}}_p \left( x_n \right) = q \frac{D_p}{L_p} \cdot \frac{{n_i}^2}{N_D} \left( e^{\frac{V}{V_T}} -1 \right) \\
\displaystyle J_n \left( x_n \right) = {J_{\text{diff}}}_n \left( -x_p \right) = q \frac{D_n}{L_n} \cdot \frac{{n_i}^2}{N_A} \left( e^{\frac{V}{V_T}} -1 \right) \end{cases}
\]
\FloatBarrier

\subsection{Caratteristica statica \texorpdfstring{$I \left( V \right)$}{I(V)}}
All'interno della regione svuotata, la densità di corrente totale è costante:
\[
J_{\text{tot}} = J_n \left( x \right) + J_p (x) = {J_{\text{diff}}}_n (x) + {J_{\text{tr}}}_n (x) + {J_{\text{diff}}}_p (x) + {J_{\text{tr}}}_p (x) = q \frac{D_n}{L_n} \cdot \frac{{n_i}^2}{N_A} \left( e^{\frac{V}{V_T}} -1 \right) + q \frac{D_p}{L_p} \cdot \frac{{n_i}^2}{N_D} \left( e^{\frac{V}{V_T}} -1 \right)
\]
e la corrente $I$ che scorre è espressa in funzione della tensione $V$ nella \textbf{caratteristica statica}, definita come la relazione tra la tensione $V$, applicata dall'esterno, e la corrente $I$ in regime stazionario nel tempo, cioè con tensione a transitorio esaurito e con corrente stabilizzata a un valore costante:
\[
I \left( V \right) = J_{\text{tot}} \cdot A = \left[ qA {n_i}^2 \left( \frac{D_n}{L_n N_A} + \frac{D_p}{L_p N_D} \right) \right] \left( e^{\frac{V}{V_T}} -1 \right) = I_s \left( e^{\frac{V}{V_T}} -1 \right)
\]
dove $I_s$ è la \textbf{corrente di saturazione inversa} della giunzione:
\[
I_s = qA {n_i}^2 \left( \frac{D_n}{\xi_n N_A} + \frac{D_p}{\xi_p N_D} \right) = \begin{cases} \displaystyle qA {n_i}^2 \left( \frac{D_n}{L_n N_A} + \frac{D_p}{L_p N_D} \right) & w_{p,n} \gg L_{n,p} \text{ (lato lungo)} \\
\displaystyle qA {n_i}^2 \left( \frac{D_n}{w_p N_A} + \frac{D_p}{w_n N_D} \right) & w_{p,n} \ll L_{n,p} \text{ (lato corto)} \end{cases}
\]
che essendo proporzionale a ${{n_i}^2}{N_{A,D}}$ è molto piccola se il drogaggio è abbastanza grande.

A seconda del tipo di polarizzazione domina il termine esponenziale o la corrente $I_s$:
\begin{itemize}
\item \ul{polarizzazione inversa:} $V<0$ $\Rightarrow$ domina la corrente $I_s$ $\Rightarrow$ la corrente $I \simeq -I_s$ è molto piccola anche con una tensione $V$ elevata;
\item \ul{polarizzazione diretta:} $V>0$ $\Rightarrow$ domina il termine esponenziale $\Rightarrow$ la corrente $I$ è fortemente crescente con la tensione $V$.
\end{itemize}

Se non si trascura il contributo dei fenomeni di generazione e ricombinazione, la caratteristica statica segue la legge:
\[
I=I_s \left( e^{\frac{V}{\eta V_T}} -1 \right)
\]
dove $\eta$ è il \textbf{fattore di idealità}, e dipende dalla polarizzazione: $\eta = \eta \left( V \right)$ ($\eta = 1 \div 2$). Per una giunzione al silicio:
\begin{itemize}
\item $\eta \approx 2$ per basse tensioni dirette ($V \leq 0,3 \; V$) e in polarizzazione inversa;
\item $\eta \approx 1$ per tensioni dirette elevate.
\end{itemize}

\subsection{Modello statico}
\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{pic/08/Giunzione_pn_vs_diodo_ideale.png}
\end{figure}

\noindent
Si alimenti un \textbf{diodo} in DC con un generatore reale $V_a$ di resistenza $R$ (che includa la resistenza parassita $R_p$). La tensione $V$ sul diodo e la corrente $I$ devono soddisfare due vincoli:
\begin{itemize}
\item la \textbf{caratteristica statica} $I \left( V \right)$ (non lineare) del diodo;
\item la relazione lineare (detta \textbf{retta di carico}) dovuta alla legge di Kirchhoff:
\[
I= \frac{V_a - V}{R}
\]
\end{itemize}

La soluzione $\left( I_0 , V_0 \right)$ costituisce il \textbf{punto di funzionamento} della giunzione, in questo caso \textbf{a riposo} poiché la tensione del generatore è costante.

Si può usare un \textbf{modello statico semplificato} per la caratteristica statica:
\begin{itemize}
\item \textbf{diodo interdetto:} in polarizzazione inversa ($V < V_\gamma$), il diodo è un circuito aperto $\Rightarrow$ $I=0$;
\item \textbf{diodo in conduzione:} in polarizzazione diretta ($I > 0$), il diodo è un generatore ideale di tensione con una caduta di potenziale $V_0$ costante pari alla tensione di accensione $V_\gamma \approx \tfrac{E_g}{2q}$ (per il silicio: $V_\gamma = 0,5 \div 0,6 \; V$).
\end{itemize}

Nel diodo ideale, i comportamenti della corrente $I$ sono approssimati al circuito aperto e al cortocircuito: la conduzione è permessa solo se la corrente scorre dal catodo verso l'anodo.
\FloatBarrier

\subsection{Fenomeni di breakdown}
\begin{figure}
	\centering
	\includegraphics[width=0.2\linewidth]{pic/08/Pn_junction_breakdown.png}
\end{figure}

\noindent
In polarizzazione inversa, esiste un valore negativo di tensione, detto \textbf{di breakdown} $V_{BD}$, in cui avviene un \textbf{fenomeno di rottura}. Dopo il breakdown, la corrente aumenta\footnote{in valore assoluto} in modo brusco, più dell'esponenziale, e la giunzione si comporta in modo simile a un generatore ideale di tensione; siccome sia la tensione sia la corrente sono molto elevate,\footnotemark[\value{footnote}] la potenza dissipata $p=v \cdot i>0$ è molto grande e può distruggere il dispositivo (in polarizzazione diretta invece la tensione non è così elevata).
\FloatBarrier

\subsubsection{Cause di breakdown}
\paragraph{Perforazione diretta}
Una tensione applicata troppo elevata\footnotemark[\value{footnote}] può allargare la regione svuotata fino ai confini della giunzione, annullando la barriera di energia potenziale in uno ``scivolo'' che favorisce il moto dei portatori maggioritari dai bordi al lato opposto $\Rightarrow$ si determina una corrente molto elevata. Il fenomeno è favorito in diodi con lati sottili.

\paragraph{Effetto valanga}
L'allargamento della regione svuotata corrisponde a un aumento\footnotemark[\value{footnote}] del valore di picco del campo elettrico. Se la tensione applicata supera\footnotemark[\value{footnote}] la \textbf{tensione di breakdown per effetto valanga} caratteristica del materiale, il campo elettrico supera\footnotemark[\value{footnote}] il valore critico $E_c$ a cui è associata, e i portatori liberi in BC durante il loro moto casuale nel cristallo, quando urtano tra di loro, acquisiscono energia cinetica sufficiente a causare la \textbf{generazione da impatto} di una coppia elettrone-lacuna $\Rightarrow$ in BC all'elettrone se ne aggiunge un altro $\Rightarrow$ aumentano gli urti $\Rightarrow$ \textbf{moltiplicazione a valanga} dei portatori $\Rightarrow$ brusco aumento della corrente inversa. Il fenomeno è favorito in giunzioni con lati poco drogati, perché a parità di campo elettrico hanno una regione svuotata più ampia. Al crescere della temperatura, la tensione di breakdown per effetto valanga aumenta.\footnotemark[\value{footnote}]

\paragraph{Effetto Zener}
Se la regione svuotata è molto sottile e se i livelli di energia $E_v$ e $E_c$ sono tanto degeneri che il livello $E_c$ al lato $n$ è superiore al livello $E_v$ al lato $p$, un campo elettrico superiore\footnotemark[\value{footnote}] al valore critico può indurre il passaggio orizzontale per \textbf{effetto tunnel} di un elettrone dalla BV del lato $p$ alla BC del lato $n$ (viceversa per le lacune). Il fenomeno è favorito da giunzioni con lati molto drogati, quindi aventi una regione svuotata più sottile a parità di campo elettrico. Al crescere della temperatura, la \textbf{tensione di breakdown per effetto Zener} diminuisce.\footnotemark[\value{footnote}]

\subsubsection{Diodi Zener}
I \textbf{diodi Zener} sono appositamente progettati per lavorare in condizioni di breakdown dovuto sia all'effetto Zener sia all'effetto valanga: poiché questi due tipi di breakdown presentano due comportamenti opposti in funzione delle variazioni di temperatura, essi si compensano a vicenda (\textbf{stabilizzazione in temperatura}). La condizione di breakdown è però limitata in tensione e corrente entro la \textbf{Safe Operating Area} (SOA) indicata dal produttore, altrimenti il dispositivo si può distruggere per effetto termico.

\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/08/Equivalent_circuit_for_Zener_diode.png}
\end{figure}

Il circuito equivalente di un diodo Zener reale è costituito dalla serie:
\begin{itemize}
\item generatore di tensione $V_{Z0} =- V_{BD}$, detta \textbf{tensione nominale};
\item resistore di resistenza $R_Z$, detta \textbf{resistenza parassita}.
\end{itemize}

Nella regione di breakdown, la caratteristica statica $I_r \left( V _r \right) = -I \left( -V \right)$ è approssimabile a una retta di pendenza $\tfrac{1}{R_Z}$; idealmente, la retta è verticale $\Rightarrow$ la resistenza parassita $R_Z$ è nulla. I diodi Zener trovano applicazione come \textbf{stabilizzatori di tensione}, perché se il carico è in parallelo a un diodo Zener quest'ultimo rende costante la tensione fornita al carico indipendentemente dal suo valore di carico.
\FloatBarrier

\section{Assenza di equilibrio con tensione applicata tempo-variante}
\subsection{Capacità di svuotamento \texorpdfstring{$C_s \left( v \right)$}{Cs(v)}}
Nella regione svuotata vi è la \textbf{carica fissa} $Q_f$, costituita dalle coppie di portatori maggioritari ricombinate, a cui è associata la \textbf{capacità di svuotamento} $C_s \left( v \right)$:
\[
i_s \left( t \right) = \frac{dQ_f}{dt} = \frac{dQ_f}{dv} \frac{dv}{dt} = C_s \left( v \right) \frac{dv}{dt} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle C_s \left( v \right) = \frac{dQ_f}{dv} \\ \displaystyle Q_f = -qA \int_{-x_p \left( v \right)}^0 N_A dx = -q A N_A x_p \left( v \right) \\ \displaystyle x_p \left( v \right) = \sqrt{\frac{2 \epsilon}{q}} \cdot \frac{\sqrt{N_{\text{eq}}}}{N_A} \cdot \sqrt{V_{\text{bi}} - v \left( t \right)} \end{cases} \Rightarrow C_s \left( v \right) = A \sqrt{\frac{q \epsilon N_{\text{eq}}}{2}} \cdot \frac{1}{\sqrt{V_{\text{bi}} - v \left( t \right)}}
\]

Questo modello non vale nella \textbf{regione di alta iniezione}, cioè per $v \left( t \right) \rightarrow V_{\text{bi}}$ o maggiore, perché in polarizzazione diretta una tensione $v$ non sufficientemente bassa comporta una corrente $i \left( v \right)$ molto elevata $\Rightarrow$ la corrente che scorre nelle resistenze parassite $R_p$ non è più trascurabile:
\[
v \left( t \right) \to V_{\text{bi}} \Rightarrow \begin{cases} \displaystyle i \left( v \right) = I_s \left( e^{\frac{v \left( t \right)}{V_T}} - 1 \right) \to + \infty \Rightarrow x_p \propto \sqrt{V_{\text{bi}} - v \left( t \right) + R_p i \left( v \right)} \to + \infty \\
\displaystyle x_p \propto \sqrt{V_{\text{bi}} - v \left( t \right)} \to 0 \text{ secondo il modello (errato)} \end{cases}
\]

\subsection{Capacità di diffusione \texorpdfstring{$C_d \left( v \right)$}{Cd(v)}}
Nelle due regioni neutre vi è la \textbf{carica mobile} $Q_m$, costituita dai portatori minoritari in eccesso spinti dalla tensione di polarizzazione diretta,\footnote{Si veda la sezione~\ref{sez:diap27}.} a cui è associata la \textbf{capacità di diffusione} $C_d \left( v \right)$, che è proporzionale alla corrente che scorre nella giunzione:
\[
\begin{cases} \displaystyle i_d \left( t \right) = \frac{dQ_d}{dt} = \frac{dQ_d}{dv} \frac{dv}{dt} = C_d \left( v \right) \frac{dv}{dt} \\ \displaystyle Q_m = Q_n' = -qA \int_{-w_p}^{-x_p} n_p' \left( x \right) dx \approx -qA \int_{- \infty}^{- x_p} n_p' \left( x \right) dx \end{cases} \Rightarrow
\]
\[
\Rightarrow \begin{cases} \displaystyle C_d \left( v \right) = \frac{dQ_m}{dv} = qA \frac{{n_i}^2}{V_T} \left( \frac{L_n}{N_A} + \frac{L_p}{N_D} \right) e^{\frac{v \left( t \right)}{V_T}} \\ \displaystyle i_{DC} \left( v \right) = I_s \left( e^{\frac{v \left( t \right)}{V_T}} -1 \right) \Rightarrow e^{\frac{v \left( t \right)}{V_T}} = \frac{I_{DC} \left( v \right)}{I_s} +1 \end{cases} \Rightarrow C_d \left( v \right) \propto e^{\frac{v \left( t \right)}{V_T}} \propto i_{DC} \left( v \right)
\]

\subsection{Modello dinamico di ampio segnale}
In una giunzione \textit{pn}, quindi, sono presenti due cariche, la carica fissa $Q_f \left( v \right)$ e la carica mobile $Q_m \left( v \right)$,\footnote{Esse sono state ricavate per brevità considerando solamente il lato $p$.} dipendenti dalla tensione applicata $v \left( t \right)$ e associate alle due capacità non lineari rispettivamente $C_s \left( v \right)$ e $C_d \left( v \right)$.

Prevale l'uno o l'altro tipo di carica a seconda del tipo di polarizzazione:
\begin{itemize}
\item \ul{polarizzazione inversa:} prevale la capacità di svuotamento $C_s \left( v \right)$ {\small ($v \left( t \right) <0$ $\Rightarrow$ l'esponenziale di $C_d \left( x \right)$ tende a zero)};
\item \ul{polarizzazione diretta:} prevale la capacità di diffusione $C_d \left( v \right)$ {\small ($v \left( t \right) > 0$ $\Rightarrow$ l'esponenziale di $C_d \left( x \right)$ tende a infinito)}.
\end{itemize}

Se la tensione di ingresso è tempo-variante, alla corrente di uscita ottenuta dalla caratteristica statica $i_{DC} \left( v \right)$ (cioè la risposta istantanea con tensione di ingresso costante) si aggiungono degli \textbf{effetti capacitivi} di ritardo, determinati dalla presenza delle cariche fissa e mobile che dipendono da una tensione applicata tempo-variante $v \left( t \right)$:
\[
i \left( v \right) = i_{DC} \left( v \right) + C_s \left( v \right) \frac{dv}{dt} + C_d \left( v \right) \frac{dv}{dt}
\]

\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{pic/08/Large-signal_equivalent_circuit_for_pn_junction.png}
\end{figure}

Il \textbf{circuito equivalente di ampio segnale} è costituito dal parallelo tra una resistore non lineare e due condensatori non lineari di capacità $C_s \left( v \right)$ e $C_d \left( v \right)$.
\FloatBarrier

\paragraph{Modello statico}
Se la tensione è costante nel tempo, gli effetti capacitivi scompaiono e vale la caratteristica statica. Quest'ultima può essere ulteriormente approssimata al modello statico semplificato del diodo.

\subsection{Modello dinamico di piccolo segnale}
Spesso un dispositivo elettronico è alimentato da una tensione di alimentazione costante e riceve in ingresso un segnale tempo-variante $\Rightarrow$ il segnale tempo-variante di uscita $v \left( t \right)$ o $i \left( t \right)$ può essere decomposto nella somma tra il contributo costante $V_0$ o $I_0$ e il contributo tempo-variante $v_{ss} \left( t \right)</math> o <math>i_{ss}  \left( t \right)$:
\[
\begin{cases} \displaystyle v \left( t \right) = V_0 + v_{ss} \left( t \right) \\ \displaystyle i \left( t \right) = I_0 + i_{ss} \left( t \right) \end{cases}
\]

In \textbf{condizione di piccolo segnale} $\left| v_{ss} \left( t \right) \right| \ll \left| V_0 \right|$, le componenti della corrente $i \left( v \right)$ possono essere sviluppate in serie di Taylor:
\[
\begin{cases} \displaystyle i_{DC} \left( v \right) \approx i_{DC} \left( V_0 \right) + {\left. \frac{\partial i_{DC}}{\partial v} \right|}_{v=V_0} \cdot v_{ss} \left( t \right) = I_0 + {g_d}_0 \cdot v_{ss} \left( t \right) \\ \displaystyle Q_f \left( v \right) \approx Q_f \left( V_0 \right) + {\left. \frac{\partial Q_f}{\partial v} \right|}_{v=V_0} \cdot v_{ss} \left( t \right) = Q_f \left( V_0 \right) + {C_s}_0 \cdot v_{ss} \left( t \right) \\ \displaystyle Q_m \left( v \right) \approx Q_m \left( V_0 \right) + {\left. \frac{\partial Q_m}{\partial v} \right|}_{v=V_0} \cdot v_{ss} \left( t \right) = Q_m \left( V_0 \right) + {C_d}_0 \left( v \right) \cdot v_{ss} \left( t \right) \end{cases}
\]
dove:
\begin{itemize}
\item ${g_d}_0$ è la conduttanza differenziale:
\[
\begin{cases} \displaystyle {g_d}_0 = {\left. \frac{\partial i_{DC}}{\partial v} \right|}_{v=V_0} \\ \displaystyle i_{DC} \left( v \right) = I_s \left( e^{\frac{v}{\eta V_T}} -1 \right) \end{cases} \Rightarrow \begin{cases} \displaystyle {g_d}_0 = \frac{I_s}{\eta V_T} e^{\frac{V_0}{\eta V_T}} \\ \displaystyle e^{\frac{V_0}{\eta V_T}} = \frac{I_0}{I_s} +1 \end{cases} \Rightarrow {g_d}_0 = \frac{I_0 + I_s}{\eta V_T}
\]
\item ${C_s}_0$ è la capacità differenziale di svuotamento:
\[
{C_s}_0 = {\left. \frac{\partial Q_f}{\partial v} \right|}_{v=V_0} = A \sqrt{\frac{q \epsilon N_{\text{eq}}}{2}} \cdot \frac{1}{\sqrt{V_{\text{bi}} -V_0}}
\]
\item ${C_d}_0$ è la capacità differenziale di diffusione:
\[
{C_d}_0 = {\frac{d Q_m}{dv}}_{v=V_0} = qA \frac{ {n_i}^2}{V_T} \left( \frac{L_n}{N_A} + \frac{L_p}{N_D} \right) e^{\frac{V_0}{V_T}}
\]
\end{itemize}

La corrente $i \left( v \right)$ è quindi linearizzabile nell'approssimazione di piccolo segnale in questo modo:
\[
i \left( v \right) = I_0 + i_{ss} \left( t \right) = i_{DC} \left( v \right) + \left( \frac{Q_f \left( v \right)}{dt} \frac{dv}{dt} + \frac{d Q_m \left( v \right)}{dt} \frac{dv}{dt} \right) \approx I_0 + {g_d}_0 \cdot v_{ss} \left( t \right) + \left( {C_s}_0 \frac{d v_{ss}}{dt} + {C_d}_0 \frac{d v_{ss}}{dt} \right)
\]
da cui si distingue la relazione tra le variazioni di segnale rispetto al punto di funzionamento a riposo $I_0$:
\[
i_{ss} \left( t \right) = {g_d}_0 v_{ss} \left( t \right) + {C_s}_0 \frac{d v_{ss}}{dt} + {C_d}_0 \frac{d v_{ss}}{dt}
\]
che è descritta dal '''circuito equivalente di piccolo segnale''' (lineare), costituito dal parallelo di un resistore e due condensatori.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.55\linewidth]{pic/08/Small-signal_equivalent_circuit_for_pn_junction.png}
\end{figure}

Nel circuito di piccolo segnale:
\begin{itemize}
\item gli elementi lineari ($R$, $L$, $C$) rimangono invariati;
\item i generatori di tensione/corrente costante vengono spenti (tensione $\rightarrow$ cortocircuito, corrente $\rightarrow$ circuito aperto).
\end{itemize}

In polarizzazione diretta la capacità di svuotamento ${C_s}_0$ è trascurabile e prevale la capacità di diffusione ${C_d}_0$ grazie al termine esponenziale dominante. La conduttanza ${g_d}_0$ posta in parallelo, però, è proporzionale anch'essa allo stesso termine esponenziale $\Rightarrow$ l'elemento capacitivo è in parallelo con una resistenza molto piccola $\Rightarrow$ a capacità di diffusione elevate corrispondono elevate perdite di potenza dovute all'elemento resistivo. In polarizzazione inversa, invece, la conduttanza ${g_d}_0$ posta in parallelo con la capacità di svuotamento ${C_s}_0$ dominante è molto piccola $\Rightarrow$ si verificano perdite di potenza inferiori.